//
//  FilterUserViewController.swift
//  Agile Sports
//
//  Created by AM on 22/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField


class FilterUserViewController: ParentViewController {
   //MARK: IBOutlets
    @IBOutlet weak var userName: SkyFloatingLabelTextField!
    @IBOutlet weak var leagueName: SkyFloatingLabelTextField!
    @IBOutlet weak var teamName: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func dismissAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
