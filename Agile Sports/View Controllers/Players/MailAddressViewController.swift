//
//  MailAddressViewController.swift
//  Agile Sports
//
//  Created by AM on 19/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class MailAddressViewController: ParentViewController {
    //MARK: IBOutlets
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if(emailTextField.text == "" || emailTextField.text == nil){
           Singleton.shared.showToast(text: "Enter your mail address")
        }else {
            Singleton.shared.addUser.email = emailTextField.text!
            
            self.push(controller: "UserTeamViewController")
        }
    
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        let myVC = storyboard?.instantiateViewController(withIdentifier: "ManagePlayersViewController") as! ManagePlayersViewController
        self.navigationController?.popToViewController(myVC, animated: true)
    }
}
