//
//  UserTypeViewController.swift
//  Agile Sports
//
//  Created by AM on 19/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import Toaster
import SkyFloatingLabelTextField

class SelectTeamViewController: ParentViewController {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var imageForNew: ImageView!
    @IBOutlet weak var imageForExisting: ImageView!
    @IBOutlet weak var viewForSelectTeam: UIView!
    @IBOutlet weak var selectedTeam: SkyFloatingLabelTextField!
    // /@IBOutlet weak var viewForPicker: UIView!
    @IBOutlet weak var labelForPicker: UILabel!
    @IBOutlet weak var formView: UIView!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var teamNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var locationTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var yearFoundedTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var sprintsTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var subdomainTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var leagueNameTextField: SkyFloatingLabelTextField!
    
    var currentPicker = Int()
    var selectedTeamId = Int()
    var selectedLeagueId = Int()
    var leagueArr = [LeagueData]()
    var teamArr = [TeamsResponse]()
    var teamDelegate: AddNewTeam? = nil
    var leagueAdminId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // self.viewForSelectTeam.isHidden = false
        self.formView.isHidden = true
        self.initialiseView()
        self.selectedTeam.delegate = self
        self.teamNameTextField.delegate = self
        self.phoneNumberTextField.delegate = self
        self.locationTextField.delegate = self
        self.yearFoundedTextField.delegate = self
        self.sprintsTextField.delegate = self
        self.subdomainTextField.delegate = self
        self.emailTextField.delegate = self
        self.leagueNameTextField.delegate = self
        self.imageForNew.image = UIImage(named: "circlegray")
        self.imageForExisting.image = UIImage(named: "circleBlue")
        self.handleScreenData()
    }
    
    func handleScreenData() {
        DispatchQueue.main.async {
            if(Singleton.shared.leagueData.count != 0){
                self.leagueArr = Singleton.shared.leagueData
            }else {
                CallAPIViewController.shared.getLeagueData { (response) in
                    self.leagueArr = response
                }
            }
            
            if(Singleton.shared.teamData.count != 0){
                self.teamArr = Singleton.shared.teamData
            }else {
                CallAPIViewController.shared.getTeams { (response) in
                    self.teamArr = response
                }
            }
        }
    }
    
    func initialiseView(){
        self.selectedTeam.text = ""
        self.teamNameTextField.text = ""
        self.locationTextField.text = ""
        self.phoneNumberTextField.text = ""
        self.emailTextField.text = ""
        self.leagueNameTextField.text = ""
        self.yearFoundedTextField.text = ""
        self.sprintsTextField.text = ""
        self.subdomainTextField.text = ""
    }
    
    func addUser(){
      ActivityIndicator.show(view: self.view)
        let data = Singleton.shared.addUser
        let param = [
            "username": data.username ?? "",
            "first_name": data.first_name ?? "",
            "last_name": data.last_name ?? "",
            "email": data.email ?? "",
            "user_type_id": data.user_type_id ?? "0",
            "team_id": data.team_id ?? "0",
            "league_id": data.league_id  ?? "0"
        ] as? [String:Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_USER, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ADD_USER) { (response) in
            Singleton.shared.showToast(text: response.message ?? "")
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SuccessViewController") as! SuccessViewController
            self.initialiseView()
            self.navigationController?.pushViewController(myVC, animated: true)
        }
    }

    //MARK: IBActions
    @IBAction func nextAction(_ sender: Any) {
        if(imageForNew.image ==  UIImage(named:"circleBlue")) {
            
            if(teamNameTextField.text!.isEmpty){
                teamNameTextField.errorMessage = "Enter Team Name"
            }else if(locationTextField.text!.isEmpty){
                locationTextField.errorMessage = "Enter Location"
            }else if(phoneNumberTextField.text!.isEmpty){
                phoneNumberTextField.errorMessage = "Enter Phone Number"
            }else if(emailTextField.text!.isEmpty){
                emailTextField.errorMessage = "Enter Email Address"
            }else if !(self.isValidEmail(emailStr: self.emailTextField.text ?? "")){
                emailTextField.errorMessage = "Enter valid Email Address"
            }else if(leagueNameTextField.text!.isEmpty){
                leagueNameTextField.errorMessage = "Enter League Name"
            }else if(sprintsTextField.text!.isEmpty){
                sprintsTextField.errorMessage = "Enter Sprint Name"
            }else if(subdomainTextField.text!.isEmpty){
                subdomainTextField.errorMessage = "Enter Subdomain"
            }else {
                ActivityIndicator.show(view: self.view)
                let param = [
                    K_NAME: self.teamNameTextField.text!,
                    "no_of_sprints": self.sprintsTextField.text,
                    "city" : self.locationTextField.text!,
                    "admin": Singleton.shared.userInfo.first_name! + Singleton.shared.userInfo.last_name! ,
                    "sub_domain":self.subdomainTextField.text!,
                    "league_id":self.selectedLeagueId,
                    "year_founded":self.yearFoundedTextField.text!,
                    "league_admin": self.leagueAdminId
                    ] as [String : Any]
                
                SessionManager.shared.methodForApiCalling(url:U_BASE + U_ADD_TEAM, method: .post, parameter: param, objectClass: CreateTeam.self, requestCode: U_ADD_TEAM) { (response) in
                    
                    Singleton.shared.addUser.team_id = "\(response.response)"
                    ActivityIndicator.hide()
                    self.addUser()
                }
            }
        }else {
            if (self.selectedTeamId == 0){
              Singleton.shared.showToast(text: "Please select team")
            }else {
                Singleton.shared.addUser.league_id = "\(self.selectedLeagueId)"
                Singleton.shared.addUser.team_id = "\(self.selectedTeamId)"
                self.addUser()
            }
        }
        
    }
    
    @IBAction func newButtonAction(_ sender: Any) {
        self.imageForNew.image = UIImage(named: "circleBlue")
        self.viewForSelectTeam.isHidden = true
        self.formView.isHidden = false
        self.initialiseView()
        self.imageForExisting.image = UIImage(named: "circlegray")
    }
    
    @IBAction func existingButtonAction(_ sender: Any) {
        self.imageForNew.image = UIImage(named: "circlegray")
        self.viewForSelectTeam.isHidden = false
        self.formView.isHidden = true
        self.initialiseView()
        self.imageForExisting.image = UIImage(named: "circleBlue")
    }
    
    @IBAction func selectTeam(_ sender: Any) {
        if(self.teamArr.count == 0){
          Singleton.shared.showToast(text: "No Team to select")
        }else {
            currentPicker = 2
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
            myVC.pickerHeading = "Select Team"
            myVC.pickerDelegate = self
            for val in teamArr {
                
                myVC.pickerData.append(val.name ?? "")
            }
            myVC.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func selectLeague(_ sender: Any) {
        if(self.leagueArr.count == 0){
            Singleton.shared.showToast(text: "No League to select")
        }else {
            currentPicker = 1
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
            myVC.pickerHeading = "Select League"
            myVC.pickerDelegate = self
            for val in leagueArr {
                myVC.pickerData.append(val.name ?? "")
            }
            myVC.modalPresentationStyle = .overCurrentContext
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SelectTeamViewController: SelectFromPicker, UITextFieldDelegate {
    func selectedItem(name: String, id: Int) {
        if(currentPicker == 1){
            self.selectedLeagueId = self.leagueArr[id].id ?? 0
            self.leagueNameTextField.text = name
            //self.leagueAdminId = self.l
        }else if(currentPicker == 2){
            self.selectedTeam.text = name
            self.selectedTeamId = self.teamArr[id].id ?? 0
            self.leagueAdminId = Int(self.teamArr[id].league_admin ?? "0")!
            self.selectedLeagueId = Int(self.teamArr[id].league_id ?? "0")!
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as? SkyFloatingLabelTextField)?.errorMessage = ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if(textField == phoneNumberTextField){
            if let char = string.cString(using: String.Encoding.utf8) {
                           let isBackSpace = strcmp(char, "\\b")
                           if (isBackSpace == -92 || textField.text!.count <= 10) {
                               return true
                           }else {
                               return false
                           }
                       }
        }else if(textField == yearFoundedTextField){
            if let char = string.cString(using: String.Encoding.utf8) {
                           let isBackSpace = strcmp(char, "\\b")
                           if (isBackSpace == -92 || textField.text!.count <= 3) {
                               return true
                           }else {
                               return false
                           }
                       }
        }else if(textField == sprintsTextField){
            if let char = string.cString(using: String.Encoding.utf8) {
                           let isBackSpace = strcmp(char, "\\b")
                           if (isBackSpace == -92 || textField.text!.count <= 1) {
                               return true
                           }else {
                               return false
                           }
                       }
        }else {
            return true
        }
        return true
    }
}
