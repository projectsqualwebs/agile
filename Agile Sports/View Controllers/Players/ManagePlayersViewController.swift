//
//  ManagePlayersViewController.swift
//  Agile Sports
//
//  Created by AM on 18/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SDWebImage
import iOSDropDown
import SkyFloatingLabelTextField

class ManagePlayersViewController: ParentViewController,UIGestureRecognizerDelegate {
    
    //MARK: IBOutlers
    @IBOutlet weak var usersCollectionView: UICollectionView!
    @IBOutlet weak var labelTotalUser: UILabel!
    // @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var vieWAddUser: UIView!
    @IBOutlet weak var userRole: UILabel!
    @IBOutlet weak var updatePopUp: UIView!
    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var lastName: SkyFloatingLabelTextField!
    @IBOutlet weak var emailField: SkyFloatingLabelTextField!
    @IBOutlet weak var userNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var userName: DesignableUILabel!
    
    @IBOutlet weak var selectTeam: DropDown!
    
    
    var userData = [UserInfo]()
    var total_count = 0
    var teamInfo = [TeamsResponse]()
    var selectedUser = Int()
    var selectedTeam = TeamsResponse()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
          self.userName.text = (Singleton.shared.userInfo.first_name ?? "") + (Singleton.shared.userInfo.last_name ?? "")
        self.userRole.text = Singleton.shared.userInfo.user_role!
        getUsers()
        self.updatePopUp.isHidden = true
        let lpgr: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleLongPress(_:)))
        lpgr.delegate = self
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        self.usersCollectionView?.addGestureRecognizer(lpgr)
        getTeams()
        self.selectTeam.didSelect { (string, id, val) in
            self.selectedTeam = self.teamInfo[id]
        }
    }
    
    func getTeams() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_TEAMS, method:.get, parameter: nil, objectClass:GetTeams.self, requestCode: U_GET_LEAGUE) { (response) in
            self.teamInfo = response.response!
            for val in self.teamInfo {
                self.selectTeam.optionArray.append(val.name!)
            }
            ActivityIndicator.hide()
        }
    }
    
    @objc func handleLongPress(_ gesture : UILongPressGestureRecognizer!) {
        if gesture.state != .ended {
            return
        }
        let p = gesture.location(in: self.usersCollectionView)
        if let indexPath = self.usersCollectionView.indexPathForItem(at: p) {
            // get the cell at indexPath (the one you long pressed)
            let cell = self.usersCollectionView.cellForItem(at: indexPath)
            //self.showAlert(title:"Sucess", message: nil, action1Name: "Ok", action2Name: nil)
            self.showPopup(title: "Action", msg: "", id: indexPath.row)
        } else {
            print("couldn't find index path")
        }
    }
    
    
    
    func getUsers() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url:  U_BASE + U_GET_ALL_USERS + "\(Singleton.shared.userInfo.user_type_id!)", method: .get, parameter: nil, objectClass: GetUsers.self, requestCode: U_GET_ALL_USERS) { (data) in
            self.userData = []
            for val in data.response! {
                switch (K_CURRENT_USER) {
                case 1:
                    self.userData.append(val)
                    break
                case 2:
                    if((val.user_type_id! != 2)  && (val.user_type_id! != 1)){
                        self.userData.append(val)
                    }
                    break
                case 3:
                    if((val.team_id == Singleton.shared.userInfo.team_id) && (val.user_type_id! != 1) && (val.user_type_id! != 2) && (val.user_type_id! != 3)){
                        self.userData.append(val)
                    }
                    break
                case 4:
                    if((val.team_id == Singleton.shared.userInfo.team_id) && (val.user_type_id! != 1) && (val.user_type_id! != 2) && (val.user_type_id! != 3)  && (val.user_type_id! != 4)){
                        self.userData.append(val)
                    }
                    break
                case 5:
                    if((val.team_id == Singleton.shared.userInfo.team_id) && ((val.user_type_id! == 6) || (val.user_type_id! == 8))){
                        self.userData.append(val)
                        self.total_count = self.total_count + 1
                    }
                    break
                case 6:
                    if((val.team_id == Singleton.shared.userInfo.team_id) && ((val.user_type_id! == 6) || (val.user_type_id! == 7)  || (val.user_type_id! == 8))){
                        self.userData.append(val)
                    }
                    break
                case 7:
                    if((val.team_id == Singleton.shared.userInfo.team_id) && ((val.user_type_id! == 7) || (val.user_type_id! == 8))){
                        self.userData.append(val)
                    }
                    break
                case 8:
                    break
                default:
                    break
                }
            }
            print(self.total_count)
            self.labelTotalUser.text = "Total Users: \(self.userData.count)"
            self.usersCollectionView.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func getPlayers() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_TEAMPLAYERS, method: .get, parameter: nil, objectClass: GetPlayers.self, requestCode: U_GET_ALL_USERS) { (data) in
            self.userData = (data.response?.players!)!
            self.labelTotalUser.text = "Total Users: \(self.userData.count)"
            self.usersCollectionView.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    
    //MARK: IBActions
    @IBAction func filterUser(_ sender: Any) {
        let myVC = storyboard?.instantiateViewController(withIdentifier: "FilterUserViewController") as! FilterUserViewController
        self.present(myVC, animated: true, completion: nil)
        
    }
    
    @IBAction func adduserAction(_ sender: Any) {
        self.push(controller:"UserTypeViewController")
    }
    
    
    @IBAction func optionActoin(_ sender: Any) {
        self.push(controller:"SideDrawerViewController")
    }
    
    func showPopup(title: String, msg: String,id:Int) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesBtn = UIAlertAction(title:"Edit", style: .default) { (UIAlertAction) in
            let val = self.userData[id]
            self.selectedUser = id
            self.firstName.text = val.first_name
            self.lastName.text = val.last_name
            self.userNameField.text = val.username
            self.emailField.text = val.email
            let team = self.teamInfo.filter{
                $0.id == val.team_id
            }
            self.selectTeam.text = team[0].name
            self.updatePopUp.isHidden = false
        }
        let noBtn = UIAlertAction(title: "Delete", style: .default){
            (UIAlertAction) in
            self.popupView(title: "Delete User", msg: "Are your sure?", id: id)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default){
            (UIAlertAction) in
            //  self.noButtonAction()
        }
        
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func popupView(title: String, msg: String,id:Int) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
            let param = [
                "row_id":id,
                "table":"User"
                ] as? [String:Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_ENTRIES, method: .post, parameter: param, objectClass: Response.self, requestCode: U_DELETE_ENTRIES) { (response) in
                print(response)
            }
        }
        let noBtn = UIAlertAction(title: "No", style: .default){
            (UIAlertAction) in
            //  self.noButtonAction()
        }
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func hideUpdateForm(_ sender: Any){
        self.updatePopUp.isHidden = true
    }
    
    @IBAction func updateAction(_ sender: Any) {
        var param = [
            "id":self.selectedUser,
            "username":self.userNameField.text,
            "first_name":self.firstName.text,
            "last_name":self.lastName.text,
            "assign_league_id":self.selectedTeam.league_id,
            "team_id":self.selectedTeam.id
            ] as [String:Any]
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_EDIT_USER, method: .post, parameter: param, objectClass: Response.self, requestCode: U_EDIT_USER) { (resposne) in
            self.updatePopUp.isHidden = true
            self.getUsers()
        }
    }
}

extension ManagePlayersViewController: UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return userData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UsersCollectionViewCell", for: indexPath) as! UsersCollectionViewCell
        let val = userData[indexPath.row]
        if(K_CURRENT_USER != 5){
            cell.stackView.isHidden = true
        }else {
            cell.stackView.isHidden = false
            if let recommend = val.recommended_value {
                cell.recommendedValue.text = "\(recommend)"
            }else {
                cell.recommendedValue.text = "0"
            }
            if let planned = val.planed_value {
                cell.plannedValue.text = "\(planned)"
            }else {
                cell.plannedValue.text = "0"
            }
            if let actual = val.actual_value {
                cell.actualValue.text = "\(actual)"
            }else {
                cell.actualValue.text = "0"
            }
        }
        cell.userName.text = val.first_name! + " " + val.last_name!
        if let img = val.image{
            cell.userImage.sd_setImage(with: URL(string:val.image!), placeholderImage: UIImage(named: "player"))
        }else {
            cell.userImage.image = UIImage(named: "player")
        }
        switch val.user_type_id! {
        case 1:
            cell.userRole.text = "Super Admin"
            break
        case 2:
            cell.userRole.text = "League Admin"
            break
        case 3:
            cell.userRole.text = "Team Admin"
            break
        case 4:
            cell.userRole.text = "Admin"
            break
        case 5:
            cell.userRole.text = "Analyst"
            break
        case 6:
            cell.userRole.text = "Coach"
            break
        case 7:
            cell.userRole.text = "Assistant Coach"
            break
        case 8:
            cell.userRole.text = "Player"
            break
        default:
            break
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(K_CURRENT_USER == 5) {
            let myVC = storyboard?.instantiateViewController(withIdentifier: "AgilityCardViewController") as! AgilityCardViewController
            myVC.userId = userData[indexPath.row].id!
            myVC.userType = userData[indexPath.row].user_type_id!
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((self.view.frame.width * 0.95)/2.15)
  
        return CGSize(width: width, height:200)
    }
}


