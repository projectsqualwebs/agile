//
//  UsersCollectionViewCell.swift
//  Agile Sports
//
//  Created by AM on 19/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class UsersCollectionViewCell: UICollectionViewCell {
    //MARK: IBOUTLETS
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var plannedValue: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var actualValue: UILabel!
    @IBOutlet weak var recommendedValue: UILabel!
    @IBOutlet weak var userRole: UILabel!
    
    
}
