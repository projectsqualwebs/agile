//
//  PlayerTeamViewController.swift
//  Agile Sports
//
//  Created by AM on 19/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class UserTeamViewController: ParentViewController {

    @IBOutlet weak var userNameTextField: SkyFloatingLabelTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextAction(_ sender: Any) {
        if(userNameTextField.text == "" || userNameTextField.text == nil){
            Singleton.shared.showToast(text: "Enter Username")
        }else {
            Singleton.shared.addUser.username = userNameTextField.text!
            
            self.push(controller: "UserNameViewController")
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
     self.navigationController?.popViewController(animated: true)
        
    }
}
