//
//  UserTypeViewController.swift
//  Agile Sports
//
//  Created by AM on 22/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class UserTypeViewController: ParentViewController {
    
    //MARK: IBOUTLETS
    @IBOutlet weak var imageForAnalyst: ImageView!
    @IBOutlet weak var imageForCoach: ImageView!
    @IBOutlet weak var imageForPlayer: ImageView!
    @IBOutlet weak var imageLeagueAdmin: ImageView!
    @IBOutlet weak var imageTeamAdmin: ImageView!
    @IBOutlet weak var imageAsstCoach: ImageView!
    @IBOutlet weak var imageScout: ImageView!
    
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var scoutView: UIView!
    @IBOutlet weak var asstCoachView: UIView!
    @IBOutlet weak var leagueAdminView: UIView!
    @IBOutlet weak var teamAdminView: UIView!
    @IBOutlet weak var coachView: UIView!
    
    @IBOutlet weak var analystView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initView()
    }
    
    func initView(){
        if(K_CURRENT_USER == 1){
            self.leagueAdminView.isHidden = false
            self.teamAdminView.isHidden = false
        }else if(K_CURRENT_USER == 2){
            self.teamAdminView.isHidden = false
        }
        self.imageLeagueAdmin.image = #imageLiteral(resourceName: "circlegray")
        self.imageTeamAdmin.image = #imageLiteral(resourceName: "circlegray")
        self.imageForAnalyst.image = #imageLiteral(resourceName: "circlegray")
        self.imageForCoach.image = #imageLiteral(resourceName: "circlegray")
        self.imageAsstCoach.image = #imageLiteral(resourceName: "circlegray")
        self.imageForPlayer.image = #imageLiteral(resourceName: "circlegray")
        self.imageScout.image = #imageLiteral(resourceName: "circlegray")
    }
    
    
    @IBAction func leagueAdminAction(_ sender: Any) {
       self.initView()
        self.imageLeagueAdmin.image = #imageLiteral(resourceName: "circleBlue")
        Singleton.shared.addUser.user_type_id = 2
        Singleton.shared.userToAdd = "Add League Admin"
    }
    
    @IBAction func teamAdminAction(_ sender: Any) {
        self.initView()
        self.imageTeamAdmin.image = #imageLiteral(resourceName: "circleBlue")
        Singleton.shared.addUser.user_type_id = 3
        Singleton.shared.userToAdd = "Add Team Admin"
    }
    
    @IBAction func analystButtonAction(_ sender: Any) {
        self.initView()
        self.imageForAnalyst.image = #imageLiteral(resourceName: "circleBlue")
        Singleton.shared.addUser.user_type_id = 5
        Singleton.shared.userToAdd = "Add Analyst"
    }
    
    @IBAction func coachButtonAction(_ sender: Any) {
       self.initView()
        self.imageForCoach.image = #imageLiteral(resourceName: "circleBlue")
        Singleton.shared.addUser.user_type_id = 6
        Singleton.shared.userToAdd = "Add Coach"
    }
    
    
    @IBAction func asstCoachAction(_ sender: Any) {
        self.initView()
        self.imageAsstCoach.image = #imageLiteral(resourceName: "circleBlue")
        Singleton.shared.addUser.user_type_id = 7
        Singleton.shared.userToAdd = "Add Assistant Coach"
    }
    
    @IBAction func playerbuttonAction(_ sender: Any) {
       self.initView()
        self.imageForPlayer.image = #imageLiteral(resourceName: "circleBlue")
        Singleton.shared.addUser.user_type_id = 8
        Singleton.shared.userToAdd = "Add Player"
    }
    
    
    @IBAction func scoutAction(_ sender: Any) {
        self.initView()
        self.imageScout.image = #imageLiteral(resourceName: "circleBlue")
        Singleton.shared.addUser.user_type_id = 11
        Singleton.shared.userToAdd = "Add Scout"
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        
        self.push(controller: "UserNameViewController")
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        Singleton.shared.addUser = AddUser()
    self.navigationController?.popViewController(animated: true)
    }
}
