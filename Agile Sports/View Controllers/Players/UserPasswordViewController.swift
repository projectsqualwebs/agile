//
//  UserPasswordViewController.swift
//  Agile Sports
//
//  Created by AM on 20/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class UserPasswordViewController: ParentViewController {
    
    //MARK: IBOutlets
     @IBOutlet weak var addUserlabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.addUserlabel.text = Singleton.shared.userToAdd
    }
    @IBAction func nextAction(_ sender: Any) {
        self.push(controller: "SelectTeamViewController")
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "UserTypeViewController") as! UserTypeViewController
        self.navigationController?.popToViewController(myVC, animated: true)
    }
}
