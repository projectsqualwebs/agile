//
//  SuccessViewController.swift
//  Agile Sports
//
//  Created by AM on 20/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class SuccessViewController: ParentViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var addUserlabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addUserlabel.text = Singleton.shared.userToAdd
    }
    
    
    @IBAction func addUserAction(_ sender: Any) {
        self.push(controller: "UserTypeViewController") 
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.push(controller: "ManageBottomTabViewController")
    }
    

}
