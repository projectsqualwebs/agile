//
//  EditUserViewController.swift
//  Agile Sports
//
//  Created by AM on 20/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class EditUserViewController: ParentViewController {
   
    //MARK: IBOutlts
    @IBOutlet weak var imageForCoach: ImageView!
    @IBOutlet weak var imageForAnalyst: ImageView!
    @IBOutlet weak var imageForPlayer: ImageView!
    @IBOutlet weak var imageForActive: ImageView!
    @IBOutlet weak var imageInactive: ImageView!
    
    @IBOutlet weak var firstName: SkyFloatingLabelTextField!
    @IBOutlet weak var lastName: SkyFloatingLabelTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func playerButtonAction(_ sender: Any) {
        self.imageForPlayer.image = UIImage(named: "circleBlue")
        self.imageForCoach.image = UIImage(named: "circlegray")
        self.imageForAnalyst.image = UIImage(named: "circlegray")
    }
    
    @IBAction func analystButtonAction(_ sender: Any) {
        self.imageForPlayer.image = UIImage(named: "circlegray")
        self.imageForCoach.image = UIImage(named: "circlegray")
        self.imageForAnalyst.image = UIImage(named: "circleBlue")
    }
    
    @IBAction func coachButtonAction(_ sender: Any) {
        self.imageForPlayer.image = UIImage(named: "circlegray")
        self.imageForCoach.image = UIImage(named: "circleBlue")
        self.imageForAnalyst.image = UIImage(named: "circlegray")
    }
    
    @IBAction func activeButtonAction(_ sender: Any) {
        self.imageForActive.image = UIImage(named: "circleBlue")
        self.imageInactive.image = UIImage(named: "circlegray")
    }
    
    @IBAction func inActiveButtonAction(_ sender: Any) {
        self.imageForActive.image = UIImage(named: "circlegray")
        self.imageInactive.image = UIImage(named: "circleBlue")
    }

}
