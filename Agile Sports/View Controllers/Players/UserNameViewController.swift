//
//  PlayerNameViewController.swift
//  Agile Sports
//
//  Created by AM on 19/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class UserNameViewController: ParentViewController, UITextFieldDelegate {

    @IBOutlet weak var firstNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var lastNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var usernameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var addUserlabel: UILabel!
    
    @IBOutlet weak var emailTextField: SkyFloatingLabelTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addUserlabel.text = Singleton.shared.userToAdd
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        usernameTextField.delegate = self
        emailTextField.delegate = self
      
    }
    
    @IBAction func nextAction(_ sender: Any) {
         if(usernameTextField.text == "" || usernameTextField.text == nil){
            usernameTextField.errorMessage = "Enter Username"
        }else if(firstNameTextField.text == "" || firstNameTextField.text == nil){
            firstNameTextField.errorMessage = "Enter your First Name"
        }else if(lastNameTextField.text == "" || lastNameTextField.text == nil){
            lastNameTextField.errorMessage = "Enter your last Name"
        }else if(emailTextField.text == "" || emailTextField.text == nil){
            emailTextField.errorMessage = "Enter Email Address"
         }else if(!isValidEmail(emailStr: self.emailTextField.text!)){
            emailTextField.errorMessage = "Enter valid Email Address"
         }else {
            Singleton.shared.addUser.first_name = firstNameTextField.text!
             Singleton.shared.addUser.last_name = lastNameTextField.text!
            Singleton.shared.addUser.email = emailTextField.text!
            Singleton.shared.addUser.username = usernameTextField.text!
            self.push(controller: "SelectTeamViewController")
        }
    }
    
    @IBAction func cancelAction(_ sender: Any) {
      for controller in self.navigationController!.viewControllers as Array {
          if controller.isKind(of: ManageBottomTabViewController.self) {
            Singleton.shared.addUser = AddUser()
            self.navigationController!.popToViewController(controller, animated: true)
              break
          }
      }
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as? SkyFloatingLabelTextField)?.errorMessage = ""
    }
    
}
