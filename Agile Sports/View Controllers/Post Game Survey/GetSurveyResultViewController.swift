//
//  GetSurveyResultViewController.swift
//  Agile Sports
//
//  Created by qw on 24/04/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import Charts


class GetSurveyResultViewController: UIViewController, ChartViewDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var surveyTable: ContentSizedTableView!
    
    
    var surveyData = [GetSurveyResultResponse]()
    var matchId = 1
    var sprintId = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        surveyTable.estimatedRowHeight = UITableView.automaticDimension
        surveyTable.rowHeight = UITableView.automaticDimension
        self.getSurveyResult()
    }
    
    
    func getSurveyResult(){
        ActivityIndicator.show(view: self.view)
        
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_TEAM_MEMBERS + "1&matchId=\(matchId)", method: .get, parameter: nil, objectClass: GetSurveyResult.self, requestCode: U_GET_TEAM_MEMBERS) { (response) in
            self.surveyData = response.response
            self.surveyTable.delegate = self
            self.surveyTable.dataSource = self
            self.surveyTable.reloadData()
        }
    }
    

    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.back()
    }
}

extension GetSurveyResultViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.surveyData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostGameStatisticTable") as! PostGameStatisticTable
        cell.questionLabel.text = "\(indexPath.row + 1). " + (self.surveyData[indexPath.row].question ?? "")
        cell.answerData = self.surveyData[indexPath.row]
        cell.handleBarView()
        cell.barChartView.fitScreen()
        cell.barChartView.fitBars = true
        return cell
    }
}

class PostGameStatisticTable: UITableViewCell, ChartViewDelegate,IAxisValueFormatter {
    //MARK: IBOutlets
    @IBOutlet weak var questionLabel: DesignableUILabel!
    @IBOutlet var barChartView: BarChartView!
    
    var answerData = GetSurveyResultResponse()
    
    override func awakeFromNib() {
        barChartView.delegate = self
         
         barChartView.chartDescription?.enabled = false
        // barChartView.pinchZoomEnabled = false
         barChartView.drawGridBackgroundEnabled = false
         barChartView.drawBarShadowEnabled = false
         barChartView.drawValueAboveBarEnabled = true
         barChartView.highlightFullBarEnabled = false
         barChartView.fitBars = false
         barChartView.setScaleEnabled(false)
         barChartView.rightAxis.enabled = false
         
         barChartView.legend.enabled = false
         barChartView.highlightPerTapEnabled = false
         barChartView.dragEnabled = true
         
         /* Set xAxis values */
         let xAxis = barChartView.xAxis
         xAxis.labelPosition = .bottom
         xAxis.drawAxisLineEnabled = true
         xAxis.drawGridLinesEnabled = false
         
         xAxis.avoidFirstLastClippingEnabled = false
         xAxis.labelPosition = .bottom
         xAxis.labelTextColor = UIColor.black
         xAxis.granularity = 1
         xAxis.axisMinimum = 0.0
        // xAxis.centerAxisLabelsEnabled = false
         xAxis.valueFormatter = self
        

         xAxis.labelFont = UIFont.systemFont(ofSize: 10)

         /* Y Axis */
         let leftAxis = barChartView.leftAxis
         leftAxis.enabled = true
        // leftAxis.drawAxisLineEnabled = false
         leftAxis.drawGridLinesEnabled = true
        leftAxis.gridLineDashPhase = 0
         leftAxis.gridLineDashLengths = [0]
        leftAxis.gridLineWidth = 0.5
         leftAxis.gridColor = UIColor.black
         //leftAxis.drawLabelsEnabled = false
         
         leftAxis.labelTextColor = UIColor.black
         leftAxis.axisMinimum = 0.0
    }
    
    func handleBarView(){
        let xArray = self.answerData.answers
        let ys1 = xArray.map { x in return Double(x.y ?? 0)}
        let yse1 = ys1.enumerated().map { x, y in return
            BarChartDataEntry(x: Double(x), y: y)
        }
        let data = BarChartData()
        let ds1 = BarChartDataSet(entries: yse1, label: "Hello")
        ds1.colors = [blueColor]
        data.addDataSet(ds1)
       
       let groupSpace = 0.01
       let barSpace = 0.001
       let barWidth = 0.5
        
        data.barWidth = barWidth
        self.barChartView.xAxis.axisMinimum =  -0.5 //Double(xArray[0])
        self.barChartView.xAxis.axisMaximum = Double(self.answerData.answers.count)
        
        self.barChartView.groupBars(fromX: 0.0, groupSpace: groupSpace, barSpace: barSpace  )
        self.barChartView.data = data
        self.barChartView.gridBackgroundColor = NSUIColor.white
        barChartView.fitScreen()
    }
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
       if(self.answerData.answers.count > 0 && Int(value) < self.answerData.answers.count){
            var fullString: String = self.answerData.answers[Int(value)].name ?? ""
            let fullStringArr = fullString.components(separatedBy: " ")
            var firstString: String = fullStringArr[0]
            var lastString:String?
            if(fullStringArr.count > 1){
             lastString  = fullStringArr[1]
            }
            return firstString + "\n" + (lastString ?? "")
        }else {
            return ""
        }
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print("working")
    }
}
