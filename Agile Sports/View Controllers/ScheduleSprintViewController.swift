//
//  ScheduleSprintViewController.swift
//  Agile Sports
//
//  Created by qw on 20/04/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class ScheduleSprintViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var plannedCollection: UICollectionView!
    @IBOutlet weak var recommendedCollection: UICollectionView!
    @IBOutlet weak var actualCollection: UICollectionView!
    @IBOutlet weak var recommendedValue: UILabel!
    @IBOutlet weak var plannedValue: UILabel!
    @IBOutlet weak var actualValue: DesignableUILabel!
    @IBOutlet weak var viewPlannedCollection: UIView!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    @IBOutlet weak var submitButton: CustomButton!
    
    
    var userId = Int()
    var matchId = Int()
    var sprintId = Int()
    var userType = Int()
    var opponentName = String()
    var userMetricData = [MatricesResponse]()
    var recommendValue  = Double()
    var planValue = Double()
    var selectedMetrices = [MatricesResponse]()
    var setMetric = Set<Int>()
    var selectedMetricJson = [NSDictionary]()
    var currentScreen = String()
    var heading : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.headingLabel.text =  self.heading ?? self.opponentName
        if(currentScreen != K_ALL_PLAYER_AGILITY_CARDS){
            userId = Singleton.shared.userInfo.id ?? 0
            self.submitButton.isHidden = false
        }else {
            self.submitButton.isHidden = true
        }
        self.getMetrics()
        self.recommendedCollection.isHidden = false
        self.viewPlannedCollection.isHidden = true
        self.viewPlannedCollection.isHidden = true
        
    }
    
    func getMetrics() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ASSIGNEMETRICS + "\(userId)" + "&matchId=\(self.matchId)&sprintId=\(self.sprintId)", method: .get, parameter: nil, objectClass: GetAgility.self, requestCode: U_GET_ASSIGNEMETRICS) { (response) in
            self.userMetricData = (response.response?.metrics!)!
            self.recommendedCollection.reloadData()
            self.actualCollection.reloadData()
            self.plannedCollection.reloadData()
            if let score = response.response?.score {
                self.recommendValue = Double(score.recommended_value ?? "0")!
                self.planValue = Double(score.planed_value ?? "0")!
                if let recommend = score.recommended_value {
                    self.recommendedValue.text = "\(recommend)"
                }else {
                    self.recommendedValue.text = "0"
                }
                if let planned = score.planed_value {
                    self.plannedValue.text = "\(planned)"
                }else {
                    self.plannedValue.text = "0"
                }
                if let actual = score.actual_value {
                    self.actualValue.text = "\(actual)"
                }else {
                    self.actualValue.text = "0"
                }
            }else {
                self.recommendValue =  0
                self.planValue =  0
                self.recommendedValue.text = "0"
                self.plannedValue.text = "0"
            }
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func jsonConversion(val: [MatricesResponse]) {
        for arr in val {
            let values = MatricesResponse(id: arr.id ?? 0, metrics_name: arr.metrics_name ?? "", type: arr.type ?? 0, sub_type: arr.sub_type ?? 0, by_player: arr.by_player ?? 0, by_analyst: arr.by_analyst ?? 0, user_id: arr.user_id ?? 0, score: arr.score ?? "", metrics_value: arr.metrics_value ?? "", numerator_metrics: arr.numerator_metrics ?? 0, denominator_metrics: arr.denominator_metrics ?? 0, ppa_recommended: arr.ppa_recommended ?? 0, failed:arr.failed , ppa_planned: arr.ppa_planned ?? 0, succeed: arr.succeed ?? 0, match_id: arr.match_id ?? 0)
            let jsonEncoder = JSONEncoder()
            let jsonData = try! jsonEncoder.encode(values)
            
            let jsonString = String(data: jsonData, encoding: .utf8)!
            let data = jsonString.data(using: .utf8)
            do {
                let dic = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
                self.selectedMetricJson.append(dic)
                print(dic)
            }
            catch let e as NSError {
                print(e.localizedDescription)
            }
        }
        var userRole = String()
        if(Singleton.shared.userInfo.user_role! == "Players"){
            userRole = "player"
        }else {
            userRole = "analyst"
        }
        
        let param = [
            "selectedMetrics": self.selectedMetricJson,
            "by_user":userRole,
            "planed_value":self.plannedValue.text!,
            "recommended_value":self.recommendedValue.text,
            "actual_value": self.actualValue.text,
            "match_id":self.matchId,
            "user_id":self.userId,
            "user_type":self.userType
            ] as [String : Any]
        SessionManager.shared.methodForApiCalling(url: U_BASE +  U_ASSIGN_METRICS, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ASSIGN_METRICS) { (response) in
            print(response)
            ActivityIndicator.hide()
            
        }
        // let jsonString = String(data: jsonData, encoding: .utf8)!
        
    }
    
    @IBAction func recommendedAction(_ sender: Any) {
        self.viewPlannedCollection.isHidden = true
        self.actualCollection.isHidden = true
        self.recommendedCollection.isHidden = false
    }
    
    @IBAction func plannedAction(_ sender: Any) {
        self.viewPlannedCollection.isHidden = false
        self.actualCollection.isHidden = true
        self.recommendedCollection.isHidden = true
    }
    
    @IBAction func actualAction(_ sender: Any) {
        self.viewPlannedCollection.isHidden = true
        self.actualCollection.isHidden = false
        self.recommendedCollection.isHidden = true
    }
    
    @IBAction func submitAction(_ sender: Any) {
        ActivityIndicator.show(view: self.view)
        for val in setMetric {
            self.selectedMetrices.append(self.userMetricData[val])
        }
        let myVal = jsonConversion(val: self.selectedMetrices)
    }
    
}

extension ScheduleSprintViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.userMetricData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let val = self.userMetricData[indexPath.row]
        if(collectionView == recommendedCollection){
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell", for: indexPath) as! AgilityCardCollectionViewCell
            cell1.titleFCM.text = val.metrics_name!
            cell1.scoreValue.text! = "\(val.by_player ?? 0)"
            if(val.sub_type == 0){
                cell1.conditionLabel.text = "\(Double(val.score ?? "0")!) pts each"
            }else if(val.sub_type == 1){
                cell1.conditionLabel.text = " if < \(Double(val.score ?? "0")!)"
            }else {
                cell1.conditionLabel.text = "if > \(Double(val.score ?? "0")!)"
            }
            return cell1
        }else if(collectionView == plannedCollection){
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell", for: indexPath) as! AgilityCardCollectionViewCell
            let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell2", for: indexPath) as! AgilityCardCollectionViewCell2
            
            if(val.type == 2){
                cell2.titleFCM.text = val.metrics_name ?? ""
                cell2.scoreValue.text = "\(val.by_player ?? 0)"
                cell2.conditionLabel.text = "\(val.score ?? "") pt wise "
                cell2.backView.backgroundColor = primaryColor
                
                if ((val.by_player ?? 0) > 0){
                    cell2.backView.backgroundColor = primaryColor
                    cell2.titleFCM.textColor = .white
                    cell2.scoreValue.textColor = .white
                    cell2.conditionLabel.textColor = .white
                    cell2.imageForMinus.image = #imageLiteral(resourceName: "minus")
                    cell2.imageForPlus.image = #imageLiteral(resourceName: "plus")
                    self.setMetric.insert(indexPath.row)
                }else {
                    cell2.backView.backgroundColor = .white
                    cell2.titleFCM.textColor = primaryColor
                    cell2.scoreValue.textColor = primaryColor
                    cell2.conditionLabel.textColor = primaryColor
                    cell2.imageForMinus.image = #imageLiteral(resourceName: "minusBlue")
                    cell2.imageForPlus.image = #imageLiteral(resourceName: "plusBlue")
                    self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
                }
                //                if(val.by_analyst == 1){
                //                    self.setMetric.insert(indexPath.row)
                //                    cell2.backView.backgroundColor = primaryColor
                //                }else {
                //                    cell2.backView.backgroundColor = UIColor.white
                //                    self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
                //                }
                if(currentScreen != K_ALL_PLAYER_AGILITY_CARDS){
                    cell2.isUserInteractionEnabled = true
                    cell2.cellClicked = {
                        if(K_CURRENT_USER == 5){
                            if(cell2.backView.backgroundColor == UIColor.green){
                                cell2.backView.backgroundColor = .white
                                self.userMetricData[indexPath.row].by_analyst = 0
                                
                                self.recommendValue = ((self.recommendValue -  (Double(val.score ?? "0")! * Double(val.metrics_value ?? "0")!))*100).rounded()/100
                                if(self.recommendValue >= 0){
                                    self.recommendedValue.text = "\(self.recommendValue)"
                                }else {
                                    self.recommendValue = 0
                                }
                                self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
                            }else {
                                self.recommendValue = ((self.recommendValue +  (Double(val.score ?? "0")! * Double(val.metrics_value ?? "0")!))*100).rounded()/100
                                
                                if(self.recommendValue >= 0){
                                    self.recommendedValue.text = "\(self.recommendValue)"
                                }else {
                                    self.recommendValue = 0
                                }
                                cell2.backView.backgroundColor = UIColor.green
                                self.userMetricData[indexPath.row].by_analyst = 1
                                self.setMetric.insert(indexPath.row)
                            }
                        }else if(K_CURRENT_USER == 8){
                            if(cell2.backView.backgroundColor == primaryColor){
                                
                                self.planValue = ((self.planValue -  (Double(val.score ?? "0")! * Double(self.userMetricData[indexPath.row].by_player ?? 0)))*100).rounded()/100
                                if(self.planValue >= 0){
                                    self.plannedValue.text = "\(self.planValue)"
                                }else {
                                    self.planValue = 0
                                }
                                self.userMetricData[indexPath.row].by_player = 0
                                self.userMetricData[indexPath.row].ppa_planned = 0
                                cell2.backView.backgroundColor = .white
                                cell2.titleFCM.textColor = primaryColor
                                cell2.scoreValue.textColor = primaryColor
                                cell2.conditionLabel.textColor = primaryColor
                                cell2.imageForMinus.image = #imageLiteral(resourceName: "minusBlue")
                                cell2.imageForPlus.image = #imageLiteral(resourceName: "plusBlue")
                                self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
                            }else {
                                self.userMetricData[indexPath.row].by_player =  self.userMetricData[indexPath.row].by_analyst
                                self.userMetricData[indexPath.row].ppa_planned = self.userMetricData[indexPath.row].ppa_recommended
                                self.planValue = ((self.planValue +  (Double(val.score ?? "0")! * Double(self.userMetricData[indexPath.row].by_player ?? 0)))*100).rounded()/100
                                if(self.planValue >= 0){
                                    self.plannedValue.text = "\(self.planValue)"
                                }else {
                                    self.planValue = 0
                                }
                                cell2.backView.backgroundColor = primaryColor
                                cell2.titleFCM.textColor = .white
                                cell2.scoreValue.textColor = .white
                                cell2.conditionLabel.textColor = .white
                                cell2.imageForMinus.image = #imageLiteral(resourceName: "minus")
                                cell2.imageForPlus.image = #imageLiteral(resourceName: "plus")
                                self.setMetric.insert(indexPath.row)
                            }
                        }
                    }
                    cell2.plusButton = {
                        if((K_CURRENT_USER == 5) && (cell2.backView.backgroundColor == UIColor.green)){
                            if(self.recommendValue > 0){
                                self.userMetricData[indexPath.row].score =
                                "\(Double(self.userMetricData[indexPath.row].score ?? "0")! + Double(1))"
                                self.recommendValue = ((self.recommendValue + Double(val.metrics_value ?? "0")!)*100).rounded()/100
                                
                                self.recommendedValue.text = "\(self.recommendValue)"
                                cell2.scoreValue.text = "\(Double(val.score ?? "0")! + Double(1))"
                                //  self.collectionViewAgility.reloadItems(at: [indexPath])
                            }
                        }else if( (K_CURRENT_USER == 8)  && (cell2.backView.backgroundColor == primaryColor)){
                            self.userMetricData[indexPath.row].by_player = (self.userMetricData[indexPath.row].by_player ?? 0) + 1
                            self.userMetricData[indexPath.row].ppa_planned = (self.userMetricData[indexPath.row].ppa_planned ?? 0) + 1
                            self.planValue = ((self.planValue + Double(val.score ?? "0")!)*100).rounded()/100
                            self.plannedValue.text = "\(self.planValue)"
                            cell2.scoreValue.text = "\(val.by_player ?? 0)"
                            self.plannedCollection.reloadItems(at: [indexPath])
                        }
                    }
                    cell2.minusButton = {
                        if((K_CURRENT_USER == 5 ) && (cell2.backView.backgroundColor == UIColor.green)){
                            if(self.recommendValue > 0){
                                self.userMetricData[indexPath.row].score =
                                "\(Double(self.userMetricData[indexPath.row].score ?? "0")! - Double(1))"
                                self.recommendValue = ((self.recommendValue -  Double(val.metrics_value ?? "0")!)*100).rounded()/100
                                self.recommendedValue.text = "\(self.recommendValue)"
                                cell2.scoreValue.text = "\(Double(val.score ?? "0")! - 1.0)"
                                //self.collectionViewAgility.reloadItems(at: [indexPath])
                            }
                        }else if( (K_CURRENT_USER == 8)  && (cell2.backView.backgroundColor == primaryColor)){
                            if((self.userMetricData[indexPath.row].by_player ?? 0) > 0){
                                self.userMetricData[indexPath.row].by_player = (self.userMetricData[indexPath.row].by_player ?? 0)  - 1
                                self.userMetricData[indexPath.row].ppa_planned = (self.userMetricData[indexPath.row].ppa_planned ?? 0) - 1
                                self.planValue = ((self.planValue - Double(val.score ?? "0")!)*100).rounded()/100
                                self.plannedValue.text = "\(self.planValue)"
                                cell2.scoreValue.text = "\(val.by_player ?? 0)"
                                self.plannedCollection.reloadItems(at: [indexPath])
                            }
                        }
                    }
                }else {
                    cell2.isUserInteractionEnabled = false
                    cell2.imageForMinus.image = nil
                    cell2.imageForPlus.image = nil
                }
                return cell2
            }else{
                
                cell1.titleFCM.text = val.metrics_name!
                cell1.scoreValue.text = "\(val.by_analyst ?? 0)"
                if(val.sub_type == 1){
                    cell1.conditionLabel.text = " if < \(Double(val.score ?? "0")!)"
                }else {
                    cell1.conditionLabel.text = "if > \(Double(val.score ?? "0")!)"
                }
                //                if(val.by_analyst == 1){
                //                    cell1.backView.backgroundColor = primaryColor
                //                    self.setMetric.insert(indexPath.row)
                //                }else {
                //                    cell1.backView.backgroundColor = UIColor.white
                //                    self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
                //                }
                if ((val.by_player ?? 0) > 0){
                    cell1.backView.backgroundColor = primaryColor
                    cell1.titleFCM.textColor = .white
                    cell1.scoreValue.textColor = .white
                    cell1.conditionLabel.textColor = .white
                    
                    self.setMetric.insert(indexPath.row)
                    
                }else {
                    cell1.backView.backgroundColor = .white
                    cell1.titleFCM.textColor = primaryColor
                    cell1.scoreValue.textColor = primaryColor
                    cell1.conditionLabel.textColor = primaryColor
                    
                    self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
                }
                
                if(currentScreen != K_ALL_PLAYER_AGILITY_CARDS){
                    
                    cell1.isUserInteractionEnabled = true
                    cell1.cellClicked = {
                        if(K_CURRENT_USER == 5){
                            if(cell1.backView.backgroundColor == UIColor.green){
                                cell1.backView.backgroundColor = .white
                                self.userMetricData[indexPath.row].by_analyst = 0
                                self.recommendValue = ((self.recommendValue - Double(val.score ?? "0")!)*100).rounded()/100
                                
                                if(self.recommendValue >= 0){
                                    self.recommendedValue.text = "\(self.recommendValue)"
                                }else {
                                    self.recommendValue = 0
                                }
                                self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
                            }else {
                                self.recommendValue = ((self.recommendValue +  Double(val.score ?? "0")!)*100).rounded()/100
                                self.userMetricData[indexPath.row].by_analyst = 1
                                if(self.recommendValue >= 0){
                                    self.recommendedValue.text = "\(self.recommendValue)"
                                }else {
                                    self.recommendValue = 0
                                }
                                cell1.backView.backgroundColor = UIColor.green
                                self.setMetric.insert(indexPath.row)
                            }
                        }else if(K_CURRENT_USER == 8){
                            if(cell1.backView.backgroundColor == primaryColor){
                                cell1.backView.backgroundColor = .white
                                cell1.titleFCM.textColor = primaryColor
                                cell1.scoreValue.textColor = primaryColor
                                cell1.conditionLabel.textColor = primaryColor
                                
                                
                                
                                self.userMetricData[indexPath.row].by_player = 0
                                self.userMetricData[indexPath.row].ppa_planned = 0
                                self.planValue = ((self.planValue -  (Double(val.score ?? "0")! * Double(self.userMetricData[indexPath.row].by_player ?? 0)))*100).rounded()/100
                                
                                if(self.planValue >= 0){
                                    self.plannedValue.text = "\(self.planValue)"
                                }else {
                                    self.planValue = 0
                                }
                                self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
                            }else {
                                self.planValue = ((self.planValue + (Double(val.score ?? "0")! * Double(self.userMetricData[indexPath.row].by_player ?? 0)))*100).rounded()/100
                                if(self.planValue >= 0){
                                    self.plannedValue.text = "\(self.planValue)"
                                }else {
                                    self.planValue = 0
                                }
                                cell1.backView.backgroundColor = primaryColor
                                cell1.titleFCM.textColor = .white
                                cell1.scoreValue.textColor = .white
                                cell1.conditionLabel.textColor = .white
                                
                                self.userMetricData[indexPath.row].by_player = self.userMetricData[indexPath.row].by_analyst
                                self.userMetricData[indexPath.row].ppa_planned = self.userMetricData[indexPath.row].ppa_recommended
                                self.setMetric.insert(indexPath.row)
                            }
                        }
                    }
                }else {
                    
                    
                    cell1.isUserInteractionEnabled = false
                }
            }
            return cell1
        }else {
            let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell", for: indexPath) as! AgilityCardCollectionViewCell
            cell1.titleFCM.text = val.metrics_name!
            cell1.scoreValue.text = val.earned
            cell1.backView.backgroundColor = .white
            cell1.backView.borderWidth = 2
            if(val.succeed == 1){
                cell1.backView.borderColor = greenColor
            }else{
                cell1.backView.borderColor = .red
            }
            if(val.sub_type == 0){
                cell1.conditionLabel.text = "\(Double(val.score ?? "0")!) pts each"
            }else if(val.sub_type == 1){
                cell1.conditionLabel.text = " if < \(Double(val.score ?? "0")!)"
            }else {
                cell1.conditionLabel.text = "if > \(Double(val.score ?? "0")!)"
            }
            return cell1
        }
    }
    
    func addToMetric(id:Int?) {
        
    }
    
    func removeFromMetric(id:Int?) {
        self.selectedMetrices =  self.selectedMetrices.filter {$0.id! != id}
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((self.view.frame.width)/2)
        return CGSize(width:width , height: width)
    }
    
    
}
