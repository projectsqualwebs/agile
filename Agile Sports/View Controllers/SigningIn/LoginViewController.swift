//
//  LoginViewController.swift
//  Agile Sports
//
//  Created by AM on 19/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import Toaster

class LoginViewController: ParentViewController {
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextField.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
    }
    
    
    
    @IBAction func loginAction(_ sender: Any) {
        if(userNameTextField.text == nil || userNameTextField.text == "" ) {
            Singleton.shared.showToast(text: "Enter Username")
        }else if(!self.isValidEmail(emailStr: userNameTextField.text!)) {
            Singleton.shared.showToast(text: "Enter valid Username")
        }else if(passwordTextField.text == nil || passwordTextField.text == "" ) {
            Singleton.shared.showToast(text: "Enter Password")
        }else {
            ActivityIndicator.show(view: self.view)
            let param = [
                K_EMAIL:self.userNameTextField.text!,
                K_PASSWORD:self.passwordTextField.text!
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_LOGIN, method: .post, parameter: param, objectClass: Loginresponse.self, requestCode: U_LOGIN) { (response) in
                UserDefaults.standard.set(response.response?.token!, forKey:UD_TOKEN)
                let userInfo = try! JSONEncoder().encode(response.response!)
                Singleton.shared.userInfo = response.response!
                K_CURRENT_USER = (response.response?.user_type_id!)!
                UserDefaults.standard.set(userInfo, forKey: UD_USERINFO)
                 self.push(controller: "DashboardViewController")
                ActivityIndicator.hide()
                
            }
        }
    }
    
    
}

