//
//  AllPlayerViewController.swift
//  Agile Sports
//
//  Created by qw on 22/04/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class AllPlayerViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: IBOutlets
    @IBOutlet weak var playerTable: UITableView!
    
    var matchId = Int()
    var currentScreen = String()
    var sprintId = Int()
    var playerData = GetAllPlayerResponse()
    var tableData = [AllPlayerDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllPlayer()
    }
    
    
    func getAllPlayer(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ALL_PLAYER + "\(self.matchId)", method: .get, parameter: nil, objectClass: GetAllPlayer.self, requestCode: U_GET_ALL_PLAYER) { (response) in
            self.playerData = response.response
            self.tableData = []
            self.tableData.append(response.response.coach!)
            for val in response.response.players! {
                self.tableData.append(val)
            }
            self.playerTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.back()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AllPlayerCell") as! AllPlayerCell
        let val = self.tableData[indexPath.row]
         cell.playerName.text = (val.first_name ?? "") + " " + (val.last_name ?? "")
         cell.playerImage.sd_setImage(with: URL(string:
            (val.image ?? "")), placeholderImage: #imageLiteral(resourceName: "player"))
        cell.recommendedValue.text = "Recommended value: " + (val.recommended_value ?? "")
        cell.plannedValue.text = "Planned value: " + (val.planed_value ?? "")
        cell.actualValue.text = "Actual value: " + (val.actual_value ?? "")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleSprintViewController") as! ScheduleSprintViewController
        myVC.sprintId = self.sprintId
        myVC.matchId = self.matchId
        myVC.userId = self.tableData[indexPath.row].id ?? 0
        myVC.heading = (self.tableData[indexPath.row].first_name ?? "") + " " + (self.tableData[indexPath.row].last_name ?? "")
        myVC.currentScreen = self.currentScreen
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}


class AllPlayerCell: UITableViewCell {
    //MARK: IBOutlets
    @IBOutlet weak var playerImage: ImageView!
    @IBOutlet weak var playerName: DesignableUILabel!
    @IBOutlet weak var recommendedValue: DesignableUILabel!
    @IBOutlet weak var actualValue: DesignableUILabel!
    @IBOutlet weak var plannedValue: DesignableUILabel!
}
