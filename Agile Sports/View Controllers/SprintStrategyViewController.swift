//
//  SprintStrategyViewController.swift
//  Agile Sports
//
//  Created by qw on 25/04/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class SprintStrategyViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var sprintStrategyTable: UITableView!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    
    var matchId = 1//Int()
    var currentScreen = String()
    var sprintStrategyData = [GetSprintStrategyResponse]()
    var heading = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        sprintStrategyTable.estimatedRowHeight = 55.0
        sprintStrategyTable.rowHeight = UITableView.automaticDimension
        self.getSprintData()
    }
    
    func getSprintData() {
        ActivityIndicator.show(view: self.view)
        var url = String()
               if(self.currentScreen == K_SPRINT_STRATEGY){
                 url = U_BASE + U_GET_TEAM_MEMBERS + "4&matchId=\(matchId)"
                self.headingLabel.text = "Sprint Strategy"
               }else if(self.currentScreen == K_SCOUTING_REPORT){
                 url = U_BASE + U_GET_TEAM_MEMBERS + "5&matchId=\(matchId)"
                self.headingLabel.text = "Scouting Report"
               }else if(self.currentScreen == K_POST_GAME_NOTES){
                 url = U_BASE + U_GET_TEAM_MEMBERS + "2&matchId=\(matchId)"
                 self.headingLabel.text = heading
               }else if(self.currentScreen == K_SPRINT_REVIEW){
                 url = U_BASE + U_GET_TEAM_MEMBERS + "3&matchId=\(matchId)"
                self.headingLabel.text = "Sprint Review"
               }
        
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetSurveyResult.self, requestCode: U_GET_TEAM_MEMBERS) { (response) in
            self.sprintStrategyData = []
            for val in response.response {
                self.sprintStrategyData.append(GetSprintStrategyResponse(id: val.id ?? 0, category: val.category ?? "", question: val.question ?? "", status: val.status ?? "", answers: val.answers, collapsed: val.collapsed ?? false))
            }
            self.sprintStrategyTable.reloadData()
            ActivityIndicator.hide()
        }
        
    }
    
    //MARK:IBActions
    @IBAction func backAction(_ sender: Any) {
        self.back()
    }
    
}

extension SprintStrategyViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sprintStrategyData.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "\(section + 1). " + (self.sprintStrategyData[section].question ?? "")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       // if(sprintStrategyData.count < section){
          return sprintStrategyData[section].answers.count
            //sprintStrategyData[section].collapsed! ? 0:sprintStrategyData[section].answers.count
//        }else {
//            return 0
//        }
       
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SprintStrategyTable", for: indexPath) as! SprintStrategyTable
        let val = self.sprintStrategyData[indexPath.section].answers[indexPath.row]
        cell.userImage.sd_setImage(with: URL(string:(val.image ?? "")), placeholderImage: #imageLiteral(resourceName: "player"))
        cell.postedDate.text = val.comment_date
        cell.userName.text = (val.first_name ?? "") + (val.last_name ?? "")
        cell.userAnswer.text = val.answer
        return cell
    }
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        sprintStrategyData[indexPath.section].collapsed =  !sprintStrategyData[indexPath.section].collapsed!
//        sprintStrategyTable.beginUpdates()
//        sprintStrategyTable.endUpdates()
//        sprintStrategyTable.reloadSections([indexPath.section], with: .fade)
//
//    }

}

class SprintStrategyTable: UITableViewCell, UITextViewDelegate{
    //MARK: IBOutlets
    
    @IBOutlet weak var userImage: ImageView!
    @IBOutlet weak var postedDate: DesignableUILabel!
    @IBOutlet weak var userAnswer: DesignableUILabel!
    @IBOutlet weak var userName: DesignableUILabel!
    @IBOutlet var toggleButton: UIButton!
    @IBOutlet weak var answerTextView: UITextView!
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        NotificationCenter.default.post(name: NSNotification.Name(N_EDIT_SPRINT_STRATEGY), object: nil, userInfo:["tag":textView.tag,"text":textView.text ?? "","ispoints":false])
    }
    
}
