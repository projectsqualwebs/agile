


import UIKit
import SideMenu
import SDWebImage

class SideDrawerViewController: UIViewController{
    //MARK: IBOutlet
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var tableViewMenu: UITableView!
    
    var arrayMenu = [MenuObject]()
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewMenu.estimatedRowHeight = 44.0
        tableViewMenu.rowHeight = UITableView.automaticDimension
        NotificationCenter.default.addObserver(self, selector: #selector(self.logout), name: NSNotification.Name(N_LOGOUT_USER), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.userImage.sd_setImage(with: URL(string: (Singleton.shared.userInfo.image ?? "")), placeholderImage: #imageLiteral(resourceName: "player"))
        self.userName.text = Singleton.shared.userInfo.username
        tableViewMenu.isUserInteractionEnabled = true
        if(K_CURRENT_USER == 11){
            arrayMenu = [
                MenuObject(name: "Dashboard", items: ["Dashboard", "Sprint Schedule", "Metircs Log"]),MenuObject(name: "My Activity", items: ["Scouting Reports"]), MenuObject(name: "Post Game Survey", items: ["View Survey Results"]), MenuObject(name: "Sprint Strategy", items: ["View Sprint Strategy"]), MenuObject(name: "Scouting Reports", items: ["View Scouting Report"]), MenuObject(name: "Sprint Review", items: ["View Post Game Meeting Notes", "Sprint Review"]), MenuObject(name: "Logout", items: [])
            ]
        }else if(K_CURRENT_USER == 8){
            arrayMenu = [
                MenuObject(name: "Dashboard", items: ["Dashboard", "Sprint Schedule", "Metircs Log"]),MenuObject(name: "My Activity", items: ["Plan Agility Score", "Submit Post Game Survey"]), MenuObject(name: "Agility Cards", items: ["Player Agility Cards", "Statistics"]), MenuObject(name: "Post Game Survey", items: ["View Survey Results"]), MenuObject(name: "Sprint Strategy", items: ["View Sprint Strategy"]), MenuObject(name: "Scouting Reports", items: ["View Scouting Report"]), MenuObject(name: "Sprint Review", items: ["View Post Game Meeting Notes", "Sprint Review"]), MenuObject(name: "Logout", items: [])
            ]
        }else  if(K_CURRENT_USER == 7){
            arrayMenu = [
                MenuObject(name: "Dashboard", items: ["Dashboard", "Sprint Schedule", "Metircs Log"]),MenuObject(name: "My Activity", items: ["Submit Sprint Strategy", "Submit Post Game Survey","Post Game Meeting Notes","Submit Sprint Review","Scouting Report"]), MenuObject(name: "Agility Cards", items: ["Player Agility Cards", "Statistics"]), MenuObject(name: "Post Game Survey", items: ["View Survey Results"]), MenuObject(name: "Sprint Strategy", items: ["View Sprint Strategy"]), MenuObject(name: "Scouting Reports", items: ["View Scouting Report"]), MenuObject(name: "Sprint Review", items: ["View Post Game Meeting Notes", "Sprint Review"]), MenuObject(name: "Logout", items: [])
            ]
        }else  if(K_CURRENT_USER == 6){
            arrayMenu = [
                MenuObject(name: "Dashboard", items: ["Dashboard", "Sprint Schedule", "Metircs Log"]),MenuObject(name: "My Activity", items: ["Team Agility Card","Submit Sprint Strategy", "Submit Post Game Survey","Post Game Meeting Notes","Submit Sprint Review","Scouting Report"]), MenuObject(name: "Agility Cards", items: ["Player Agility Cards", "Statistics"]), MenuObject(name: "Post Game Survey", items: ["View Survey Results"]), MenuObject(name: "Sprint Strategy", items: ["View Sprint Strategy"]), MenuObject(name: "Scouting Report", items: ["View Scouting Report"]), MenuObject(name: "Sprint Review", items: ["View Post Game Meeting Notes", "Sprint Review"]), MenuObject(name: "Logout", items: [])
            ]
        }else if(K_CURRENT_USER == 5){
            arrayMenu = [
                MenuObject(name: "Dashboard", items: ["Dashboard", "Sprint Schedule", "Metircs Log"]),MenuObject(name: "My Activity", items: ["Manage Metrics", "Manage Sprints", "Set Recommended Values"]), MenuObject(name: "Agility Cards", items: ["Player Agility Cards", "Statistics"]), MenuObject(name: "Post Game Survey", items: ["View Survey Results"]), MenuObject(name: "Sprint Strategy", items: ["View Sprint Strategy"]), MenuObject(name: "Scouting Report", items: ["View Scouting Report"]), MenuObject(name: "Sprint Review", items: ["View Post Game Meeting Notes", "Sprint Review"]), MenuObject(name: "Logout", items: [])
            ]
        }
        tableViewMenu.reloadData()
    }
    
    //MARK: IBAction
    
    @objc func logout(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_LOGOUT_USER), object: nil)
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        UserDefaults.standard.removeObject(forKey: UD_USERINFO)
        UserDefaults.standard.removeObject(forKey: UD_TOKEN)
        tableViewMenu.isUserInteractionEnabled = true
        self.navigationController?.pushViewController(myVC, animated: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.logout), name: NSNotification.Name(N_LOGOUT_USER), object: nil)
    }
    
    @IBAction func showProfile(_ sender: Any) {
        
    }
}

extension SideDrawerViewController: UITableViewDataSource, CollapsibleTableViewHeaderDelegate {
    
    
    func toggleSection(header: CollapsibleTableViewHeader, section: Int) {
        let collapsed = !arrayMenu[section].collapsed
        
        // Toggle collapse
        arrayMenu[section].collapsed = collapsed
        header.setCollapsed(collapsed: collapsed)
        
        // Reload the whole section
        tableViewMenu.reloadSections(NSIndexSet(index: section) as IndexSet, with: .automatic)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 55
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrayMenu.count
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? CollapsibleTableViewHeader ?? CollapsibleTableViewHeader(reuseIdentifier: "header")
        let border = UIView(frame: CGRect(x: 0, y: 54, width: self.view.bounds.width, height: 1))
        
        border.backgroundColor = primaryColor
        header.titleLabel.text = arrayMenu[section].name
        header.titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        
        header.arrowLabel.text = "➤"
        header.arrowLabel.tintColor = blueColor
        header.setCollapsed(collapsed: arrayMenu[section].collapsed)
        header.section = section
        header.backgroundColor = .white
        header.addSubview(border)
        header.delegate = self
        return header
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrayMenu[(indexPath as NSIndexPath).section].collapsed ? 0 : UITableView.automaticDimension
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (arrayMenu[section].collapsed) ? 0 : arrayMenu[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell", for: indexPath) as! SideMenuTableViewCell
        cell.menuName.text = self.arrayMenu[indexPath.section].items[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableViewMenu.isUserInteractionEnabled = false
        switch indexPath.section {
        case 0:
            if(indexPath.row == 0){
                tableViewMenu.isUserInteractionEnabled = true
                self.dismiss(animated: true, completion: nil)
            }else if(indexPath.row == 1){
                tableViewMenu.isUserInteractionEnabled = true
                Singleton.shared.showToast(text: "Coming soon")
            }else if(indexPath.row == 2){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ManageMetricsViewController") as! ManageMetricsViewController
                tableViewMenu.isUserInteractionEnabled = true
                myVC.headingLabel = "Metrics Log"
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            
            break
        case 1:
            if(K_CURRENT_USER == 8){
                if(indexPath.row == 0){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_PLAN_AGILITY_SCORE
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 1){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_POST_GAME_SURVEY
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }else if(K_CURRENT_USER == 11){
                if(indexPath.row == 0){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_SUBMIT_SCOUTING_REPORT
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }else if(K_CURRENT_USER == 6){
                if(indexPath.row == 0){
                    tableViewMenu.isUserInteractionEnabled = true
                }else if(indexPath.row == 1){
                    
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = k_SUBMIT_SPRINT_STRATEGY
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 2){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_POST_GAME_SURVEY
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 3){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_SUBMIT_MEETING_NOTES
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 4){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_SUBMIT_SPRINT_REVIEW
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 5){
                    
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_SUBMIT_SCOUTING_REPORT
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }else if(K_CURRENT_USER == 7){
                if(indexPath.row == 0){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = k_SUBMIT_SPRINT_STRATEGY
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 1){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_POST_GAME_SURVEY
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 2){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_SUBMIT_MEETING_NOTES
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 3){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_SUBMIT_SPRINT_REVIEW
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 4){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_SUBMIT_SCOUTING_REPORT
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }else if(K_CURRENT_USER == 5){
                if(indexPath.row == 0){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ManageMetricsViewController") as! ManageMetricsViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.isMetricLog = true
                    myVC.headingLabel = "Manage Metrics"
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 1){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.isManageSprint = true
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 2){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    myVC.currentScreen = K_SET_RECOMMENDED_VALUE
                    tableViewMenu.isUserInteractionEnabled = true
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }
            break
        case 2:
            if(K_CURRENT_USER == 11){
                if(indexPath.row == 0){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_VIEW_SURVEY_RESULT
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }else {
                if(indexPath.row == 0){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_ALL_PLAYER_AGILITY_CARDS
                    self.navigationController?.pushViewController(myVC, animated: true)
                }else if(indexPath.row == 1){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_STATISTICS_SCREEN
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }
            break
        case 3:
             if(K_CURRENT_USER == 11){
                if(indexPath.row == 0){
                               let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                               tableViewMenu.isUserInteractionEnabled = true
                               myVC.currentScreen = K_SPRINT_STRATEGY
                               self.navigationController?.pushViewController(myVC, animated: true)
                           }
             }else {
                if(indexPath.row == 0){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_VIEW_SURVEY_RESULT
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
             }
            break
        case 4:
            if(K_CURRENT_USER == 11){
                if(indexPath.row == 0){
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                    tableViewMenu.isUserInteractionEnabled = true
                    myVC.currentScreen = K_SCOUTING_REPORT
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }else {
            if(indexPath.row == 0){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                tableViewMenu.isUserInteractionEnabled = true
                myVC.currentScreen = K_SPRINT_STRATEGY
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            }
            break
        case 5:
            if(K_CURRENT_USER == 11){
             if(indexPath.row == 0){
                 let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                 tableViewMenu.isUserInteractionEnabled = true
                 myVC.currentScreen = K_SPRINT_REVIEW
                 self.navigationController?.pushViewController(myVC, animated: true)
             }else if(indexPath.row == 1){
                 let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                 tableViewMenu.isUserInteractionEnabled = true
                 myVC.currentScreen = K_POST_GAME_NOTES
                 self.navigationController?.pushViewController(myVC, animated: true)
             }
            }else {
            if(indexPath.row == 0){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                tableViewMenu.isUserInteractionEnabled = true
                myVC.currentScreen = K_SCOUTING_REPORT
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            }
            break
            
        case 6:
            if(indexPath.row == 0){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                tableViewMenu.isUserInteractionEnabled = true
                myVC.currentScreen = K_SPRINT_REVIEW
                self.navigationController?.pushViewController(myVC, animated: true)
            }else if(indexPath.row == 1){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectSprintViewController") as! SelectSprintViewController
                tableViewMenu.isUserInteractionEnabled = true
                myVC.currentScreen = K_POST_GAME_NOTES
                self.navigationController?.pushViewController(myVC, animated: true)
            }
            break
        default:
            break
        }
    }
    
}
extension SideDrawerViewController: UITableViewDelegate {
    
    func showPopup(title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
            
        }
        let noBtn = UIAlertAction(title: "No", style: .default){
            (UIAlertAction) in
            self.dismiss(animated: true, completion: nil)
        }
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        present(alert, animated: true, completion: nil)
    }
}


class SideMenuTableViewCell: UITableViewCell {
    
    //MARK: IBOutlets
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var menuName: UILabel!
    
    @IBOutlet weak var toggleButton: UIButton!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

protocol CollapsibleTableViewHeaderDelegate {
    func toggleSection(header: CollapsibleTableViewHeader, section: Int)
}

class CollapsibleTableViewHeader: UITableViewHeaderFooterView {
    let titleLabel = UILabel()
    let arrowLabel = UILabel()
    var delegate: CollapsibleTableViewHeaderDelegate?
    var section: Int = 0
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        contentView.backgroundColor = .white
        
        let marginGuide = contentView.layoutMarginsGuide
        
        // Arrow label
        contentView.addSubview(arrowLabel)
        arrowLabel.textColor = primaryColor
        arrowLabel.translatesAutoresizingMaskIntoConstraints = false
        arrowLabel.widthAnchor.constraint(equalToConstant: 25).isActive = true
        arrowLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        arrowLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        arrowLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        
        // Title label
        contentView.addSubview(titleLabel)
        titleLabel.textColor = primaryColor
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        titleLabel.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        
        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.tapHeader(_:))))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    @objc func tapHeader(_ gestureRecognizer: UITapGestureRecognizer) {
        guard let cell = gestureRecognizer.view as? CollapsibleTableViewHeader else {
            return
        }
        if (cell.titleLabel.text == "Logout"){
            NotificationCenter.default.post(name: NSNotification.Name(N_LOGOUT_USER), object: nil)
        }else{
            delegate?.toggleSection(header: self, section: cell.section)
        }
        
    }
    
    func setCollapsed(collapsed: Bool) {
        // Animate the arrow rotation (see Extensions.swf)
        arrowLabel.rotate(collapsed ? 0.0 : .pi / 2)
    }
}

extension UILabel {
    func rotate(_ toValue: CGFloat, duration: CFTimeInterval = 0.2) {
        let animation = CABasicAnimation(keyPath: "transform.rotation")
        
        animation.toValue = toValue
        animation.duration = duration
        animation.isRemovedOnCompletion = false
        animation.fillMode = CAMediaTimingFillMode.forwards
        
        self.layer.add(animation, forKey: nil)
    }
}
