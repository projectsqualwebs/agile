//
//  ManageMetricsViewController.swift
//  Agile Sports
//
//  Created by qw on 27/04/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import iOSDropDown

class ManageMetricsViewController: UIViewController, SelectFromPicker {
    func selectedItem(name: String, id: Int) {
        self.selectedSprint.text = name
        self.sprintSelected = self.sprintData[id]
        self.getMetricData()
    }
    
    //MARK:IBOutlets
    @IBOutlet weak var metricTable: UICollectionView!
    @IBOutlet weak var viewForPopup: UIView!
    @IBOutlet weak var selectMetric: UITextField!
    @IBOutlet weak var viewAddNewMetric: UIView!
    @IBOutlet weak var newmetricName: UITextField!
    @IBOutlet weak var pointsPerLabel: DesignableUILabel!
    @IBOutlet weak var conditionalLabel: DesignableUILabel!
    @IBOutlet weak var viewPointsper: View!
    @IBOutlet weak var viewPointsConditional: UIView!
    @IBOutlet weak var viewLessthan: View!
    @IBOutlet weak var viewGreaterThan: View!
    @IBOutlet weak var nominator: DropDown!
    @IBOutlet weak var denominator: DropDown!
    @IBOutlet weak var headingPoints: UILabel!
    @IBOutlet weak var pointsTextField: UITextField!
    @IBOutlet weak var conditionPoint: UITextField!
    @IBOutlet weak var viewForCondition: UIView!
    @IBOutlet weak var popupHeading: DesignableUILabel!
    @IBOutlet weak var addButton: CustomButton!
    @IBOutlet weak var viewForSelectSprint: UIView!
    @IBOutlet weak var selectedSprint: DesignableUILabel!
    @IBOutlet weak var navigationTitle: DesignableUILabel!
    
    
    
    var selectMetrics = ["Select Metrics","Add New"]
    var selectedMetric = [String]()
    var metricData = [MatricesResponse]()
    var selectedSubtype = 0
    var selectedType = 1
    var sprintSelected = SprintResponse()
    var selectedMetricId = Int()
    var selectedNumerator = MatricesResponse()
    var selectedDenominator = MatricesResponse()
    var isMetricLog = false
    var sprintData = [SprintResponse]()
    var headingLabel = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationTitle.text = self.headingLabel
        if !(self.isMetricLog){
            self.viewAddNewMetric.isHidden = true
            self.viewForSelectSprint.isHidden = false
        }else {
            self.viewAddNewMetric.isHidden = false
            self.viewForSelectSprint.isHidden = true
        }
        self.selectedSprint.text = "Sprint All"
        self.sprintData = Singleton.shared.sprintData
        
        self.nominator.didSelect { (string, val, id) in
            self.selectedNumerator = self.metricData[val]
            if(self.selectedDenominator.metrics_value != nil){
                let value = (((Double(self.selectedNumerator.metrics_value ?? "0")!/Double(self.selectedDenominator.metrics_value ?? "0")!) ?? 0)*100)
                self.conditionPoint.text = "\(value.rounded())"
            }
        }
        self.denominator.didSelect { (string, val, id) in
            self.selectedDenominator = self.metricData[val]
            if(self.selectedNumerator.metrics_value != nil){
                let value = (((Double(self.selectedNumerator.metrics_value ?? "0")!/Double(self.selectedDenominator.metrics_value ?? "0")!))*100)
                self.conditionPoint.text = "\(value.rounded())"
            }
        }
        self.viewForPopup.isHidden = true
        self.getMetricData()
    }
    
    func initView(){
        self.newmetricName.text = ""
        self.conditionAction(self)
        self.lessthanAction(self)
        self.denominator.text = ""
        self.selectedNumerator = MatricesResponse()
        self.selectedDenominator = MatricesResponse()
        self.conditionPoint.text = ""
        self.pointsTextField.text = ""
        self.nominator.text = ""
    }
    
    func getMetricData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_METRICS + "?sprint_no=\(self.sprintSelected.id ?? 0)", method: .get, parameter: nil, objectClass: GetMatrices.self, requestCode: U_GET_METRICS) { (response) in
            self.metricData = response.response!
            self.selectMetrics = []
            for val in response.response!{
                if(val.type == 2){
                    self.selectMetrics.append(val.metrics_name ?? "")
                }
            }
            self.nominator.optionArray = self.selectMetrics
            self.denominator.optionArray = self.selectMetrics
            self.metricTable.delegate = self
            self.metricTable.dataSource = self
            self.metricTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.back()
    }
    
    @IBAction func selectSprintAction(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.pickerData = []
        myVC.pickerDelegate = self
        myVC.pickerHeading = "Select Sprint"
        for val in self.sprintData{
            myVC.pickerData.append("SPRINT" + (val.sprint_no ?? ""))
        }
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func addNewAction(_ sender: Any) {
        self.popupHeading.text = "Add Metrics"
        self.viewForPopup.isHidden = false
        self.addButton.setTitle("ADD", for: .normal)
    }
    
    @IBAction func hidePopupAction(_ sender: Any) {
        self.initView()
        self.viewForPopup.isHidden = true
    }
    
    @IBAction func lessthanAction(_ sender: Any) {
        selectedSubtype = 1
        self.viewLessthan.backgroundColor = blueColor
        self.viewGreaterThan.backgroundColor = .white
    }
    
    @IBAction func greaterThanAction(_ sender: Any) {
        selectedSubtype = 2
        self.viewLessthan.backgroundColor = .white
        self.viewGreaterThan.backgroundColor = blueColor
    }
    
    @IBAction func conditionAction(_ sender: Any) {
        selectedSubtype = 1
        selectedType = 1
        self.conditionalLabel.textColor = .white
        self.pointsPerLabel.textColor = blueColor
        self.viewPointsConditional.backgroundColor = blueColor
        self.viewPointsper.backgroundColor = .white
        self.viewForCondition.isHidden = false
        self.conditionPoint.text = ""
        self.headingPoints.text = "Points Earned"
    }
    
    @IBAction func pointsperAction(_ sender: Any) {
        selectedType = 2
        self.conditionalLabel.textColor = blueColor
        self.pointsPerLabel.textColor = .white
        self.viewPointsConditional.backgroundColor = .white
        self.viewPointsper.backgroundColor = blueColor
        self.viewForCondition.isHidden = true
        self.conditionPoint.text = ""
        self.headingPoints.text = "Enter points per action"
    }
    
    @IBAction func addMetricAction(_ sender: Any) {
        if(selectMetric.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter metric name")
        }else  if(conditionalLabel.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter metric value")
        }else {
            var url = String()
            var  param = [String:Any]()
            self.viewForPopup.isHidden = true
            ActivityIndicator.show(view: self.view)
            if(self.addButton.titleLabel?.text == "ADD"){
                url = U_BASE + U_ADD_METRIC
                param =  [
                    "metrics_name":self.newmetricName.text ?? "",
                    "type": self.selectedType,
                    "sub_type":selectedSubtype,
                    "metrics_value":self.conditionPoint.text ?? "",
                    "score": self.pointsTextField.text ?? "",
                    "numerator_metrics":self.nominator.text ?? "",
                    "denominator_metrics":self.denominator.text ?? "",
                ]
            }else {
                url = U_BASE + U_EDIT_METRIC
                param =  [
                    "id": self.selectedMetricId,
                    "metrics_name":self.newmetricName.text ?? "",
                    "type": self.selectedType,
                    "sub_type":selectedSubtype,
                    "metrics_value":self.conditionPoint.text ?? "",
                    "score": self.pointsTextField.text ?? "",
                    "numerator_metrics":self.nominator.text ?? "",
                    "denominator_metrics":self.denominator.text ?? "",
                ]
            }
            SessionManager.shared.methodForApiCalling(url: url, method: .post, parameter: param, objectClass: Response.self, requestCode: url) { (response) in
                if(self.addButton.titleLabel?.text == "ADD"){
                    Singleton.shared.showToast(text: "Metrics Added Successfully")
                }else {
                    Singleton.shared.showToast(text: "Metrics Updated Successfully")
                }
                self.getMetricData()
                self.initView()
                ActivityIndicator.hide()
            }
        }
        
    }
    
}

extension ManageMetricsViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.metricData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = metricTable.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell2", for: indexPath) as! AgilityCardCollectionViewCell2
        
        let val = self.metricData[indexPath.row]
        cell.titleFCM.text = val.metrics_name
        cell.scoreValue.text = val.score
        if(K_CURRENT_USER == 8){
            cell.viewEditMetrices.isHidden = true
        }else {
            cell.viewEditMetrices.isHidden = false
        }
        if(val.type == 2){
            cell.conditionLabel.text = "Point Each"
        }else if(val.type == 1){
            if(val.sub_type == 1 || val.sub_type == 0){
                cell.conditionLabel.text = "if < \(val.metrics_value!)"
            }else if(val.sub_type == 2){
                cell.conditionLabel.text = "if > \(val.metrics_value!)"
            }
        }
        cell.minusButton = {
            
            if(val.type == 1){
                self.conditionAction(self)
                if(val.sub_type == 1){
                    self.lessthanAction(self)
                }else if(val.sub_type == 2){
                    self.greaterThanAction(self)
                }
                let num = self.metricData.filter{
                    $0.id == val.numerator_metrics
                }
                if(num.count > 0){
                    self.selectedNumerator =  num[0]
                    self.nominator.text = self.selectedNumerator.metrics_name
                }
                
                let denom = self.metricData.filter{
                    $0.id == val.denominator_metrics
                }
                if(denom.count > 0){
                    self.selectedDenominator =  denom[0]
                    self.denominator.text = self.selectedDenominator.metrics_name
                }
                
            }else if(val.type == 2){
                self.pointsperAction(self)
            }
            self.popupHeading.text = "Update Metrics"
            self.addButton.setTitle("UPDATE", for: .normal)
            self.newmetricName.text = val.metrics_name
            self.conditionPoint.text = val.metrics_value
            self.pointsTextField.text = val.score
            self.selectedMetricId = val.id ?? 0
            self.viewForPopup.isHidden = false
        }
        cell.plusButton = {
            let alert = UIAlertController(title: "Are you sure?", message: "You want to delete this Metrics? It will be deleted from assigned metrics also(if assigned in any).", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Yes", style: .default) { (action) in
                ActivityIndicator.show(view: self.view)
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_ENTRIES, method: .post, parameter: ["row_id": val.id ?? 0, "table": "metrics"], objectClass: Response.self, requestCode: U_DELETE_ENTRIES) { (response) in
                    Singleton.shared.showToast(text: "Metrices deleted Successfully")
                    ActivityIndicator.hide()
                    self.getMetricData()
                }
            }
            
            let noAction = UIAlertAction(title: "No", style: .cancel) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            
            alert.addAction(yesAction)
            alert.addAction(noAction)
            self.present(alert, animated: true, completion: nil)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((self.view.frame.width)/2)
        return CGSize(width:width , height: width)
    }
    
}
