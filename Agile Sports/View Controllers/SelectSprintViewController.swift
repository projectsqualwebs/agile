//
//  SelectPlayerViewController.swift
//  Agile Sports
//
//  Created by qw on 20/04/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class SelectSprintViewController: ParentViewController, UIActionSheetDelegate {
    //MARK: IBOutlets
    @IBOutlet weak var sprintCount: DesignableUILabel!
    @IBOutlet weak var sprintTable: UITableView!
    
    var sprintData = [SprintResponse]()
    var currentScreen = String()
    var isManageSprint = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.sprintTable.estimatedRowHeight = 90
        self.sprintTable.rowHeight = UITableView.automaticDimension
        if(Singleton.shared.sprintData.count == 0){
            self.getSprintData { (response) in
                self.sprintData = response.response
                self.sprintTable.reloadData()
            }
        }else {
            self.sprintData = Singleton.shared.sprintData
            self.sprintTable.reloadData()
        }
    }
    
    func deleteSprint(id: Int){
        let actionSheet = UIAlertController(title: "Are you sure", message: "You want to delete this sprint.", preferredStyle: .alert)
        actionSheet.addAction(UIAlertAction(title: "No", style: .default , handler:{ (UIAlertAction)in
            self.dismiss(controller: self)
        }))
        actionSheet.addAction(UIAlertAction(title: "Yes", style: .default , handler:{ (UIAlertAction)in
            ActivityIndicator.show(view: self.view)
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_ENTRIES, method: .post, parameter: ["row_id":id, "table": "sprints"], objectClass: Response.self, requestCode: U_DELETE_ENTRIES) { (response) in
                Singleton.shared.showToast(text: "Metrices deleted Successfully")
                ActivityIndicator.hide()
                
            }
        }))
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.back()
    }
    
    
}

extension SelectSprintViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.sprintCount.text = "Total Sprint: " + "\(self.sprintData.count)"
        return self.sprintData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SprintTableViewCell") as! SprintTableViewCell
        let val = self.sprintData[indexPath.row]
        if(self.isManageSprint){
            cell.viewOptionButton.isHidden = false
        }
        cell.leagueName.text = "Sprint " + (val.sprint_no ?? "0")
        cell.teamName.text = "Start Date: " + self.convertTimestampToDate(val.start_date ?? 0, to: "dd MMM yyyy")
        cell.duration.text = "End Date: " + self.convertTimestampToDate(val.end_date ?? 0, to: "dd MMM yyyy")
        cell.status.text = "Total Games: " +  "\(val.match?.count ?? 0)"
        cell.optionButton={
            let actionSheet = UIAlertController(title: "Choose Option", message: nil, preferredStyle: .actionSheet)
            actionSheet.addAction(UIAlertAction(title: "Edit", style: .default , handler:{ (UIAlertAction)in
                self.dismiss(animated: true) {
                    let myVC = self.storyboard?.instantiateViewController(withIdentifier: "EditSprintViewController") as! EditSprintViewController
                    myVC.sprintId = val.id ?? 0
                    self.navigationController?.pushViewController(myVC, animated: true)
                }
            }))
            actionSheet.addAction(UIAlertAction(title: "Delete", style: .default , handler:{ (UIAlertAction)in
                self.deleteSprint(id:val.id ?? 0)
            }))
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .default , handler:{ (UIAlertAction)in
                print("User click Approve button")
            }))
            self.present(actionSheet, animated: true, completion: nil)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !(self.isManageSprint){
            if(currentScreen == k_SUBMIT_SPRINT_STRATEGY || currentScreen == K_SUBMIT_SPRINT_REVIEW){
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SubmitSprintStrategyController") as! SubmitSprintStrategyController
                myVC.sprintId = self.sprintData[indexPath.row].id ?? 0
                myVC.currentScreen = self.currentScreen
                self.navigationController?.pushViewController(myVC, animated: true)
            }else {
                let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectGameViewController") as! SelectGameViewController
                myVC.sprintNumber = self.sprintData[indexPath.row].sprint_no ?? "0"
                myVC.matchData = self.sprintData[indexPath.row].match!
                myVC.sprintId = self.sprintData[indexPath.row].id ?? 0
                myVC.currentScreen = self.currentScreen
                self.navigationController?.pushViewController(myVC, animated: true)
            }
        }
    }
}



