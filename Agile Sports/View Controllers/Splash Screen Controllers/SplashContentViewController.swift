//
//  SplashContentViewController.swift
//  Bee Tutor
//
//  Created by Manisha  Sharma on 12/04/2019.
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit

class SplashContentViewController: ParentViewController {
    
    //MARK: IBOutlet
  //  @IBOutlet weak var backgroundImage: UIImageView!
   // @IBOutlet weak var image2: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
   // @IBOutlet weak var labelContent: UILabel!
    @IBOutlet weak var viewForButton: UIStackView!
    
    var content: SplashContent?
    var index: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if index == 1{
            self.labelTitle.text = "Create Team Synergy"
        }else if index == 0{
            self.labelTitle.text = "Execute Game Plan"
        }
        if index == 2 {
            self.labelTitle.text = "Increase Player IQ"
            viewForButton.isHidden = false
        } else {
            viewForButton.isHidden = true
        }
 
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    
    @IBAction func startAction(_ sender: Any) {
        self.push(controller: "LoginViewController")
    }
    
}
