//
//  SplashViewController.swift
//  Bee Tutor
//
//  Created by Manisha  Sharma on 12/04/2019.
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit

class SplashViewController: ParentViewController {
    
    //MARK: IBOutlet
    var currentIndex: Int = 0
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // handleTutorRegistration()
        SinglePageViewController.indexDelegate = self
    }

}
extension SplashViewController: PageContentIndexDelegate {
    func getContentIndex(index: Int) {
        pageControl.currentPage = (currentIndex == 0) ? index:currentIndex
    }
}
