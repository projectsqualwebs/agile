//
//  ImagesPageViewController.swift
//  Ezhal
//
//  Created by Manisha  Sharma on 11/01/2018.
//  Copyright © 2018 Qualwebs. All rights reserved.
//

import UIKit
protocol PageContentIndexDelegate {
    func getContentIndex(index: Int)
}

class SinglePageViewController: UIPageViewController {
    
    static var dataSource1: UIPageViewControllerDataSource?
    static var indexDelegate: PageContentIndexDelegate? = nil
    static var controller: String = K_SPLASH
    
    var currentIndex: Int = 0
    var arrayContent: [SplashContent]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrayContent = [SplashContent]()
        
            arrayContent = [SplashContent(backgroundImage: "splash1", icon: "1", title: "Connecting Tutors and Students ", content: "Bee Tutor is an easy and convenient platform optimized for Tutors and Tutees to connect."),
                            SplashContent(backgroundImage: "splash2", icon: "2", title: "Quality Tutors at Competitive Rates", content: "Bee Tutor strives to provide responsible tutors dedicated to your child’s learning at affordable rates."),
                            SplashContent(backgroundImage: "splash3", icon: "3", title: "Tailored to your preferences", content: "Bee Tutor’s comprehensive and intuitive Tutor search and filter function lets you choose your ideal Tutor (e.g location, subjects, gender, days per week, rates.. etc.)")]
        
        self.dataSource = self
        self.delegate = self
        
        getContent()
        SinglePageViewController.dataSource1 = self
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        SinglePageViewController.dataSource1 = self
    }
    
    func getContent() {
        self.setViewControllers([contentAtIndex(index: currentIndex)!], direction: .forward, animated: false, completion: nil)
    }
    
    func contentAtIndex(index: Int) -> UIViewController? {
        if (arrayContent?.count == 0) || (index >= (arrayContent?.count)!) {
            return nil
        }
            let splashContent = self.storyboard?.instantiateViewController(withIdentifier: "SplashContentViewController") as! SplashContentViewController
            guard let content = arrayContent as? [SplashContent] else { return nil }
            splashContent.content = content[index]
            splashContent.index = index
            return splashContent
    }
    
    func viewAtIndex(_ viewController: UIViewController, next: Bool) -> UIViewController? {
        var index:Int = 0
            let pageContent: SplashContentViewController = viewController as! SplashContentViewController
            index = pageContent.index
     
        
        if next {
            if (index == NSNotFound) {
                return nil
            }
            index += 1
            if index == arrayContent?.count {
                return nil
            }
        } else {
            if ((index == 0) || (index == NSNotFound)) {
                return nil
            }
            index -= 1
        }
        return contentAtIndex(index: index)
    }
}
extension SinglePageViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return viewAtIndex(viewController, next: false)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return viewAtIndex(viewController, next: true)
    }
}
extension SinglePageViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
            let currentVC: SplashContentViewController = pageViewController.viewControllers?[0] as! SplashContentViewController
            currentIndex = currentVC.index
        SinglePageViewController.indexDelegate?.getContentIndex(index: currentIndex)
    }
}

extension UIPageViewController {
    func goToPage(animated: Bool = true, completion: ((Bool) -> Void)? = nil, next: Bool) {
        if let currentViewController = viewControllers?[0] {
            if next{
                if let nextPage = SinglePageViewController.dataSource1?.pageViewController(self, viewControllerAfter: currentViewController) {
                    setViewControllers([nextPage], direction: .forward, animated: animated, completion: completion)
                }
            }else{
                if let previousPage = SinglePageViewController.dataSource1?.pageViewController(self, viewControllerBefore: currentViewController) {
                    setViewControllers([previousPage], direction: .reverse, animated: animated, completion: completion)
                }
            }
        }
    }
    
    func removeSwipeGesture(){
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
}

