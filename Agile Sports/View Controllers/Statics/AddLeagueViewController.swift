//
//  AddLeagueViewController.swift
//  Agile Sports
//
//  Created by AM on 09/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

protocol LeagueAdded {
    func refreshLeagueTable()
}

class AddLeagueViewController: ParentViewController, UITextFieldDelegate {
    
    //IBOutlets
    @IBOutlet weak var leagueLocation: SkyFloatingLabelTextField!
    @IBOutlet weak var leagueName: SkyFloatingLabelTextField!
    @IBOutlet weak var addLeagueButton: CustomButton!
    @IBOutlet weak var addLeagueLabel: DesignableUILabel!
    
    var leagueDelegate: LeagueAdded? = nil
    var isEdit = false
    var leagueInfo = LeagueData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        leagueLocation.delegate = self
        leagueName.delegate = self
        if(isEdit){
            self.leagueName.text = leagueInfo.name
            self.addLeagueLabel.text = "Edit League Details"
            self.addLeagueButton.setTitle("UPDATE LEAGUE", for: .normal)
            self.leagueLocation.text = leagueInfo.location
        }
    }
    
    @IBAction func addLeagueAction(_ sender: Any) {
        if(leagueName.text!.isEmpty){
            leagueName.errorMessage = "Enter League Name"
        }else if(leagueLocation.text!.isEmpty){
            leagueLocation.errorMessage = "Enter Location"
        }else {
            if(self.isEdit){
                let param = [
                    "id":self.leagueInfo.id,
                    "name":self.leagueName.text,
                    "location":self.leagueLocation.text,
                    "admin_name":self.leagueInfo.admin_name
                    ] as? [String:Any]
                
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_EDIT_LEAGUE, method: .post, parameter: param, objectClass: Response.self, requestCode: U_EDIT_LEAGUE) { (response) in
                    Singleton.shared.showToast(text: response.message ?? "")
                    self.leagueDelegate?.refreshLeagueTable()
                    self.navigationController?.popViewController(animated: true)
                }
            }else {
                let param = [
                    "name":leagueName.text ?? "",
                    "location":leagueLocation.text ?? "",
                    ] as! [String:Any]
                
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_LEAGUE, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ADD_LEAGUE) { (response) in
                    self.showAlert(title: nil, message: response.message!, action1Name: "Ok", action2Name: nil)
                    self.leagueName.text = ""
                    self.leagueLocation.text = ""
                    self.leagueDelegate?.refreshLeagueTable()
                    self.navigationController?.popViewController(animated: true)
                }}
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        (textField as! SkyFloatingLabelTextField).errorMessage = ""
    }
    
}
