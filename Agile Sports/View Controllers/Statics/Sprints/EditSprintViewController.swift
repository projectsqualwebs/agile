//
//  EditSprintViewController.swift
//  Agile Sports
//
//  Created by qw on 09/07/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import iOSDropDown

class EditSprintViewController: UIViewController, SelectDate {
    func selectedDate(date: Int) {
        if(self.currentPicker == 1){
            self.sDate = date
            self.startDate.text = self.convertTimestampToDate(date, to: "dd/MM/yyyy")
        }else {
            self.eDate = date
            self.endDate.text = self.convertTimestampToDate(date, to: "dd/MM/yyyy")
        }
    }
    
    //MARK:IBOutlets
    @IBOutlet weak var leagueName: SkyFloatingLabelTextField!
    @IBOutlet weak var teamName: SkyFloatingLabelTextField!
    @IBOutlet weak var sprintNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var startDate: SkyFloatingLabelTextField!
    @IBOutlet weak var endDate: SkyFloatingLabelTextField!
    @IBOutlet weak var metricCollection: UICollectionView!
    @IBOutlet weak var multiSelectionView: UIView!
    @IBOutlet weak var multiSelectionTable: ContentSizedTableView!
    
    
    @IBOutlet weak var sprintStatus: DropDown!
    @IBOutlet weak var opponentTeam: DropDown!
    @IBOutlet weak var metricsName: DropDown!
    
    var sprintId = Int()
    var sprintData = GetSingleSprintsResponse()
    var metricesData = [MatricesResponse]()
    var matchData = [SingleMatchResponse]()
    var currentTableType = Int()
    var currentPicker = Int()
    var sDate = Int()
    var eDate = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getSingleSprint()
        NotificationCenter.default.addObserver(self, selector: #selector(self.collectionValueChanged), name: NSNotification.Name(N_UPDATE_COLLECTION_FIELDS), object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_UPDATE_COLLECTION_FIELDS), object: nil)
    }
    
    @objc func collectionValueChanged(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_UPDATE_COLLECTION_FIELDS), object: nil)
        if let data = notif.userInfo {
            //  ["tag":textField.tag,"text":textField.text ?? "","ispoints":true]
            if((data["ispoints"] as! Bool) == true){
                self.metricesData[data["tag"] as! Int].score = data["text"] as! String
            }else {
                self.metricesData[data["tag"] as! Int].metrics_value = data["text"] as! String
            }
            self.metricCollection.reloadData()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.collectionValueChanged), name: NSNotification.Name(N_UPDATE_COLLECTION_FIELDS), object: nil)
        
    }
    
    func getSingleSprint(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SINGLE_SPRINT + "\(sprintId)", method: .get, parameter: nil, objectClass: GetSingleSprints.self, requestCode: U_GET_SINGLE_SPRINT) { (response) in
            self.sprintData = response.response
            ActivityIndicator.hide()
            self.getMetricData()
            self.initView()
        }
    }
    
    func getMetricData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_METRICS + "?sprint_no=\(sprintId)", method: .get, parameter: nil, objectClass: GetMatrices.self, requestCode: U_GET_METRICS) { (response) in
            
            if((response.response?.count ?? 0) > 0){
                self.metricesData = []
                for val in response.response! {
                    var  metric = MatricesResponse()
                    metric = val
                    if((self.sprintData.metrics?.count ?? 0) > 0){
                    for  data in self.sprintData.metrics!{
                        if((data.item_text ?? "") == (val.metrics_name ?? "")){
                            metric.isSelected = 1
                            break
                        }else {
                            metric.isSelected = 0
                        }
                        metric.item_id = data.id
                    }
                    }
                    self.metricesData.append(metric)
                }
                self.metricCollection.reloadData()
            }
            ActivityIndicator.hide()
        }
    }
    
    func initView(){
        self.leagueName.text = self.sprintData.league?.name
        self.teamName.text = self.sprintData.team?.name
        self.sprintNumber.text = self.sprintData.sprint_no
        self.opponentTeam.text = self.sprintData.team?.name
        self.startDate.text = self.convertTimestampToDate(self.sprintData.start_date ?? 0, to: "dd/MM/yyyy")
        self.endDate.text = self.convertTimestampToDate(self.sprintData.end_date ?? 0, to: "dd/MM/yyyy")
        self.sDate = self.sprintData.start_date ?? 0
        self.eDate = self.sprintData.end_date ?? 0
        self.sprintStatus.text = self.sprintData.status
        if((self.sprintData.matches?.count ?? 0) > 0){
            self.matchData = []
            for val in 0..<self.sprintData.matches!.count{
                var a = self.sprintData.matches![val]
                if(self.sprintData.matches![val].team_name == self.sprintData.team_name){
                    a.isSelected = 1
                }else {
                    a.isSelected = 0
                }
                if(val == 0){
                    a.match_date = self.sprintData.start_date ?? 0
                }else if(val == (self.sprintData.matches!.count-1)){
                    a.match_date = self.sprintData.end_date ?? 0
                }
                a.item_text = self.sprintData.matches![val].team_name
                self.matchData.append(a)
            }
        }
    }
    
    
    //MARK:IBActions
    @IBAction func backAction(_ sender: Any) {
        self.back()
    }
    
    @IBAction func teamAction(_ sender: Any) {
        self.currentTableType = 1
        self.multiSelectionView.isHidden = false
        self.multiSelectionTable.reloadData()
    }
    
    @IBAction func metricesAction(_ sender: Any) {
        self.currentTableType = 2
        self.multiSelectionView.isHidden = false
        self.multiSelectionTable.reloadData()
    }
    
    @IBAction func doneAction(_ sender: Any) {
        self.multiSelectionView.isHidden = true
        self.sprintData.metrics = []
        for val in self.metricesData {
            var count = 0
            var myVal = val
            myVal.item_text = val.metrics_name
            myVal.item_id = val.id
            
            if(val.isSelected == 1){
                count = count + 1
                self.sprintData.metrics?.append(myVal)
                if(count > 1){
                    self.metricsName.text = (val.metrics_name ?? val.item_text ?? "") + " and more"
                    break
                }else {
                    self.metricsName.text = (val.metrics_name ?? val.item_text ?? "")
                }
            }
        }
        
        for val in matchData {
            var count = 0
            if(val.isSelected == 1){
                count = count + 1
                if(count > 1){
                    break
                    self.opponentTeam.text = (val.team_name ?? "") + " and more"
                }else if(count == 1) {
                    self.opponentTeam.text = (val.team_name ?? "")
                }
            }
        }
        self.metricCollection.reloadData()
    }
    
    @IBAction func updateAction(_ sender: Any) {
        let matches = self.matchData.filter{
            $0.isSelected == 1
        }
        var matchJson = [Dictionary<String,Any>]()
        var metricJson = [Dictionary<String,Any>]()
       var teamJson = Dictionary<String,Any>()
        var leagueJson = Dictionary<String,Any>()
        let encoder = JSONEncoder()
                       do {
                        matchJson =  try JSONSerialization.jsonObject(with: encoder.encode(matches), options : .allowFragments) as! [Dictionary<String,Any>]
                        metricJson =  try JSONSerialization.jsonObject(with: encoder.encode(self.sprintData.metrics!), options : .allowFragments) as! [Dictionary<String,Any>]
                        teamJson =  try JSONSerialization.jsonObject(with: encoder.encode(self.sprintData.team!), options : .allowFragments) as! Dictionary<String,Any>
                        leagueJson =  try JSONSerialization.jsonObject(with: encoder.encode(self.sprintData.league!), options : .allowFragments) as! Dictionary<String,Any>
                       }catch {
                           print(error.localizedDescription)
                       }
        
       
            let param:[String:Any] = [
                "id":self.sprintData.id,
                "sprint_no":self.sprintData.sprint_no,
                "league_id":self.sprintData.league_id,
                "team_id":self.sprintData.team_id,
                "start_date":self.sDate,
                "end_date":self.eDate,
                "status":self.sprintData.status,
                "league_name":self.leagueName.text,
                "match":matchJson,
                "metrics":metricJson,
                "team_name":self.teamName.text,
                "team":teamJson,
                "league":leagueJson,
            ]
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_EDIT_SPRINT, method: .post, parameter: param, objectClass: Response.self, requestCode: U_EDIT_SPRINT) { (response) in
            Singleton.shared.showToast(text: "Sprint Updated Successfully.")
            self.getSingleSprint()
        }
    }
    
    @IBAction func startDateAction(_ sender: Any) {
        self.currentPicker = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.minDate = Date()
        myVC.dateDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
    
    @IBAction func endDateAction(_ sender: Any) {
        self.currentPicker = 2
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "DatePickerViewController") as! DatePickerViewController
        myVC.minDate = Date()
        myVC.dateDelegate = self
        myVC.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(myVC, animated: true, completion: nil)
    }
}

extension EditSprintViewController: UICollectionViewDelegate, UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource, UICollectionViewDelegateFlowLayout  {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sprintData.metrics?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell", for: indexPath) as! AgilityCardCollectionViewCell
        let val = self.sprintData.metrics?[indexPath.row]
        cell.titleFCM.text = val?.metrics_name ?? val?.item_text
        cell.scoreValueField.text = val?.metrics_value
        cell.pointsPerAction.text = val?.score
        cell.scoreValueField.delegate = cell
        cell.pointsPerAction.delegate = cell
        cell.scoreValueField.tag = indexPath.row
        cell.pointsPerAction.tag = indexPath.row
        if(val?.type == 2){
            cell.conditionLabel.text = "Enter Points Per Action"
            cell.viewPointsEarned.isHidden = true
        }else if(val?.type == 1){
            cell.viewPointsEarned.isHidden = false
            if(val?.sub_type == 1 || val?.sub_type == 0){
                cell.conditionLabel.text = "if less than"
            }else if(val?.sub_type == 2){
                cell.conditionLabel.text = "if greater than"
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150, height: 240)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(currentTableType == 1){
            return matchData.count
        }else {
            return metricesData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MultiSelectionCell", for: indexPath) as! MultiSelectionCell
        if(currentTableType == 1){
            let val = self.matchData[indexPath.row]
            if(val.isSelected == 1){
                cell.cellImage.image = #imageLiteral(resourceName: "GreyTick")
            }else {
                cell.cellImage.image = #imageLiteral(resourceName: "GreyBox")
            }
            cell.lblCategories.text = (val.team_name ?? "") + " - " +  (val.date ?? "")
        }else {
            let val = self.metricesData[indexPath.row]
            if(val.isSelected == 1){
                cell.cellImage.image = #imageLiteral(resourceName: "GreyTick")
            }else {
                cell.cellImage.image = #imageLiteral(resourceName: "GreyBox")
            }
            cell.lblCategories.text = val.metrics_name
        }
        cell.tickButton = {
            if(cell.cellImage.image == #imageLiteral(resourceName: "GreyTick")){
                cell.cellImage.image = #imageLiteral(resourceName: "GreyBox")
                if(self.currentTableType == 1){
                    self.matchData[indexPath.row].isSelected = 0
                }else {
                    self.metricesData[indexPath.row].isSelected = 0
                }
            }else{
                cell.cellImage.image = #imageLiteral(resourceName: "GreyTick")
                if(self.currentTableType == 1){
                    self.matchData[indexPath.row].isSelected = 1
                }else {
                    self.metricesData[indexPath.row].isSelected = 1
                }
            }
        }
        return cell
    }
}


class MultiSelectionCell: UITableViewCell, UITextFieldDelegate{
    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var cellImage: UIImageView!
    
    var tickButton:(()-> Void)? = nil
    
    @IBAction func ticlAction(_ sender: Any) {
        if let tickButton = self.tickButton{
            tickButton()
        }
    }
    
}
