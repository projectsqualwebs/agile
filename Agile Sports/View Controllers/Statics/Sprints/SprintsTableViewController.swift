//
//  SprintsTableViewController.swift
//  Agile Sports
//
//  Created by AM on 05/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class SprintsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
       getSprints()
    }

    // MARK: - Table view data source
    func getSprints() {
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MATRICES, method: .get, parameter: nil, objectClass: Response.self, requestCode: U_GET_MATRICES, userToken: nil) { (response) in
            print(response)
        }
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

}


class SprintTableViewCell: UITableViewCell {
    
    
}
