//
//  SprintTableViewCell.swift
//  Agile Sports
//
//  Created by AM on 12/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class SprintTableViewCell: UITableViewCell {

    //MARK:IBOutlets
    
    @IBOutlet weak var leagueName: UILabel!
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var sprintName: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var recommendedValueStack: UIStackView!
    @IBOutlet weak var viewOptionButton: UIView!
    
    var optionButton:(()-> Void)? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK:IBActions
    @IBAction func optionAction(_ sender: Any) {
        if let optionButton = optionButton{
            optionButton()
        }
    }
    
}
