//
//  AddSprintViewController.swift
//  Agile Sports
//
//  Created by AM on 12/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class AddSprintViewController: ParentViewController {
    
    //MARK: IBOUtlets
    @IBOutlet weak var teamCollection: UICollectionView!
    
    
    var matchData = [MatchResponse]()

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
}


extension AddSprintViewController: UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return matchData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdddSprintCollectionViewCell", for: indexPath) as! AdddSprintCollectionViewCell
        let val = self.matchData[indexPath.row]

        cell.matchName.setTitle(val.team_name! + " vs. " + val.opponent_team!, for: .normal)
          return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((self.view.frame.width * 0.85)/2) - 1
        return CGSize(width: width, height:150)
    }
}


class AdddSprintCollectionViewCell: UICollectionViewCell {
    //MARK: IBOultes
    @IBOutlet weak var matchName: CustomButton!
   
    var matchButton:(() -> Void)? = nil
    
    @IBAction func matchAction(_ sender: Any) {
        if let matchButton  = matchButton {
            matchButton()
        }
    }
    
}
