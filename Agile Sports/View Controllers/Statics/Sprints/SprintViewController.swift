//
//  SprintViewController.swift
//  Agile Sports
//
//  Created by AM on 12/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class SprintViewController: ParentViewController {
    
    //MARK: IBOutlets
    
    @IBOutlet weak var sprintTable: UITableView!
    
    var sprintData = [SprintResponse]()

    override func viewDidLoad() {
        super.viewDidLoad()

        getSprint()
    }
    
    func getSprint() {
       ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SPRINT, method: .get, parameter: nil, objectClass: GetSprints.self, requestCode: U_GET_SPRINT) { (response) in
            self.sprintData = response.response
        self.sprintTable.reloadData()
        ActivityIndicator.hide()
        }
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
    self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func addSprint(_ sender: Any) {
        self.push(controller: "AddSprintViewController")
    }
}

extension SprintViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      return self.sprintData.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SprintTableViewCell") as! SprintTableViewCell
        let val = self.sprintData[indexPath.row]
        cell.leagueName.text = val.league_name
        cell.teamName.text = val.team_name
        cell.sprintName.text = val.sprint_no
        cell.duration.text = self.convertTimestampToDate(val.start_date!, to: "dd/MM/yyyy") + "-" + self.convertTimestampToDate(val.end_date!, to: "dd/MM/yyyy")
        cell.status.text = val.status
        return cell
    }


}
