//
//  MaticesCollectionViewCell.swift
//  Agile Sports
//
//  Created by AM on 20/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class MaticesCollectionViewCell: UICollectionViewCell {
    
    //MARK: UIOutlets
    @IBOutlet weak var matricName: UILabel!
    @IBOutlet weak var matricValue: UITextField!
    @IBOutlet weak var playerScore: DesignableUILabel!
    @IBOutlet weak var recommendedMatric: UISegmentedControl!
    @IBOutlet weak var disableMatric: UISegmentedControl!
    
    @IBOutlet weak var backView: View!
    
    var greaterButton: (()-> Void)? = nil
    var lessButton: (()-> Void)? = nil
    
    override func awakeFromNib() {
        backView.clipsToBounds = false
    }
    
    @IBAction func greaterAction(_ sender: Any) {
        if let greaterButton = greaterButton {
            greaterButton()
        }
    }
    
    @IBAction func lessAction(_ sender: Any) {
        if let lessButton = lessButton {
            lessButton()
        }
    }
    
}
