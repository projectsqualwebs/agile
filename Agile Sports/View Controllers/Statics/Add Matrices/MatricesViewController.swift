//
//  MatricesViewController.swift
//  Agile Sports
//
//  Created by AM on 20/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import iOSDropDown


class MatricesViewController: ParentViewController, SelectFromPicker {
    func selectedItem(name: String, id: Int) {
        self.selectedSprint.text = name
        self.getMetric(id: self.sprintData[id].id ?? 0)
    }
    
    @IBOutlet weak var metricCollection: UICollectionView!
    @IBOutlet weak var addNewMetricStack: UIStackView!
    @IBOutlet weak var selectedSprint: DesignableUILabel!
    
   // @IBOutlet weak var viewForPopup: UIView!
  //  @IBOutlet weak var metricNameTextField: DropDown!
   // @IBOutlet weak var viewForAddNew: UIView!
//    @IBOutlet weak var viewWithoutCondition: UIView!
//    @IBOutlet weak var viewWithCondition: UIView!
//    @IBOutlet weak var eachPointValue2: DesignableUITextField!
//
//    @IBOutlet weak var eachPointValue: DesignableUILabel!
//    @IBOutlet weak var scoreTextField: DesignableUITextField!
//    @IBOutlet weak var buttonGreaterThan: UIButton!
//    @IBOutlet weak var buttonLessThan: UIButton!
//    @IBOutlet weak var addNewTextField: UITextField!
    
    
    
    var sprintData = [SprintResponse]()
    var userMetricData = [MatricesResponse]()
    var currentMetric = String()
    var sub_type = 1
    var type = 1
    var metric_value = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
   //     self.viewForPopup.isHidden = true
      //  self.viewForAddNew.isHidden = true
     //   self.addNewTextField.delegate = self
      //  self.eachPointValue2.delegate = self
        //metricNameTextField.arrowColor = .clear
      //  metricNameTextField.optionArray = ["FTA","FGM","FGA","FG3M","FG%","FG3A","Add New"]
      
//        metricNameTextField.didSelect { (string,index,id)  in
//           // self.viewForAddNew.isHidden = true
//            if(string == "FTA"){
//                self.metricNameTextField.hideList()
//                self.metricNameTextField.text = string
//
//             var currentMetric = string
//            }else if(string == "FTA"){
//                self.metricNameTextField.hideList()
//                self.metricNameTextField.text = string
//
//                self.currentMetric = string
//            }else if(string == "FGM"){
//                self.metricNameTextField.hideList()
//                self.metricNameTextField.text = string
//
//                self.currentMetric = string
//            }else if(string == "FGA"){
//                self.metricNameTextField.hideList()
//                self.metricNameTextField.text = string
//
//                self.currentMetric = string
//            }else if(string == "FG3M"){
//                self.metricNameTextField.hideList()
//                self.metricNameTextField.text = string
//
//                self.currentMetric = string
//            }else if(string == "FG%"){
//                self.metricNameTextField.hideList()
//                self.metricNameTextField.text = string
//
//                self.currentMetric = string
//            }else if(string == "FG3A"){
//                self.metricNameTextField.hideList()
//                self.metricNameTextField.text = string
//
//                self.currentMetric = string
//            }else if(string == "Add New"){
//                self.metricNameTextField.hideList()
//                self.metricNameTextField.text = string
//                self.currentMetric = string
//               // self.viewForAddNew.isHidden = false
//            }
//        }
        
        if(Singleton.shared.sprintData.count == 0){
            self.getSprintData { (response) in
                self.sprintData = response.response
                
            }
        }else {
          self.sprintData = Singleton.shared.sprintData
        }
    
    }
    
//    func addMatrices() {
//        ActivityIndicator.show(view: self.view)
//        let param = [
//            "metrics_name": self.currentMetric,
//            "type":self.type,
//            "sub_type":self.sub_type,
//            "metrics_value":(self.eachPointValue.text == "") ? self.eachPointValue2.text:self.eachPointValue.text,
//            "score":self.scoreTextField.text
//            ] as [String : Any]
//
//        SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_METRICES, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ADD_METRICES) { (data) in
//                self.showAlert(title: nil, message:data.message!, action1Name: "Ok", action2Name: nil)
//               //  self.getMetric()
//        }
//    }
    
    func getMetric(id: Int) {
       ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MATRICES + "?sprint_no=\(id)", method: .get, parameter: nil, objectClass: GetMatrices.self, requestCode: U_GET_MATRICES) { (response) in
            self.userMetricData = response.response!
            self.metricCollection.reloadData()
             ActivityIndicator.hide()
        }
    }
    
    func clearConstraint() {
      //  self.buttonGreaterThan.setTitleColor(.black, for: .normal)
      //  self.buttonLessThan.setTitleColor(.lightGray, for: .normal)
      //  self.metricNameTextField.text = "Name of the Metrices"
        
       // self.eachPointValue.text = ""
       //  self.eachPointValue2.text = ""
       // self.viewForAddNew.isHidden = true
       // self.addNewTextField.text = ""
      //  self.scoreTextField.text = nil
        currentMetric = String()
        sub_type = 0
        type = 0
        
    }
    
    //MARK: IBOutlets
    @IBAction func selectSprintAction(_ sender: Any) {
           
           let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
           myVC.modalPresentationStyle = .overFullScreen
           myVC.pickerData = []
           myVC.pickerDelegate = self
           myVC.pickerHeading = "Select Sprint"
           for val in self.sprintData{
               myVC.pickerData.append("SPRINT" + (val.sprint_no ?? ""))
           }
           if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
           }
           
       }

    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func metricwithButton(_ sender: Any) {
        type = 1
      //  self.viewForPopup.isHidden = false
      //  self.viewWithoutCondition.isHidden = true
      //  self.viewWithCondition.isHidden = false
    }
    
    @IBAction func metricWithoutButton(_ sender: Any) {
        type = 2
       // self.viewForPopup.isHidden = false
      //  self.viewWithoutCondition.isHidden = false
      //  self.viewWithCondition.isHidden = true
    }
    
    @IBAction func hidePopupAction(_ sender: Any) {
       // self.viewForPopup.isHidden = true
        self.clearConstraint()
    }
    
    @IBAction func doneActionWithMetric(_ sender: Any) {
//        if(self.currentMetric.isEmpty){
//            Singleton.shared.showToast(text: "Select Metric")
//        }else if (eachPointValue.text!.isEmpty || eachPointValue2.text!.isEmpty){
//            Singleton.shared.showToast(text: "Select Point")
//        }else if(scoreTextField.text!.isEmpty){
//            Singleton.shared.showToast(text: "Enter Metric Score")
//        }else {
//           // self.viewForPopup.isHidden = true
//            self.addMatrices()
//            self.clearConstraint()
//        }
        
    }
    
    @IBAction func plusButtonAction(_ sender: Any) {
        self.metric_value = metric_value + 1
      //  self.eachPointValue.text = "\(self.metric_value)"
        // self.eachPointValue2.text = "\(self.metric_value)"
    }
    
    @IBAction func minusButtonAction(_ sender: Any) {
        if(self.metric_value != 0){
          self.metric_value = metric_value - 1
          //   self.eachPointValue2.text = "\(self.metric_value)"
           // self.eachPointValue.text = "\(self.metric_value)"
        }
        
    }
    
    @IBAction func greaterThanAction(_ sender: Any) {
        sub_type = 1
       // self.buttonGreaterThan.setTitleColor(.black, for: .normal)
      //  self.buttonLessThan.setTitleColor(.lightGray, for: .normal)
    }
    
    @IBAction func lessThanAction(_ sender: Any) {
        sub_type = 2
      //  self.buttonLessThan.setTitleColor(.black, for: .normal)
      //  self.buttonGreaterThan.setTitleColor(.lightGray, for: .normal)
    }
    
}


extension MatricesViewController : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.userMetricData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell", for: indexPath) as! AgilityCardCollectionViewCell
        let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell2", for: indexPath) as! AgilityCardCollectionViewCell2
        let val = self.userMetricData[indexPath.row]
        if(K_CURRENT_USER == 8){
            cell2.imageForPlus.image = nil
            cell2.imageForMinus.image = nil
        }
        if(val.type == 2){
            cell2.titleFCM.text = val.metrics_name!
            cell2.scoreValue.text! = "\(val.score!)"
            cell2.conditionLabel.text = "\(val.metrics_value!) pt wise "
            if (val.by_player == 1){
                cell2.backView.borderColor = primaryColor
                cell2.backView.borderWidth = 2
            }else {
                cell2.backView.borderColor = .clear
                cell2.backView.borderWidth = 0
            }
            
            if(val.by_analyst == 1){
                cell2.backView.backgroundColor = UIColor.green
            }else {
                cell2.backView.backgroundColor = UIColor.white
            }
            return cell2
        }else{
            cell1.titleFCM.text = val.metrics_name!
            cell1.scoreValue.text! = "\(val.score!)"
            cell1.conditionLabel.text = "\(val.metrics_value!) pt wise "
            if (val.by_player == 1){
                cell1.backView.borderColor = primaryColor
                cell1.backView.borderWidth = 2
            }else {
                cell1.backView.borderColor = .clear
                cell1.backView.borderWidth = 0
            }
            
            if(val.by_analyst == 1){
                cell1.backView.backgroundColor = UIColor.green
            }else {
                cell1.backView.backgroundColor = UIColor.white
            }
            return cell1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((self.view.frame.width)/2)
        return CGSize(width:width , height: width)
    }
    
    
}

extension MatricesViewController: UITextFieldDelegate {
//    func textFieldDidEndEditing(_ textField: UITextField) {
//        if(textField == self.addNewTextField){
//        self.metricNameTextField.text = textField.text!
//        self.currentMetric = textField.text!
//        }else if(textField == eachPointValue2 ) {
//            self.eachPointValue.text = textField.text!
//        }
//    }
}
