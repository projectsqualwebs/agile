//
//  EditMetricesViewController.swift
//  Agile Sports
//
//  Created by AM on 12/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class EditMetricesViewController: ParentViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var metricesTable: UITableView!
    
    
    var metricesData = [MatricesResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMetrices()
    }
    
    func  getMetrices() {
       ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MATRICES, method: .get, parameter: nil, objectClass: GetMatrices.self, requestCode: U_GET_MATRICES) { (response) in
            self.metricesData = response.response!
        self.metricesTable.reloadData()
        ActivityIndicator.hide()
            
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}

extension EditMetricesViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return metricesData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditMetricesViewCell") as! EditMetricesViewCell
        let val = self.metricesData[indexPath.row]
        cell.metricesName.text = val.metrics_name
        
        return cell
    }
    
    
}




class EditMetricesViewCell: UITableViewCell {
     //MARK: IBOutlets
    @IBOutlet weak var metricesName: UILabel!
    @IBOutlet weak var metricesValue: UITextField!
}
