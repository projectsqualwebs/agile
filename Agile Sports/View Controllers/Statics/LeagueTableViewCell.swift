//
//  LeagueTableViewCell.swift
//  Agile Sports
//
//  Created by AM on 09/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class LeagueTableViewCell: UITableViewCell {
    
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var numberOfTeam: UILabel!
    @IBOutlet weak var numberOfGames: UILabel!
    @IBOutlet weak var league: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
