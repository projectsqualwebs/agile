//
//  LeagueTableViewController.swift
//  Agile Sports
//
//  Created by AM on 09/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit



class LeagueTableViewController: ParentViewController,LeagueAdded {
    
    //MARK: IBOutlets
    @IBOutlet var leagueTable: UITableView!
    @IBOutlet weak var leagueTitle: DesignableUILabel!
    
    var leagueData = [LeagueData]()
    var isAddLeague = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lpgr: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleLongPress(_:)))
            lpgr.delegate = self
            lpgr.minimumPressDuration = 0.5
            lpgr.delaysTouchesBegan = true
            self.leagueTable?.addGestureRecognizer(lpgr)
        if(isAddLeague){
            self.leagueTitle.text = "Select League"
        }else {
            self.leagueTitle.text = "Add new League(s)"
        }
        self.handleScreenData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func handleScreenData(){
        if(Singleton.shared.leagueData.count != 0){
            self.leagueData = Singleton.shared.leagueData
            self.leagueTable.reloadData()
        }else {
            DispatchQueue.main.async {
                CallAPIViewController.shared.getLeagueData { (response) in
                    self.leagueData = response
                    self.leagueTable.reloadData()
                }
            }
        }
    }
    
    func refreshLeagueTable() {
        Singleton.shared.leagueData = []
        self.handleScreenData()
    }
    
    
    //MARK: IBACtions
    @IBAction func addLeague(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddLeagueViewController") as! AddLeagueViewController
        myVC.leagueDelegate = self
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension LeagueTableViewController: UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return self.leagueData.count
      }
      
      func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
          let cell = tableView.dequeueReusableCell(withIdentifier: "LeagueTableViewCell") as! LeagueTableViewCell
          let val = self.leagueData[indexPath.row]
          cell.location.text = val.location
          cell.league.text = val.name
          cell.numberOfTeam.text = "\(val.no_of_teams)"
          cell.numberOfGames.text = "\(val.no_of_games)"
          return cell
      }
    
    @objc func handleLongPress(_ gesture : UILongPressGestureRecognizer!) {
           if gesture.state != .ended {
               return
           }
           let p = gesture.location(in: self.leagueTable)
           if let indexPath = self.leagueTable.indexPathForRow(at: p){
               // get the cell at indexPath (the one you long pressed)
               let cell = self.leagueTable.cellForRow(at: indexPath)
               self.showPopup(title: "Action", msg: "", id: indexPath.row)
           } else {
               print("couldn't find index path")
           }
       }
       
       func showPopup(title: String, msg: String,id:Int) {
           let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
           let yesBtn = UIAlertAction(title:"Edit", style: .default) { (UIAlertAction) in
               let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddLeagueViewController") as! AddLeagueViewController
               myVC.isEdit = true
               myVC.leagueDelegate = self
               myVC.leagueInfo = self.leagueData[id]
               self.navigationController?.pushViewController(myVC, animated: true)
           }
           let noBtn = UIAlertAction(title: "Delete", style: .default){
               (UIAlertAction) in
               self.popupView(title: "Delete User", msg: "Are your sure?", id: id)
           }
           let cancel = UIAlertAction(title: "Cancel", style: .default){
               (UIAlertAction) in
               //  self.noButtonAction()
           }
           
           alert.addAction(yesBtn)
           alert.addAction(noBtn)
           alert.addAction(cancel)
           
           present(alert, animated: true, completion: nil)
       }
       
       func popupView(title: String, msg: String,id:Int) {
           let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
           let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
               let param = [
                   "row_id":id,
                   "table":"leagues"
                   ] as? [String:Any]
               SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_ENTRIES, method: .post, parameter: param, objectClass: Response.self, requestCode: U_DELETE_ENTRIES) { (response) in
                   print(response)
               }
           }
           let noBtn = UIAlertAction(title: "No", style: .default){
               (UIAlertAction) in
               //  self.noButtonAction()
           }
           alert.addAction(yesBtn)
           alert.addAction(noBtn)
           present(alert, animated: true, completion: nil)
       }
}

