//
//  MatchTableTableViewCell.swift
//  Agile Sports
//
//  Created by AM on 10/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class MatchTableTableViewCell: UITableViewCell {
    
    @IBOutlet weak var opponentTeam: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var league: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}

