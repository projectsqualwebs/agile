//
//  MatchTableViewController.swift
//  Agile Sports
//
//  Created by AM on 10/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class MatchTableViewController: ParentViewController {
    
    //MARK: IBOutlets
    @IBOutlet var matchTable: UITableView!
    
    var matchData = [MatchResponse]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lpgr: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleLongPress(_:)))
        lpgr.delegate = self
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        self.matchTable?.addGestureRecognizer(lpgr)
        self.handleScreenData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    
    func handleScreenData() {
        if(Singleton.shared.matchData.count > 0){
            self.matchData = Singleton.shared.matchData
            self.matchTable.reloadData()
        }else {
            CallAPIViewController.shared.getMatches { (response) in
                self.matchData = response
                self.matchTable.reloadData()
            }
        }
    }
    
    //MARK: IBACtions
    @IBAction func addLeague(_ sender: Any) {
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddMatchViewController") as! AddMatchViewController
        myVC.matchDelegate = self
        self.navigationController?.pushViewController(myVC, animated: true)
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension MatchTableViewController: UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,MatchData {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.matchData.count
    }
    
    func updateMatchData() {
        Singleton.shared.matchData = []
        self.handleScreenData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MatchTableTableViewCell") as! MatchTableTableViewCell
        let val = self.matchData[indexPath.row]
        cell.opponentTeam.text = val.opponent_team
        cell.league.text = val.league_name
        cell.location.text = val.location
        cell.date.text = self.convertTimestampToDate(val.date!
            
            , to: "dd/MM/yyyy" )
        cell.time.text = val.time
        return cell
    }
    
    @objc func handleLongPress(_ gesture : UILongPressGestureRecognizer!) {
        if gesture.state != .ended {
            return
        }
        let p = gesture.location(in: self.matchTable)
        if let indexPath = self.matchTable.indexPathForRow(at: p){
            // get the cell at indexPath (the one you long pressed)
            let cell = self.matchTable.cellForRow(at: indexPath)
            self.showPopup(title: "Action", msg: "", id: indexPath.row)
        } else {
            print("couldn't find index path")
        }
    }
    
    func showPopup(title: String, msg: String,id:Int) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesBtn = UIAlertAction(title:"Edit", style: .default) { (UIAlertAction) in
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddMatchViewController") as! AddMatchViewController
            myVC.isEdit = true
            myVC.matchInfo = self.matchData[id]
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        let noBtn = UIAlertAction(title: "Delete", style: .default){
            (UIAlertAction) in
            self.popupView(title: "Delete User", msg: "Are your sure?", id: id)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default){
            (UIAlertAction) in
            //  self.noButtonAction()
        }
        
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func popupView(title: String, msg: String,id:Int) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
            let param = [
                "row_id":id,
                "table":"matches"
                ] as? [String:Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_ENTRIES, method: .post, parameter: param, objectClass: Response.self, requestCode: U_DELETE_ENTRIES) { (response) in
                print(response)
            }
        }
        let noBtn = UIAlertAction(title: "No", style: .default){
            (UIAlertAction) in
            //  self.noButtonAction()
        }
        alert.addAction(yesBtn)
        alert.addAction(noBtn)
        present(alert, animated: true, completion: nil)
    }
}
