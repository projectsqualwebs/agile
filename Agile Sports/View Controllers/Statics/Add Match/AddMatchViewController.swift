//
//  AddMatchViewController.swift
//  Agile Sports
//
//  Created by AM on 10/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import iOSDropDown
import SkyFloatingLabelTextField

protocol MatchData {
    func updateMatchData()
}

class AddMatchViewController: ParentViewController {
    //MARK: IBOutlets
    @IBOutlet weak var teamName: DropDown!
    @IBOutlet weak var matchLocation: SkyFloatingLabelTextField!
    @IBOutlet weak var startDate: SkyFloatingLabelTextField!
    @IBOutlet weak var startTime: SkyFloatingLabelTextField!
    @IBOutlet weak var viewFroDatePicker: UIView!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var addButton: CustomButton!
    @IBOutlet weak var addMatchLabel: DesignableUILabel!
    
    
    var teamData = [TeamsResponse]()
    var selectedTeamId = Int()
    var selectedTime = Int()
    var selectedDate = Int()
    var matchDelegate: MatchData? = nil
    var isEdit = false
    var matchInfo = MatchResponse()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        getTeams()
        self.datePicker.minimumDate = Date()
        self.viewFroDatePicker.isHidden = true
        if(isEdit){
            self.selectedTime = Int(matchInfo.time ?? "0")!
            self.addMatchLabel.text = "Edit Match"
            self.startTime.text = self.convertTimestampToDate(self.selectedTime, to: "hh:mm a")
                  
            self.selectedDate = Int(self.matchInfo.date ?? 0)
            self.startDate.text = self.convertTimestampToDate(self.matchInfo.date ?? 0, to: "dd/MM/yyyy")
            self.teamName.text = self.matchInfo.team_name
            self.matchLocation.text = self.matchInfo.location
            self.addButton.setTitle("UPDATE MATCH", for: .normal)
        }
        teamName.didSelect { (string, id, val) in
            self.selectedTeamId = self.teamData[id].id!
        }
    }
    
    func getTeams() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_TEAMS, method: .get, parameter: nil, objectClass: GetTeams.self, requestCode: U_GET_TEAMS) { (response) in
           self.teamData = response.response!
            for val in self.teamData {
           self.teamName.optionArray.append(val.name!)
            }
        }
    }
  
    
    //MARK : IBActions
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func startDateAction(_ sender: Any) {
        self.datePicker.datePickerMode = .date
        self.viewFroDatePicker.isHidden = false
    }
    
    @IBAction func startTimeAction(_ sender: Any) {
        self.datePicker.datePickerMode = .time
        self.viewFroDatePicker.isHidden = false
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        self.viewFroDatePicker.isHidden = true
        if(   self.datePicker.datePickerMode == .time){
            self.selectedTime = Int(self.datePicker!.date.timeIntervalSince1970)
            self.startTime.text = self.convertTimestampToDate(Int(self.datePicker!.date.timeIntervalSince1970), to: "hh:mm a")
        }else {
            self.selectedDate = Int(self.datePicker!.date.timeIntervalSince1970)
            self.startDate.text = self.convertTimestampToDate(Int(self.datePicker!.date.timeIntervalSince1970), to: "dd/MM/yyyy")
        }
    }
    
    @IBAction func dismissAction(_ sender: Any) {
        self.viewFroDatePicker.isHidden = true
    }
    
    
    @IBAction func addteamAction(_ sender: Any) {
        if(matchLocation.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Match Location")
        }else if(startDate.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Date")
        }else if(startTime.text!.isEmpty){
            Singleton.shared.showToast(text: "Enter Match Time")
        }else {
            let param = [
                "opponent_id":self.selectedTeamId,
                "date":self.selectedDate,
                "time":self.selectedTime,
                "location": self.matchLocation.text!
                ] as! [String:Any]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_MATCH, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ADD_TEAM) { (response) in
                self.showAlert(title: nil, message: response.message!, action1Name: "Ok", action2Name: nil)
                self.matchLocation.text =  ""
                self.startTime.text = ""
                self.startDate.text = ""
                self.teamName.text = ""
                self.matchDelegate?.updateMatchData()
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
}
