//
//  TeamsTableViewController.swift
//  Agile Sports
//
//  Created by AM on 09/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class TeamsTableViewController: ParentViewController, AddNewTeam{
    
    //MARK: IBOutlets
    @IBOutlet var teamTable: UITableView!
    @IBOutlet weak var navbarTitle: DesignableUILabel!
    
    var teamData = [TeamsResponse]()
    var isAddTeam = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lpgr: UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action:#selector(handleLongPress(_:)))
        lpgr.delegate = self
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        self.teamTable?.addGestureRecognizer(lpgr)
        self.handleScreenData()
        if(isAddTeam){
            self.navbarTitle.text = "Select Team"
        }
    }
    
    func handleScreenData() {
        DispatchQueue.main.async {
            if(Singleton.shared.teamData.count != 0){
                self.teamData = Singleton.shared.teamData
                self.teamTable.reloadData()
            }else {
                CallAPIViewController.shared.getTeams { (response) in
                    self.teamData = response
                    self.teamTable.reloadData()
                }
            }
        }
    }
    
    func updateTeamTable() {
        Singleton.shared.teamData = []
        self.handleScreenData()
    }
    
    //MARK: IBActions
    @IBAction func addTeam(_ sender: Any) {
        if(self.isAddTeam){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SelectTeamViewController") as! SelectTeamViewController
            myVC.teamDelegate = self
            self.navigationController?.pushViewController(myVC, animated: true)
            
            
        }else {
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddTeamViewController") as! AddTeamViewController
            myVC.teamDelegate = self
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


extension TeamsTableViewController: UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.teamData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TeamsTableViewCell") as! TeamsTableViewCell
        let val = self.teamData[indexPath.row]
        cell.teamName.text = val.name
        cell.location.text = val.city
        cell.year.text = val.year_founded
        cell.admin.text = (val.first_name ?? "") + (val.last_name ?? "")
        cell.league.text = val.league_name
        cell.numberOfSprint.text = val.no_of_sprints
        cell.numberOfGames.text = "0"
        return cell
    }
    
    
    @objc func handleLongPress(_ gesture : UILongPressGestureRecognizer!) {
              if gesture.state != .ended {
                  return
              }
              let p = gesture.location(in: self.teamTable)
              if let indexPath = self.teamTable.indexPathForRow(at: p){
                  // get the cell at indexPath (the one you long pressed)
                  let cell = self.teamTable.cellForRow(at: indexPath)
                  self.showPopup(title: "Action", msg: "", id: indexPath.row)
              } else {
                  print("couldn't find index path")
              }
          }
          
          func showPopup(title: String, msg: String,id:Int) {
              let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
              let yesBtn = UIAlertAction(title:"Edit", style: .default) { (UIAlertAction) in
                  let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AddTeamViewController") as! AddTeamViewController
                  myVC.isEdit = true
                  myVC.teamDelegate = self
                  myVC.editTeamInfo = self.teamData[id]
                  self.navigationController?.pushViewController(myVC, animated: true)
              }
              let noBtn = UIAlertAction(title: "Delete", style: .default){
                  (UIAlertAction) in
                  self.popupView(title: "Delete User", msg: "Are your sure?", id: id)
              }
              let cancel = UIAlertAction(title: "Cancel", style: .default){
                  (UIAlertAction) in
                  //  self.noButtonAction()
              }
              
              alert.addAction(yesBtn)
              alert.addAction(noBtn)
              alert.addAction(cancel)
              
              present(alert, animated: true, completion: nil)
          }
          
          func popupView(title: String, msg: String,id:Int) {
              let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
              let yesBtn = UIAlertAction(title:"Yes", style: .default) { (UIAlertAction) in
                  let param = [
                      "row_id":id,
                      "table":"teams"
                      ] as? [String:Any]
                  SessionManager.shared.methodForApiCalling(url: U_BASE + U_DELETE_ENTRIES, method: .post, parameter: param, objectClass: Response.self, requestCode: U_DELETE_ENTRIES) { (response) in
                      print(response)
                  }
              }
              let noBtn = UIAlertAction(title: "No", style: .default){
                  (UIAlertAction) in
                  //  self.noButtonAction()
              }
              alert.addAction(yesBtn)
              alert.addAction(noBtn)
              present(alert, animated: true, completion: nil)
          }
}
