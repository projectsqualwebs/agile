//
//  TeamsTableViewCell.swift
//  Agile Sports
//
//  Created by AM on 09/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class TeamsTableViewCell: UITableViewCell {
    
    //MARK: IBActions
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var year: UILabel!
    @IBOutlet weak var numberOfSprint: UILabel!
    @IBOutlet weak var numberOfGames: UILabel!
    @IBOutlet weak var league: UILabel!
    @IBOutlet weak var admin: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
