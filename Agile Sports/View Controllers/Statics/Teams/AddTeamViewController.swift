//
//  AddTeamViewController.swift
//  Agile Sports
//
//  Created by AM on 09/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import iOSDropDown
import SkyFloatingLabelTextField

class AddTeamViewController: ParentViewController {
    //MARK: IBOuteles
    @IBOutlet weak var leagueDropdown: DropDown!
    @IBOutlet weak var leagueNameDropdown: DropDown!
    @IBOutlet weak var teamDropdown: DropDown!
    @IBOutlet weak var foundedYear: DropDown!
    @IBOutlet weak var imgTeamAdmin: ImageView!
    @IBOutlet weak var imgNewTeamAdmin: ImageView!
    @IBOutlet weak var imgLeagueAdmin: ImageView!
    @IBOutlet weak var imgNewLeagueAdmin: ImageView!
    @IBOutlet weak var viewFornewLeague: UIStackView!
    @IBOutlet weak var teamLocation: SkyFloatingLabelTextField!
    @IBOutlet weak var teamName: SkyFloatingLabelTextField!
    @IBOutlet weak var viewFornewTeam: UIStackView!
    @IBOutlet weak var subdomainTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var teamUsername: SkyFloatingLabelTextField!
    @IBOutlet weak var teamFirstName: SkyFloatingLabelTextField!
    @IBOutlet weak var teamLastname: SkyFloatingLabelTextField!
    @IBOutlet weak var teamEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var leagueLastname: SkyFloatingLabelTextField!
    @IBOutlet weak var leagueusername: SkyFloatingLabelTextField!
    @IBOutlet weak var leagueEmail: SkyFloatingLabelTextField!
    @IBOutlet weak var leagueFirstname: SkyFloatingLabelTextField!
    @IBOutlet weak var teamAdminStack: UIStackView!
    @IBOutlet weak var leagueAdminStack: UIStackView!
    @IBOutlet weak var addTeamButton: CustomButton!
    @IBOutlet weak var viewTeamAdminStack: UIStackView!
    
    var teamAdminData = [UserInfo]()
    var leagueAdminData = [UserInfo]()
    var leagueData = [LeagueData]()
    var yearData = ["2010","2011","2012","2013","2014","2015","2016","2017","2018","2019"]
    var selectedyear = String()
    var selectedTeamAdminId = Int()
    var selectedLeagueAdminId = Int()
    var selectedLeagueId = Int()
    var selectedTeamType = Int()
    var selectedLeagueType = Int()
    var teamDelegate: AddNewTeam? = nil
    
    var isEdit = false
    var editTeamInfo = TeamsResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.foundedYear.optionArray = yearData
        self.viewFornewTeam.isHidden = false
        self.viewFornewLeague.isHidden = true
        getTeamAdmin()
        getLeagueAdmin()
        getLeagues()
        self.selectTeamAdmin(self)
        self.selectLeagueAdmin(self)
        leagueDropdown.didSelect { (string, id, val) in
            self.selectedLeagueId = self.leagueData[id].id!
        }
        
        teamDropdown.didSelect { (string, id, val) in
            self.selectedTeamAdminId = self.teamAdminData[id].id!
        }
        
        leagueNameDropdown.didSelect { (string, id, val) in
            self.selectedLeagueAdminId = self.leagueData[id].id!
        }
        
        foundedYear.didSelect { (string, id, val) in
            self.selectedyear = string
        }
    }
    
    func initEditTeam(){
        self.leagueAdminStack.isUserInteractionEnabled = false
        self.viewFornewLeague.isHidden = true
        self.viewTeamAdminStack.isHidden = true
        self.viewFornewTeam.isHidden = false
        self.teamAdminStack.isUserInteractionEnabled = false
        self.teamName.text = self.editTeamInfo.name
        self.teamLocation.text = self.editTeamInfo.city
        self.leagueDropdown.text = self.editTeamInfo.league_name
        self.foundedYear.text = self.editTeamInfo.year_founded
        
        self.leagueDropdown.text = self.editTeamInfo.league_name
        self.subdomainTextField.text = self.editTeamInfo.sub_domain
        self.addTeamButton.setTitle("UPDATE TEAM", for: .normal)
        selectedyear = self.editTeamInfo.year_founded ?? ""
        selectedLeagueId = Int(self.editTeamInfo.league_id ?? "0")!
        let adminVal = self.teamAdminData.filter{
            ($0.id ?? 0) == Int(self.editTeamInfo.admin ?? "0")!
        }
        self.teamDropdown.text = (adminVal[0].first_name ?? "") + " " + (adminVal[0].last_name ?? "")
        self.selectedTeamAdminId = Int(self.editTeamInfo.admin ?? "0")!

    }
    
    func getTeamAdmin() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ADMINS + "3", method: .get, parameter: nil, objectClass: GetTeamAdmin.self, requestCode: U_GET_ADMINS) { (response) in
            self.teamAdminData = response.response!
            for val in response.response! {
                self.teamDropdown.optionArray.append(val.username!)
            }
            if(self.isEdit){
                self.initEditTeam()
            }
        }
    }
    
    func initializeView() {
        selectedyear = String()
        selectedTeamAdminId = Int()
        selectedLeagueAdminId = Int()
        selectedLeagueId = Int()
        selectedTeamType = Int()
        selectedLeagueType = Int()
        self.leagueusername.text = ""
        self.leagueFirstname.text = ""
        self.leagueLastname.text = ""
        self.leagueEmail.text = ""
        self.teamUsername.text = ""
        self.teamFirstName.text = ""
        self.teamLastname.text = ""
        self.teamEmail.text = ""
        imgLeagueAdmin.image = UIImage(named: "circleBlue")
        imgNewLeagueAdmin.image = UIImage(named: "circlegray")
        imgTeamAdmin.image = UIImage(named: "circlegray")
        imgNewTeamAdmin.image = UIImage(named: "circleBlue")
        self.teamDropdown.text = ""
        self.leagueDropdown.text = ""
        self.leagueDropdown.text = ""
        self.foundedYear.text = ""
        self.teamName.text = ""
        self.subdomainTextField.text = ""
        self.teamLocation.text = ""
    }
    
    func getLeagueAdmin(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ADMINS + "2", method: .get, parameter: nil, objectClass: GetLeagueAdmin.self, requestCode: U_GET_ADMINS) { (response) in
            self.leagueAdminData = response.response!
            for val in response.response! {
                self.leagueNameDropdown.optionArray.append(val.username!)
            }
        }
    }
    
    func getLeagues() {
        DispatchQueue.main.async {
            if(Singleton.shared.leagueData.count != 0){
                self.leagueData = Singleton.shared.leagueData
                for val in self.leagueData {
                    self.leagueDropdown!.optionArray.append(val.name!)
                }
            }else {
                CallAPIViewController.shared.getLeagueData { (response) in
                    self.leagueData = response
                    for val in Singleton.shared.leagueData {
                        self.leagueDropdown!.optionArray.append(val.name!)
                    }
                }
            }
        }
    }
    
    //MARK:IBAction
    @IBAction func selectTeamAdmin(_ sender: Any) {
        imgTeamAdmin.image = UIImage(named: "circleBlue")
        imgNewTeamAdmin.image = UIImage(named: "circlegray")
        self.teamDropdown.isHidden = false
        self.viewTeamAdminStack.isHidden = true
        self.selectedTeamType = 1
        self.teamUsername.text = ""
        self.teamFirstName.text = ""
        self.teamLastname.text = ""
        self.teamEmail.text = ""
    }
    
    
    @IBAction func addNewTeamAdmin(_ sender: Any) {
        imgTeamAdmin.image = UIImage(named: "circlegray")
        imgNewTeamAdmin.image = UIImage(named: "circleBlue")
        self.teamDropdown.isHidden = true
        self.viewTeamAdminStack.isHidden = false
        self.selectedTeamType = 2
    }
    
    
    @IBAction func selectLeagueAdmin(_ sender: Any) {
        imgLeagueAdmin.image = UIImage(named: "circleBlue")
        imgNewLeagueAdmin.image = UIImage(named: "circlegray")
        self.leagueDropdown.isHidden = false
        self.viewFornewLeague.isHidden = true
        self.selectedLeagueType = 1
        self.leagueusername.text = ""
        self.leagueFirstname.text = ""
        self.leagueLastname.text = ""
        self.leagueEmail.text = ""
        
    }
    
    @IBAction func addnewLeagueAdmin(_ sender: Any) {
        imgLeagueAdmin.image = UIImage(named: "circlegray")
        imgNewLeagueAdmin.image = UIImage(named: "circleBlue")
        self.leagueDropdown.isHidden = true
        self.viewFornewLeague.isHidden = false
        self.selectedLeagueType = 2
    }
    
    @IBAction func doneButtonAction(_ sender: Any) {
        if(isEdit){
            if(self.teamName.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Team Name")
            }else if(self.teamLocation.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Location")
            }else if(self.subdomainTextField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Subdomain name")
            }else {
                let param = [
                    "id":self.editTeamInfo.id,
                    "league_id":self.selectedLeagueId,
                    "name":self.teamName.text,
                    "city":self.teamLocation.text,
                    "year_founded":self.foundedYear.text,
                    //"no_of_sprints":self.spri,
                    "sub_domain":self.subdomainTextField.text,
                    "admin":self.selectedTeamAdminId
                    ] as? [String:Any]
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_EDIT_TEAM, method: .post, parameter: param, objectClass: Response.self, requestCode: U_EDIT_TEAM) { (response) in
                    Singleton.shared.showToast(text: response.message ?? "")
                    self.teamDelegate?.updateTeamTable()
                    self.navigationController?.popViewController(animated: true)
                }
                
            }
        }else {
            if(self.selectedLeagueId == nil){
                Singleton.shared.showToast(text: "Select league")
            }else if(self.teamName.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Team Name")
            }else if(self.teamLocation.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Location")
            }else if(self.selectedyear == nil){
                Singleton.shared.showToast(text: "Select Founded year")
            }else if(self.subdomainTextField.text!.isEmpty){
                Singleton.shared.showToast(text: "Enter Subdomain name")
            }else if(self.selectedTeamType == nil){
                Singleton.shared.showToast(text: "Select Team Admin")
            }else if(self.selectedLeagueType == nil){
                Singleton.shared.showToast(text: "Select League Admin")
            }else {
                if(self.selectedTeamType == 2){
                    if(teamUsername.text!.isEmpty){
                        Singleton.shared.showToast(text: "Enter New Team Admin Username")
                        return
                    }else if(teamFirstName.text!.isEmpty){
                        Singleton.shared.showToast(text: "Enter New Team Admin First Name")
                        return
                    }else if(teamLastname.text!.isEmpty){
                        Singleton.shared.showToast(text: "Enter New Team Admin Last Name")
                        return
                    }else if(teamEmail.text!.isEmpty){
                        Singleton.shared.showToast(text: "Enter New Team Admin Email")
                        return
                    }
                }
                if(self.selectedLeagueType == 2){
                    if(leagueusername.text!.isEmpty){
                        Singleton.shared.showToast(text: "Enter New League Admin Username")
                        return
                    }else if(leagueFirstname.text!.isEmpty){
                        Singleton.shared.showToast(text: "Enter New League Admin First Name")
                        return
                    }else if(leagueLastname.text!.isEmpty){
                        Singleton.shared.showToast(text: "Enter New League Admin Last Name")
                        return
                    }else if(leagueEmail.text!.isEmpty){
                        Singleton.shared.showToast(text: "Enter New League Admin Email")
                        return
                    }
                }
                let team = [
                    "type": self.selectedTeamType,
                    "username": self.teamUsername.text,
                    "firstName":self.teamFirstName.text,
                    "lastName": self.teamLastname.text,
                    "email": self.teamEmail.text,
                    "profileImage": ""
                    ] as? [String:Any]
                let league = [
                    "type": self.selectedLeagueType,
                    "username": self.leagueusername.text,
                    "firstName":self.leagueFirstname.text,
                    "lastName": self.leagueLastname.text,
                    "email": self.leagueEmail.text,
                    "profileImage": ""
                    ] as! [String:Any]
                
                let param = [
                    "name": teamName.text!,
                    "city": teamLocation.text!,
                    "location": teamLocation.text! ,
                    "team_admin":team ,
                    "league_admin": league,
                    "subDomain": self.subdomainTextField.text,
                    "team_admin_id": self.selectedTeamAdminId ?? "",
                    "league_admin_id": self.selectedLeagueAdminId ?? "",
                    "leagueId": self.selectedLeagueId ?? "",
                    "yearFounded": self.selectedyear
                    ] as! [String:Any]
                
                SessionManager.shared.methodForApiCalling(url: U_BASE + U_ADD_NEWTEAM, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ADD_NEWTEAM) { (response) in
                    self.initializeView()
                    self.showAlert(title: nil, message: response.message!, action1Name: "Ok", action2Name: nil)
                }
            }
        }
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}


