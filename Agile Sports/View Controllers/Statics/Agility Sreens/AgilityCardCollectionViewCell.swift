//
//  AgilityCardCollectionViewCell.swift
//  Agile Sports
//
//  Created by AM on 20/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class AgilityCardCollectionViewCell2: UICollectionViewCell {
    
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var titleFCM: UILabel!
    @IBOutlet weak var backView: View!
    @IBOutlet weak var imageForMinus: UIImageView!
    @IBOutlet weak var imageForPlus: UIImageView!
    @IBOutlet weak var scoreValue: UILabel!
    @IBOutlet weak var viewEditMetrices: UIView!
    
    var minusButton:(()-> Void)? = nil
    var plusButton: (()-> Void)? = nil
    var cellClicked: (()-> Void)? = nil

    override func awakeFromNib() {
    }
  
    @IBAction func minusAction(_ sender: UIButton) {
        if let minusButton = minusButton {
            minusButton()
        }
    }
    
    @IBAction func plusAction(_ sender: UIButton) {
        if let plusButton = plusButton {
            plusButton()
        }
    }
    
    @IBAction func cellAction(_ sender: UIButton) {
        if let cellClicked = cellClicked {
            cellClicked()
        }
    }
}

class AgilityCardCollectionViewCell: UICollectionViewCell, UITextFieldDelegate {
    
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var titleFCM: UILabel!
    @IBOutlet weak var backView: View!
    @IBOutlet weak var scoreValue: UILabel!
    @IBOutlet weak var pointsPerAction: UITextField!
    @IBOutlet weak var scoreValueField: UITextField!
    @IBOutlet weak var viewPointsEarned: UIStackView!
    
    var cellClicked: (()-> Void)? = nil
    
    @IBAction func cellAction(_ sender: UIButton) {
        if let cellClicked = cellClicked {
            cellClicked()
        }
    }
    
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == pointsPerAction){
            NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_COLLECTION_FIELDS), object: nil, userInfo:["tag":textField.tag,"text":textField.text ?? "","ispoints":true])
        }else if(textField == scoreValue){
            NotificationCenter.default.post(name: NSNotification.Name(N_UPDATE_COLLECTION_FIELDS), object: nil, userInfo:["tag":textField.tag,"text":textField.text ?? "","ispoints":false])
        }
    }
}
