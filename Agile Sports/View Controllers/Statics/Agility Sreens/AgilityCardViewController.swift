//
//  AgilityCardViewController.swift
//  Agile Sports
//
//  Created by AM on 20/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import SDWebImage

class AgilityCardViewController: ParentViewController {

    //MARK: IBOutlets
    
    @IBOutlet weak var collectionViewAgility: UICollectionView!
    @IBOutlet weak var recommendedValue: UILabel!
    @IBOutlet weak var plannedValue: UILabel!
    
    
    var userId = Int()
    var userType = Int()
    var userMetricData = [MatricesResponse]()
    var recommendValue  = Int()
    var planValue = Int()
    var selectedMetrices = [MatricesResponse]()
    var setMetric = Set<Int>()
    var selectedMetricJson = [NSDictionary]()
   
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMetrics()
    }
    
    func getMetrics() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ASSIGNEMETRICS + "\(userId)", method: .get, parameter: nil, objectClass: GetAgility.self, requestCode: U_GET_ASSIGNEMETRICS) { (response) in
             self.userMetricData = (response.response?.metrics!)!
            self.collectionViewAgility.reloadData()
//            if let score = response.response?.score {
//                self.recommendValue = Double(score.recommended_value ?? 0)
//                self.planValue = score.planed_value ?? ""
//                if let recommend = score.recommended_value {
//                  self.recommendedValue.text = "\(recommend)"
//                }else {
//                    self.recommendedValue.text = "0"
//                }
//                if let planned = score.planed_value {
//                    self.plannedValue.text = "\(planned)"
//                }else {
//                    self.plannedValue.text = "0"
//                }
//            }else {
//                self.recommendValue =  0
//                self.planValue =  0
//                self.recommendedValue.text = "0"
//                self.plannedValue.text = "0"
//            }
          
            
            ActivityIndicator.hide()
        }

     
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func assignMetrices(_ sender: Any) {
        ActivityIndicator.show(view: self.view)
        for val in setMetric {
            self.selectedMetrices.append(self.userMetricData[val])
        }
        // let myVal = jsonConversion(val: self.selectedMetrices)
    }
    
//    func jsonConversion(val: [MatricesResponse]) {
//        for arr in val {
//            let values = MatricesResponse(id:arr.id!, metrics_name: arr.metrics_name, type:  arr.type, sub_type: arr.sub_type, by_player: arr.by_player, by_analyst: arr.by_analyst, user_id: arr.user_id, score: arr.score, metrics_value: arr.metrics_value,earned:nil,match_id: nil)
//
//            let jsonEncoder = JSONEncoder()
//            let jsonData = try! jsonEncoder.encode(values)
//
//            let jsonString = String(data: jsonData, encoding: .utf8)!
//            let data = jsonString.data(using: .utf8)
//            do {
//                let dic = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! NSDictionary
//               self.selectedMetricJson.append(dic)
//                print(dic)
//            }
//            catch let e as NSError {
//                print(e.localizedDescription)
//            }
//        }
//        var userRole = String()
//        if(Singleton.shared.userInfo.user_role! == "Players"){
//            userRole = "player"
//        }else {
//            userRole = "analyst"
//        }
//
//        let param = [
//         "selectedMetrics": self.selectedMetricJson,
//         "by_user":userRole,
//        "planed_value":self.plannedValue.text!,
//        "recommended_value":self.recommendedValue.text,
//        "user_id":self.userId,
//        "user_type":self.userType
//            ] as [String : Any]
//        SessionManager.shared.methodForApiCalling(url: U_BASE +  U_ASSIGN_METRICS, method: .post, parameter: param, objectClass: Response.self, requestCode: U_ASSIGN_METRICS) { (response) in
//            print(response)
//            ActivityIndicator.hide()
//
//        }
//       // let jsonString = String(data: jsonData, encoding: .utf8)!
//
//    }
    
}

extension AgilityCardViewController: UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.userMetricData.count
    }
    
    //func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {}
//        let cell1 = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell", for: indexPath) as! AgilityCardCollectionViewCell
//        let cell2 = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell2", for: indexPath) as! AgilityCardCollectionViewCell2
//        let val = self.userMetricData[indexPath.row]
//        if(val.type == 2){
//            cell2.titleFCM.text = val.metrics_name!
//         cell2.scoreValue.text! = "\(val.score!)"
//            cell2.conditionLabel.text = "\(val.metrics_value!) pt wise "
//            if (val.by_player == 1){
//                cell2.backView.borderColor = primaryColor
//                self.setMetric.insert(indexPath.row)
//                cell2.backView.borderWidth = 2
//            }else {
//                cell2.backView.borderColor = .clear
//                cell2.backView.borderWidth = 0
//                self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
//            }
//            if(val.by_analyst == 1){
//                self.setMetric.insert(indexPath.row)
//                cell2.backView.backgroundColor = UIColor.green
//            }else {
//                cell2.backView.backgroundColor = UIColor.white
//                self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
//            }
//            cell2.cellClicked = {
//                if(K_CURRENT_USER == 5){
//                    if(cell2.backView.backgroundColor == UIColor.green){
//                        cell2.backView.backgroundColor = .white
//                        self.userMetricData[indexPath.row].by_analyst = 0
//                        self.recommendValue = self.recommendValue -  (val.score! * val.metrics_value!)
//                        if(self.recommendValue >= 0){
//                            self.recommendedValue.text = "\(self.recommendValue)"
//                        }else {
//                            self.recommendValue = 0
//                        }
//                        self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
//                    }else {
//                        self.recommendValue = self.recommendValue +  (val.score! * val.metrics_value!)
//                        if(self.recommendValue >= 0){
//                            self.recommendedValue.text = "\(self.recommendValue)"
//                        }else {
//                            self.recommendValue = 0
//                        }
//                        cell2.backView.backgroundColor = UIColor.green
//                        self.userMetricData[indexPath.row].by_analyst = 1
//                        self.setMetric.insert(indexPath.row)
//                    }
//                }else if(K_CURRENT_USER == 8){
//                    if(cell2.backView.borderColor == primaryColor){
//                        cell2.backView.borderColor = .clear
//                        self.userMetricData[indexPath.row].by_player = 0
//                        cell2.backView.borderWidth = 0
//                        self.planValue = self.planValue -  (val.score! * val.metrics_value!)
//                        if(self.planValue >= 0){
//                            self.plannedValue.text = "\(self.planValue)"
//                        }else {
//                            self.planValue = 0
//                        }
//                        self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
//                    }else {
//                        self.planValue = self.planValue +  (val.score! * val.metrics_value!)
//                        if(self.planValue >= 0){
//                            self.plannedValue.text = "\(self.planValue)"
//                        }else {
//                            self.planValue = 0
//                        }
//                        cell2.backView.borderColor = primaryColor
//                        cell2.backView.borderWidth = 2
//                        self.userMetricData[indexPath.row].by_player = 1
//                        self.setMetric.insert(indexPath.row)
//                    }
//                }
//            }
//            cell2.plusButton = {
//                if((K_CURRENT_USER == 5) && (cell2.backView.backgroundColor == UIColor.green)){
//                    if(self.recommendValue > 0){
//                        self.userMetricData[indexPath.row].score =
//                            self.userMetricData[indexPath.row].score! + 1
//                        self.recommendValue = self.recommendValue + val.metrics_value!
//                        self.recommendedValue.text = "\(self.recommendValue)"
//                        cell2.scoreValue.text = "\(val.score! + 1)"
//                        self.collectionViewAgility.reloadItems(at: [indexPath])
//                    }
//                }
////                else if( (K_CURRENT_USER == 8)  && (cell2.backView.borderColor == blueColor)){
////                    self.userMetricData[indexPath.row].score =
////                        self.userMetricData[indexPath.row].score! + 1
////                    self.planValue = self.planValue + val.metrics_value!
////                    self.plannedValue.text = "\(self.planValue)"
////                    cell2.scoreValue.text = "\(val.score! + 1)"
////                    self.collectionViewAgility.reloadItems(at: [indexPath])
////                }
//            }
//            cell2.minusButton = {
//                if((K_CURRENT_USER == 5 ) && (cell2.backView.backgroundColor == UIColor.green)){
//                    if(self.recommendValue > 0){
//                        self.userMetricData[indexPath.row].score =
//                            self.userMetricData[indexPath.row].score! - 1
//                        self.recommendValue = self.recommendValue -  val.metrics_value!
//                        self.recommendedValue.text = "\(self.recommendValue)"
//                        cell2.scoreValue.text = "\(val.score! - 1)"
//                        self.collectionViewAgility.reloadItems(at: [indexPath])
//                    }
//                }
//
//
////                else if( (K_CURRENT_USER == 8)  && (cell2.backView.borderColor == blueColor)){
////                   self.userMetricData[indexPath.row].score = self.userMetricData[indexPath.row].score! - 1
////                    self.planValue = self.planValue + val.metrics_value!
////                    self.plannedValue.text = "\(self.planValue)"
////                    cell2.scoreValue.text = "\(val.score! - 1)"
////                    self.collectionViewAgility.reloadItems(at: [indexPath])
////                }
//            }
//            return cell2
//        }else{
//            cell1.titleFCM.text = val.metrics_name!
//            cell1.scoreValue.text! = "\(val.score!)"
//            if(val.sub_type == 1){
//                cell1.conditionLabel.text = " if < \(val.metrics_value!)"
//            }else {
//                 cell1.conditionLabel.text = "if > \(val.metrics_value!)"
//            }
//            if (val.by_player == 1){
//                cell1.backView.borderColor = primaryColor
//                cell1.backView.borderWidth = 2
//                self.setMetric.insert(indexPath.row)
//            }else {
//                cell1.backView.borderColor = .clear
//                cell1.backView.borderWidth = 0
//                self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
//            }
//
//            if(val.by_analyst == 1){
//                cell1.backView.backgroundColor = UIColor.green
//                self.setMetric.insert(indexPath.row)
//            }else {
//                cell1.backView.backgroundColor = UIColor.white
//                self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
//            }
//            cell1.cellClicked = {
//                if(K_CURRENT_USER == 5){
//                    if(cell1.backView.backgroundColor == UIColor.green){
//                        cell1.backView.backgroundColor = .white
//                        self.userMetricData[indexPath.row].by_analyst = 0
//                        self.recommendValue = self.recommendValue - val.score!
//
//                        if(self.recommendValue >= 0){
//                            self.recommendedValue.text = "\(self.recommendValue)"
//                        }else {
//                            self.recommendValue = 0
//                        }
//                        self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
//                    }else {
//                        self.recommendValue = self.recommendValue +  val.score!
//                        self.userMetricData[indexPath.row].by_analyst = 1
//                        if(self.recommendValue >= 0){
//                            self.recommendedValue.text = "\(self.recommendValue)"
//                        }else {
//                            self.recommendValue = 0
//                        }
//                        cell1.backView.backgroundColor = UIColor.green
//                        self.setMetric.insert(indexPath.row)
//                    }
//                }else if(K_CURRENT_USER == 8){
//                    if(cell1.backView.borderColor == primaryColor){
//                        cell1.backView.borderColor = .clear
//                        cell1.backView.borderWidth = 0
//                        self.userMetricData[indexPath.row].by_player = 0
//                        self.planValue = self.planValue - val.score!
//                        if(self.planValue >= 0){
//                            self.plannedValue.text = "\(self.planValue)"
//                        }else {
//                            self.planValue = 0
//                        }
//                         self.setMetric =  self.setMetric.filter {$0 != indexPath.row}
//                    }else {
//                        self.planValue = self.planValue +  val.score!
//                        if(self.planValue >= 0){
//                            self.plannedValue.text = "\(self.planValue)"
//                        }else {
//                            self.planValue = 0
//                        }
//                        cell1.backView.borderColor = primaryColor
//                        cell1.backView.borderWidth = 2
//                        self.userMetricData[indexPath.row].by_player = 1
//                        self.setMetric.insert(indexPath.row)
//                    }
//                }
//            }
//            return cell1
//        }
//    }
    
    func addToMetric(id:Int?) {
       
    }
    
    func removeFromMetric(id:Int?) {
       self.selectedMetrices =  self.selectedMetrices.filter {$0.id! != id}
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = ((self.view.frame.width * 0.95)/2) - 1
            return CGSize(width:width , height: 100)
    }
    
    
}
