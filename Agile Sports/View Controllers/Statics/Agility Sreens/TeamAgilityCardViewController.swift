//
//  TeamAgilityCardViewController.swift
//  Agile Sports
//
//  Created by AM on 10/07/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import iOSDropDown

class TeamAgilityCardViewController: ParentViewController, UITableViewDelegate,UITableViewDataSource {
  
    //MARK: IBOutlets
    
    @IBOutlet weak var teamAgilityTable: UITableView!
    @IBOutlet weak var selectOpponentTeam: DropDown!
    
    var mathchData = [MatchResponse]()
    var teamInfo = GetTeamResponse()
    var selectedmatchId = Int()
    var tableDate = [UserInfo]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getOpponentMatches()
        selectOpponentTeam.didSelect { (string, id, val) in
            self.selectedmatchId = self.mathchData[id].id!
            self.getTeamInfo(id:self.mathchData[id].id!)
        }
    }
    
    func getTeamInfo(id:Int){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SINGLE_TEAMINFO + "\(id)", method: .get, parameter: nil, objectClass: GetTeamInfo.self, requestCode: U_GET_SINGLE_TEAMINFO) { (response) in
         self.teamInfo = response.response!
            for val in self.teamInfo.players! {
             self.tableDate.append(val)
            }
            self.tableDate.append(self.teamInfo.coach!)
            self.tableDate.append(self.teamInfo.analyst!)
            self.teamAgilityTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func getOpponentMatches() {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_TEAM_MATCHES, method: .get, parameter: nil, objectClass: GetMatch.self, requestCode: U_GET_TEAM_MATCHES) { (response) in
            self.mathchData = response.response
            for val in self.mathchData {
                self.selectOpponentTeam.optionArray.append(val.team_name! + " vs. " + val.opponent_team!)
            }
            self.teamAgilityTable.reloadData()
            ActivityIndicator.hide()
    }
    }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return self.tableDate.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "TeamAgilityTableViewCell") as! TeamAgilityTableViewCell
            let val = self.tableDate[indexPath.row]
            cell.role.text = val.user_role
            cell.userName.text = (val.first_name ?? "") + (val.last_name ?? "")
            cell.totalScore.text = "0"
            cell.editButtonAction = {
                self.push(controller: "EditMetricesViewController")
            }
            
            
            return cell
        }
        
        
}

class TeamAgilityTableViewCell: UITableViewCell {
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var totalScore: UILabel!
    @IBOutlet weak var showOption: UIButton!
    @IBOutlet weak var editButton: UIButton!
    
    var editButtonAction:(() -> Void)? = nil
    
    
 
    
    @IBAction func editAction(_ sender: Any) {
        if let editButtonAction = editButtonAction {
            editButtonAction()
        }
    }
    
}
