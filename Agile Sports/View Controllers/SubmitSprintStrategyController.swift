//
//  SubmitSprintStrategyController.swift
//  Agile Sports
//
//  Created by qw on 21/07/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class SubmitSprintStrategyController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var sprintStrategyTable: UITableView!
    @IBOutlet weak var submitButton: CustomButton!
    @IBOutlet weak var headingLabel: DesignableUILabel!
    
    
    var sprintId = Int()
    var currentScreen = String()
    var surveyData = GetSubmitSurveyResponse()
    var meetingNoteData = GetSubmitSurveyResponse()
    var sprintReviewData = GetSubmitSurveyResponse()
    var scoutingReportData = GetSubmitSurveyResponse()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(currentScreen == K_SUBMIT_MEETING_NOTES){
            self.headingLabel.text = "Submit Post Game Meeting Notes"
            self.getPostGameNotes()
        }else if(currentScreen == K_SUBMIT_SCOUTING_REPORT){
            self.headingLabel.text = "Submit Scouting Report"
            self.getScoutingReport()
        }else if(currentScreen == k_SUBMIT_SPRINT_STRATEGY){
            self.headingLabel.text = "Submit Sprint Strategy"
            self.getSprintStrategy()
        }else if(currentScreen == K_SUBMIT_SPRINT_REVIEW){
            self.headingLabel.text = "Submit Sprint Review"
            self.getSprintReview()
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.collectionValueChanged), name: NSNotification.Name(N_EDIT_SPRINT_STRATEGY), object: nil)
    }
    
    @objc func collectionValueChanged(_ notif: Notification){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(N_EDIT_SPRINT_STRATEGY), object: nil)
        if let data = notif.userInfo {
            //  ["tag":textField.tag,"text":textField.text ?? "","ispoints":true]
            if(currentScreen == K_SUBMIT_MEETING_NOTES){
                self.meetingNoteData.data[data["tag"] as! Int].answer = data["text"] as! String
            }else if(currentScreen == K_SUBMIT_SCOUTING_REPORT){
                self.scoutingReportData.data[data["tag"] as! Int].answer = data["text"] as! String
            }else if(currentScreen == k_SUBMIT_SPRINT_STRATEGY){
                self.surveyData.data[data["tag"] as! Int].answer = data["text"] as! String
            }else if(currentScreen == K_SUBMIT_SPRINT_REVIEW){
                self.sprintReviewData.data[data["tag"] as! Int].answer = data["text"] as! String
            }
            
            
            self.sprintStrategyTable.reloadData()
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.collectionValueChanged), name: NSNotification.Name(N_EDIT_SPRINT_STRATEGY), object: nil)
        
    }
    
    
    func getSprintStrategy(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SPRINT_STRATEGY_QUESTION + "\(self.sprintId)", method: .get, parameter: nil, objectClass: GetSubmitSurvey.self, requestCode: U_SPRINT_STRATEGY_QUESTION) { (response) in
            self.surveyData = response.response!
            if(self.surveyData.isSubmitted!){
                self.submitButton.backgroundColor = .lightGray
                self.submitButton.isUserInteractionEnabled = false
            }else {
                self.submitButton.backgroundColor = primaryColor
                self.submitButton.isUserInteractionEnabled = true
            }
            self.sprintStrategyTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func getScoutingReport(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SCOUTING_REPORT_QUESTION + "\(self.sprintId)", method: .get, parameter: nil, objectClass: GetSubmitSurvey.self, requestCode: U_GET_SCOUTING_REPORT_QUESTION) { (response) in
            self.scoutingReportData = response.response!
            if(self.scoutingReportData.isSubmitted!){
                self.submitButton.backgroundColor = .lightGray
                self.submitButton.isUserInteractionEnabled = false
            }else {
                self.submitButton.backgroundColor = primaryColor
                self.submitButton.isUserInteractionEnabled = true
            }
            self.sprintStrategyTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func getSprintReview(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_SPRINT_REVIEW + "\(self.sprintId)", method: .get, parameter: nil, objectClass: GetSubmitSurvey.self, requestCode: U_GET_SPRINT_REVIEW) { (response) in
            self.sprintReviewData = response.response!
            if(self.sprintReviewData.isSubmitted!){
                self.submitButton.backgroundColor = .lightGray
                self.submitButton.isUserInteractionEnabled = false
            }else {
                self.submitButton.backgroundColor = primaryColor
                self.submitButton.isUserInteractionEnabled = true
            }
            self.sprintStrategyTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    func getPostGameNotes(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_MEETING_NOTES_QUESTION + "\(self.sprintId)", method: .get, parameter: nil, objectClass: GetSubmitSurvey.self, requestCode: U_MEETING_NOTES_QUESTION) { (response) in
            self.meetingNoteData = response.response!
            if(self.meetingNoteData.isSubmitted!){
                self.submitButton.backgroundColor = .lightGray
                self.submitButton.isUserInteractionEnabled = false
            }else {
                self.submitButton.backgroundColor = primaryColor
                self.submitButton.isUserInteractionEnabled = true
            }
            self.sprintStrategyTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBAction
    @IBAction func backAction(_ sender: Any) {
        self.back()
    }
    
    @IBAction func submitAction(_ sender: Any) {
        var sprintJson = [Dictionary<String,Any>]()
        let encoder = JSONEncoder()
        do {
            if(currentScreen == K_SUBMIT_MEETING_NOTES){
                sprintJson =  try JSONSerialization.jsonObject(with: encoder.encode(self.meetingNoteData.data), options : .allowFragments) as! [Dictionary<String,Any>]
            }else if(self.currentScreen == K_SUBMIT_SPRINT_REVIEW){
                sprintJson =  try JSONSerialization.jsonObject(with: encoder.encode(self.sprintReviewData.data), options : .allowFragments) as! [Dictionary<String,Any>]
            }else if(self.currentScreen == K_SUBMIT_SCOUTING_REPORT){
                sprintJson =  try JSONSerialization.jsonObject(with: encoder.encode(self.scoutingReportData.data), options : .allowFragments) as! [Dictionary<String,Any>]
            }else {
                sprintJson =  try JSONSerialization.jsonObject(with: encoder.encode(self.surveyData.data), options : .allowFragments) as! [Dictionary<String,Any>]
            }
            
        }catch {
            print(error.localizedDescription)
        }
        ActivityIndicator.show(view: self.view)
        var param = [String:Any]()
        if(currentScreen == K_SUBMIT_MEETING_NOTES){
            param = [
                "sprint_match_id": self.sprintId,
                "meeting_notes":sprintJson
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SUBMIT_MEETING_NOTES, method: .post, parameter: param, objectClass: Response.self, requestCode: U_SUBMIT_MEETING_NOTES) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.showToast(text: "Post Game Meeting Notes submitted successfully.")
            }
        }else if(self.currentScreen == K_SUBMIT_SPRINT_REVIEW){
            param = [
                "sprint_id": self.sprintId,
                "sprint_review":sprintJson
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SUBMIT_SPRINT_REVIEW, method: .post, parameter: param, objectClass: Response.self, requestCode: U_SUBMIT_SPRINT_REVIEW) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.showToast(text: "Sprint Review submitted successfully.")
            }
        }else if(self.currentScreen == K_SUBMIT_SCOUTING_REPORT){
            param = [
                "sprint_match_id": self.sprintId,
                "scouting_reports":sprintJson
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SUBMIT_SCOUTING_REPORT, method: .post, parameter: param, objectClass: Response.self, requestCode: U_SUBMIT_SCOUTING_REPORT) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.showToast(text: "Scouting Report submitted successfully.")
            }
        }else {
            param = [
                "sprint_id": self.sprintId,
                "sprint_strategy":sprintJson
            ]
            SessionManager.shared.methodForApiCalling(url: U_BASE + U_SUBMIT_SPRINT_STRATEGY, method: .post, parameter: param, objectClass: Response.self, requestCode: U_SUBMIT_SPRINT_STRATEGY) { (response) in
                ActivityIndicator.hide()
                Singleton.shared.showToast(text: "Sprint Startegy submitted successfully.")
            }
        }
    }
}

extension SubmitSprintStrategyController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.currentScreen == K_SUBMIT_MEETING_NOTES){
            return self.meetingNoteData.data.count
        }else if(self.currentScreen == K_SUBMIT_SPRINT_REVIEW){
            return self.sprintReviewData.data.count
        }else if(self.currentScreen == K_SUBMIT_SCOUTING_REPORT){
            return self.scoutingReportData.data.count
        }else{
            return self.surveyData.data.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SprintStrategyTable") as! SprintStrategyTable
        var val = SubmitSurvey()
        if(self.currentScreen == K_SUBMIT_MEETING_NOTES){
            val = self.meetingNoteData.data[indexPath.row]
        }else if(self.currentScreen == K_SUBMIT_SCOUTING_REPORT){
            val = self.scoutingReportData.data[indexPath.row]
        }else if(self.currentScreen == K_SUBMIT_SPRINT_REVIEW){
            val = self.sprintReviewData.data[indexPath.row]
        }else {
            val = self.surveyData.data[indexPath.row]
        }
        cell.userName.text = "\(indexPath.row + 1). " + (val.question ?? "")
        cell.answerTextView.text = val.answer
        cell.answerTextView.delegate = cell
        cell.answerTextView.tag = indexPath.row
        if(self.surveyData.isSubmitted == true){
            cell.answerTextView.isUserInteractionEnabled = false
        }else {
            cell.answerTextView.isUserInteractionEnabled = true
        }
        return cell
    }
}
