//
//  SelectGameViewController.swift
//  Agile Sports
//
//  Created by qw on 20/04/20.
//  Copyright © 2020 AM. All rights reserved.
//

//

import UIKit

class SelectGameViewController: ParentViewController {
    //MARK: IBOutlets
    @IBOutlet weak var sprintHeading: DesignableUILabel!
    @IBOutlet weak var matchTable: UITableView!
    
    var matchData = [MatchResponse]()
    var sprintNumber = String()
    var sprintId = Int()
    var currentScreen = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.sprintHeading.text = "Sprint \(self.sprintNumber) >> Total Games: \(self.matchData.count)"
        self.matchTable.reloadData()
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.back()
    }
    
    
}

extension SelectGameViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.matchData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SprintTableViewCell") as! SprintTableViewCell
        let val = self.matchData[indexPath.row]
        if(self.currentScreen == K_POST_GAME_SURVEY || self.currentScreen == K_SPRINT_STRATEGY || self.currentScreen == K_SCOUTING_REPORT || self.currentScreen == K_SPRINT_REVIEW) {
            cell.recommendedValueStack.isHidden = true
        }
        cell.teamName.text = "Recommended value: " + (val.team_details?.recommended_value ?? "0")
        cell.leagueName.text =  "Vs. " + (val.opponent_team ?? "")
        cell.duration.text = "Planned Value: " + (val.team_details?.planed_value ?? "0")
        cell.status.text = "Actual Score: " +  "\(val.team_details?.actual_value ?? "0")"
        cell.sprintName.text = "Dated: " + self.convertTimestampToDate(val.date ?? 0, to: "dd MMM yyyy")
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(currentScreen == K_POST_GAME_SURVEY){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PostGameSurveyViewController") as! PostGameSurveyViewController
            myVC.matchId = self.matchData[indexPath.row].id ?? 0
            myVC.opponentName = "Vs. " + (self.matchData[indexPath.row].opponent_team ?? "")
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if(self.currentScreen == K_ALL_PLAYER_AGILITY_CARDS || self.currentScreen == K_SET_RECOMMENDED_VALUE){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "AllPlayerViewController") as! AllPlayerViewController
            myVC.matchId = self.matchData[indexPath.row].id ?? 0
            myVC.sprintId = self.sprintId
            myVC.currentScreen = K_ALL_PLAYER_AGILITY_CARDS
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if(self.currentScreen == K_STATISTICS_SCREEN){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "StatisticsViewController") as! StatisticsViewController
            myVC.matchId = self.matchData[indexPath.row].id ?? 0
            myVC.sprintId = self.sprintId
            myVC.opponentName = "Vs. " + (self.matchData[indexPath.row].opponent_team ?? "")
            myVC.currentScreen = K_STATISTICS_SCREEN
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if(self.currentScreen == K_VIEW_SURVEY_RESULT){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "GetSurveyResultViewController") as! GetSurveyResultViewController
            myVC.matchId = self.matchData[indexPath.row].id ?? 0
            //myVC.sprintId = self.sprintId
            //myVC.opponentName = self.matchData[indexPath.row].opponent_team ?? ""
            // myVC.currentScreen = K_STATISTICS_SCREEN
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if(self.currentScreen == K_SPRINT_STRATEGY || self.currentScreen == K_SCOUTING_REPORT || self.currentScreen == K_SPRINT_REVIEW || self.currentScreen == K_POST_GAME_NOTES){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SprintStrategyViewController") as! SprintStrategyViewController
            myVC.matchId = self.matchData[indexPath.row].id ?? 0
            
            //myVC.sprintId = self.sprintId
            myVC.heading = "Vs. " + (self.matchData[indexPath.row].opponent_team ?? "")
            myVC.currentScreen = self.currentScreen
            self.navigationController?.pushViewController(myVC, animated: true)
        }else if(self.currentScreen == K_SUBMIT_MEETING_NOTES || self.currentScreen == K_SUBMIT_SCOUTING_REPORT){
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "SubmitSprintStrategyController") as! SubmitSprintStrategyController
            myVC.sprintId = self.matchData[indexPath.row].id ?? 0
            myVC.currentScreen = self.currentScreen
            self.navigationController?.pushViewController(myVC, animated: true)
        }else{
            let myVC = self.storyboard?.instantiateViewController(withIdentifier: "ScheduleSprintViewController") as! ScheduleSprintViewController
            myVC.matchId = self.matchData[indexPath.row].id ?? 0
            myVC.opponentName = "Vs. " + (self.matchData[indexPath.row].opponent_team ?? "")
            myVC.sprintId = self.sprintId
            self.navigationController?.pushViewController(myVC, animated: true)
        }
        
    }
    
}
