//
//  StatisticsViewController.swift
//  Agile Sports
//
//  Created by qw on 22/04/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

class StatisticsViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var statisticTable: ContentSizedTableView!
    
    var matchId = Int()
    var opponentName = String()
    var sprintId = Int()
    var currentScreen = String()
    var plyerStatisticrData = GetAllPlayerResponse()
    var tableData = [AllPlayerDetail]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        statisticTable.estimatedRowHeight = 400
        statisticTable.rowHeight = UITableView.automaticDimension
        self.getStatisctics()
    }
    
    func getStatisctics(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_STATISTICS_DATA + "\(matchId)/2/\(sprintId)", method: .get, parameter: nil, objectClass:GetAllPlayer.self , requestCode: U_GET_STATISTICS_DATA) { (response) in
            self.plyerStatisticrData = response.response
            self.tableData = []
            self.tableData.append(response.response.coach!)
            for val in response.response.players!{
                self.tableData.append(val)
            }
            self.statisticTable.reloadData()
            ActivityIndicator.hide()
        }
    }
    
    //MARK: IBActions
    @IBAction func backAction(_ sender: Any) {
        self.back()
    }
}

extension StatisticsViewController: UITableViewDataSource, UITableViewDelegate {
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableData.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatisticViewCell") as! StatisticViewCell
        let val = self.tableData[indexPath.row]
        if(indexPath.row == 0){
          cell.playerName.text = "Team"
        }else {
          cell.playerName.text = (val.first_name ?? "") + " " + (val.last_name ?? "")
        }
        cell.metricData = (val.metrics)!
        cell.metricTable.reloadData()
        self.statisticTable.rowHeight = UITableView.automaticDimension
        return cell
    }
    
    
}



class  StatisticViewCell: UITableViewCell{
    //MARK: IBOutlets
    @IBOutlet weak var playerName: DesignableUILabel!
    @IBOutlet weak var metricTable: UITableView!
    
    var metricData = [MatricesResponse]()
    var selectedIndex = Int()

       override func awakeFromNib() {
           metricTable.delegate = self
           metricTable.dataSource = self
           metricTable.estimatedRowHeight = UITableView.automaticDimension
           metricTable.rowHeight = UITableView.automaticDimension
           super.awakeFromNib()
       }
}

extension StatisticViewCell: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.metricData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StatisticMetricCell") as! StatisticMetricCell
        let val = self.metricData[indexPath.row]
        cell.metricLabel.text = val.metrics_name
        cell.rLabel.text = "\(val.ppa_recommended ?? 0)"
        cell.pLabel.text = "\(val.ppa_planned ?? 0)"
        cell.aLabel.text = val.earned
        if(val.succeed == 1){
            cell.aLabel.textColor = greenColor
        }else {
            cell.aLabel.textColor = .red
        }
        return cell
    }
}
 
class StatisticMetricCell: UITableViewCell{
    //Mark: IBOutlets
    
    @IBOutlet weak var metricLabel: DesignableUILabel!
    @IBOutlet weak var rLabel: DesignableUILabel!
    @IBOutlet weak var aLabel: DesignableUILabel!
    @IBOutlet weak var pLabel: DesignableUILabel!
    
    var answerButton:(()-> Void)? = nil
    
    @IBAction func answerAction(_ sender: Any) {
        if let answerButton = self.answerButton{
            answerButton()
        }
    }
}
