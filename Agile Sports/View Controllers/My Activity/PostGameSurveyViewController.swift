//
//  PostGameSurveyViewController.swift
//  Agile Sports
//
//  Created by qw on 21/04/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

var surveyQuestionData = GetSurveyData()

class PostGameSurveyViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var surveyTable: ContentSizedTableView!
    @IBOutlet weak var submitButton: CustomButton!
    
    
    
    var matchId = Int()
    var opponentName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        surveyQuestionData = GetSurveyData()
        surveyTable.estimatedRowHeight = UITableView.automaticDimension
        surveyTable.rowHeight = UITableView.automaticDimension
        self.getSurveyData()
        
    }
    
    
    func getSurveyData(){
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_POST_GAME_QUESTIONS + "\(matchId)", method: .get, parameter: nil, objectClass: GetSurveyQuestion.self, requestCode: U_GET_POST_GAME_QUESTIONS) { (response) in
            surveyQuestionData = response.response
            if(surveyQuestionData.isSubmitted!){
                self.submitButton.isHidden = true
            }else {
                self.submitButton.isHidden = false
            }
            self.surveyTable.reloadData()
        }
    }
    
    //MARK: IBActions
    @IBAction func submitAction(_ sender: Any) {
        var submit = [SubmitSurvey]()
        for val in surveyQuestionData.data {
            if(val.answer_id == nil){
                Singleton.shared.showToast(text: "Please answer all questions")
                submit = [SubmitSurvey]()
                return
            }else{
                var answer = val.answer.filter{
                    $0.id == val.answer_id
                }
                submit.append(SubmitSurvey(question: val.question ?? "", question_id: val.question_id ?? 0, answer_id: val.answer_id ?? 0, answer: answer[0].answer ?? ""))
            }
        }
        var surveyArray:Any?
        do {
            let jsonEncoder = JSONEncoder()
            let jsonData = try! jsonEncoder.encode(submit)
            if let jsonArray = try JSONSerialization.jsonObject(with: jsonData, options : .allowFragments) as? [Dictionary<String,Any>]
            {
                surveyArray = jsonArray
                
            } else {
                print("bad json")
            }
        } catch { print(error) }
        
        let param: [String:Any] = [
            "sprint_match_id": self.matchId,
            "post_game_survey": surveyArray
        ]
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_SUBMIT_SURVEY, method: .post, parameter: param, objectClass: Response.self, requestCode: U_SUBMIT_SURVEY) { (respons) in
            Singleton.shared.showToast(text: "Successfully submitted survey")
            ActivityIndicator.hide()
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func backAction(_ sender: Any) {
        self.back()
    }
    
}

extension PostGameSurveyViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return surveyQuestionData.data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SurveyQuestionCell") as! SurveyQuestionCell
        let val = surveyQuestionData.data[indexPath.row]
        cell.questionlabel.text = val.question
        cell.answerData = val
        cell.selectedIndex = indexPath.row
        cell.answerTable.reloadData()
        return cell
    }
}

  

class  SurveyQuestionCell: UITableViewCell{
    //MARK: IBOutlets
    @IBOutlet weak var questionlabel: DesignableUILabel!
    @IBOutlet weak var answerTable: UITableView!
    
    var answerData = GetSurveyQuestionResponse()
    var selectedIndex = Int()

       override func awakeFromNib() {
           answerTable.delegate = self
           answerTable.dataSource = self
           answerTable.estimatedRowHeight = UITableView.automaticDimension
           answerTable.rowHeight = UITableView.automaticDimension
           super.awakeFromNib()
       }
}

extension SurveyQuestionCell: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answerData.answer.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerTableCell") as! AnswerTableCell
        let val = answerData.answer[indexPath.row]
        cell.answerLabel.text = val.answer
        
        if(answerData.answer_id == val.id){
            cell.answerImage.image = #imageLiteral(resourceName: "circleBlue")
        }else {
            cell.answerImage.image = #imageLiteral(resourceName: "circlegray")
        }
        cell.answerButton = {
            self.answerData.answer_id = self.answerData.answer[indexPath.row].id
            surveyQuestionData.data[self.selectedIndex].answer_id = self.answerData.answer[indexPath.row].id
            self.answerTable.reloadData()
        }
        return cell
    }
  
    
}
 
class AnswerTableCell: UITableViewCell{
    //Mark: IBOutlets
    @IBOutlet weak var answerImage: UIImageView!
    @IBOutlet weak var answerLabel: DesignableUILabel!
    
   
    var answerButton:(()-> Void)? = nil
    
    @IBAction func answerAction(_ sender: Any) {
        if let answerButton = self.answerButton{
            answerButton()
        }
    }
}
