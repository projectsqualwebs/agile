//
//  BottomTabViewController.swift
//  Agile Sports
//
//  Created by AM on 26/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class BottomTabViewController: UIPageViewController {
    //MARK: Properties
    var pageControl: UIPageControl?
    var transitionInProgress: Bool = false
    static var customDataSource : UIPageViewControllerDataSource?
    lazy var viewControllerList: [UIViewController] = {
        let sb = UIStoryboard(name:"Main",bundle:nil)
        return [sb.instantiateViewController(withIdentifier: "DashboardViewController" ),
                sb.instantiateViewController(withIdentifier: "ManagePlayersViewController" ),
                sb.instantiateViewController(withIdentifier: "DashboardViewController" ),
        sb.instantiateViewController(withIdentifier: "DashboardViewController" )]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        BottomTabViewController.customDataSource = self
        if let firstViewController = viewControllerList.first {
            self.setViewControllers([firstViewController],direction: .forward,
                                    animated:false,completion:nil)
        }
        for view in self.view.subviews {
            if let subView = view as? UIScrollView {
                subView.isScrollEnabled = false
            }
        }
    }
    
    
    
    func setFirstController() {
        setViewControllers([viewControllerList[0]], direction: .reverse, animated: true, completion: nil)
        self.pageControl?.currentPage = 0
    }
    
    func setSecondController() {
        setViewControllers([viewControllerList[1]], direction: .forward, animated: true, completion: nil)
        self.pageControl?.currentPage = 1
    }
    
    func setThirdController() {
        setViewControllers([viewControllerList[2]], direction: .forward, animated: true, completion: nil)
        self.pageControl?.currentPage = 2
    }
    
    func setFourthController() {
        setViewControllers([viewControllerList[2]], direction: .forward, animated: true, completion: nil)
        self.pageControl?.currentPage = 3
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }
}

extension BottomTabViewController : UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of:viewController)
            else{return nil}
        let previousIndex = vcIndex - 1
        guard previousIndex >= 0 else{return nil}
        guard viewControllerList.count > previousIndex else{return nil}
        return viewControllerList[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of:viewController)
            else{return nil}
        let nextIndex = vcIndex + 1
        guard viewControllerList.count != nextIndex else{return nil}
        guard viewControllerList.count > nextIndex else{return nil}
        return viewControllerList[nextIndex]
    }
}

extension BottomTabViewController : UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl?.currentPage = viewControllerList.index(of: pageContentViewController)!
    }
}


