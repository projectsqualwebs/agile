//
//  DashboardViewController.swift
//  Agile Sports
//
//  Created by AM on 26/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit
import Charts

class DashboardViewController: ParentViewController, SelectFromPicker {
    func selectedItem(name: String, id: Int) {
        if(pickerType == 1){
            self.sprintName.text = name
            self.sprintOverView.text = "Sprint Overview"
            self.selectedSprint = self.sprintData[id]
            self.getMatchSprintData(id: self.sprintData[id].id ?? 0)
            self.getSprints(id:0)
            self.opponentDetailView.isHidden = true
            self.getActualSprintValue(sprintId:self.selectedSprint.id ?? 0)
            self.getMatchScoreGraph(matchId: 0, sprintId: self.selectedSprint.id ?? 0)
            self.getSurveyResult(sprintId:self.selectedSprint.id ?? 0, matchId: 0)
            self.getSprintSurveyGraph(matchId: 0, sprintId: self.selectedSprint.id ?? 0)
            self.getMatchStatics(id: 0)
        }else if(pickerType == 2){
            if(name == "Sprint Overview"){
                self.sprintOverView.text = name
                self.getMatchStatics(id: 0)
            }else {
                self.sprintOverView.text = name.components(separatedBy: " ")[0]
                self.opponentDetailView.isHidden = false
                self.selectedMatchSprint = self.matchSprintData[id-1]
                self.getMatchStatics(id: self.selectedMatchSprint.id ?? 0)
                self.getMatchScoreGraph(matchId: self.selectedMatchSprint.id ?? 0, sprintId: self.selectedSprint.id ?? 0)
                getSurveyResult(sprintId:self.selectedSprint.id ?? 0, matchId: self.selectedMatchSprint.id ?? 0)
                self.getSprintSurveyGraph(matchId: self.selectedMatchSprint.id ?? 0, sprintId: self.selectedSprint.id ?? 0)
                self.getSprints(id: self.selectedMatchSprint.id ?? 0)
                self.totalGameStatisticLabel.text = "Team Sprint Statistics Vs. " +  (self.selectedMatchSprint.team_name ?? "")
            }
        }
    }
    
    
    //MARK: IBOutlets
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var userRole: UILabel!
    @IBOutlet weak var sprintName: UILabel!
    @IBOutlet weak var sprintOverView: UILabel!
    @IBOutlet weak var viewmostAchievedMetrics: UIView!
    @IBOutlet weak var firstAchievedMatric: DesignableUILabel!
    @IBOutlet weak var secondImagePlayer: ImageView!
    @IBOutlet weak var firstImagePlayer: ImageView!
    @IBOutlet weak var secondAchievedMatric: DesignableUILabel!
    @IBOutlet weak var firstPlayerName: DesignableUILabel!
    @IBOutlet weak var secondPlayerName: DesignableUILabel!
    
    @IBOutlet weak var viewTopTeamPerformance: UIView!
    @IBOutlet weak var teamPerformancePoint: DesignableUILabel!
    @IBOutlet weak var viewTopSprintPerformer: UIView!
    @IBOutlet weak var topPerformerPoint: DesignableUILabel!
    @IBOutlet weak var performanceOpponentTeam: DesignableUILabel!
    @IBOutlet weak var topSprintPerformerImage: ImageView!
    @IBOutlet weak var topSprintPerformerName: DesignableUILabel!
    @IBOutlet weak var oppositeTeamName: DesignableUILabel!
    @IBOutlet weak var playerPerformerImage: UIImageView!
    
    @IBOutlet weak var opponentDetailView: UIView!
    @IBOutlet weak var myTeamImage: UIImageView!
    @IBOutlet weak var myTeamName: DesignableUILabel!
    @IBOutlet weak var opponentTeamImage: UIImageView!
    @IBOutlet weak var opponentTeamNme: DesignableUILabel!
    @IBOutlet weak var gameDate: DesignableUILabel!
    @IBOutlet weak var gameLocation: DesignableUILabel!
    @IBOutlet weak var totalGameStatisticLabel: DesignableUILabel!
    
    @IBOutlet weak var viewLineGraph: UIStackView!
    @IBOutlet weak var gameStaticCollection: UICollectionView!
    
    @IBOutlet weak var teamAgilityChart: LineChartView!
    
    @IBOutlet weak var playerAgilityCardCollection: UICollectionView! {
        didSet {
            playerAgilityCardCollection.bounces = false
        }
    }
    
    @IBOutlet weak var AgilityCollectionFlowLayout: AgilityCollectionFlowLayout! {
        didSet {
            AgilityCollectionFlowLayout.stickyRowsCount = 1
            AgilityCollectionFlowLayout.stickyColumnsCount = 1
        }
    }
    
    
    var sprintData = [SprintResponse]()
    var matchSprintData = [GetMatchSprintResponse]()
    var selectedSprint = SprintResponse()
    var selectedMatchSprint = GetMatchSprintResponse()
    var matchStatistics:GetMatchStaticsResponse?
    var matchGraphData = MatchGraphDataResponse()
    var actualSprintData = ActualSpritDataResponse()
    var sprintSurveyData = SprintSurveyDataResponse()
    var surveyData = [GetSurveyResultResponse]()
    var lineChartData = [SprintResponse]()
    
    var pickerType = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Singleton.shared.userInfo = Singleton.getUserInfo()
        self.userName.text = (Singleton.shared.userInfo.first_name ?? "") + " " +  (Singleton.shared.userInfo.last_name ?? "")
        self.userRole.text = Singleton.shared.userInfo.user_role ?? ""
        _ = [ChartDataEntry(x: 1, y: 1),ChartDataEntry(x: 1, y: 5),ChartDataEntry(x: 2, y: 7)]
        DispatchQueue.main.async {
            self.handleAPI()
        }
    }
    
    func handleLineChart(){
        teamAgilityChart.delegate = self
        teamAgilityChart.chartDescription?.enabled = false
        // barChartView.pinchZoomEnabled = false
        teamAgilityChart.drawGridBackgroundEnabled = false
        teamAgilityChart.setScaleEnabled(false)
        teamAgilityChart.rightAxis.enabled = false
        
        teamAgilityChart.legend.enabled = false
        teamAgilityChart.highlightPerTapEnabled = false
        teamAgilityChart.dragEnabled = true
        teamAgilityChart.setScaleEnabled(true)
        
        
        /* Set xAxis values */
        let xAxis = teamAgilityChart.xAxis
        xAxis.labelPosition = .bottom
        xAxis.drawAxisLineEnabled = true
        xAxis.drawGridLinesEnabled = false
        
        xAxis.avoidFirstLastClippingEnabled = false
        
        xAxis.labelTextColor = UIColor.black
        xAxis.granularity = 1
        xAxis.axisMinimum = 0.0
        // xAxis.centerAxisLabelsEnabled = false
        xAxis.valueFormatter = self
        xAxis.labelFont = UIFont.systemFont(ofSize: 10)
        
        /* Y Axis */
        let leftAxis = teamAgilityChart.leftAxis
        leftAxis.enabled = true
        // leftAxis.drawAxisLineEnabled = false
        leftAxis.drawGridLinesEnabled = true
        leftAxis.gridLineDashPhase = 0
        leftAxis.gridLineDashLengths = [0]
        leftAxis.gridLineWidth = 0.5
        leftAxis.gridColor = UIColor.black
        //leftAxis.drawLabelsEnabled = false
        
        leftAxis.labelTextColor = UIColor.black
        leftAxis.axisMinimum = 0.0
        
        self.teamAgilityChart.fitScreen()
        
        var lineChartEntry  = [ChartDataEntry]()
        var lineChartEntry2  = [ChartDataEntry]()
       // let numbers = [30.0,40.0,50.0,60.0,70.0,80.0,100.0,200.0,350.0,600.0]
        for i in 0..<self.matchGraphData.actual_score!.count {
            let value = ChartDataEntry(x:Double(i), y: Double(self.matchGraphData.actual_score?[i] ?? "0")!)
            lineChartEntry.append(value)
        }
        
        for i in 0..<self.matchGraphData.planned_score!.count {
            let value = ChartDataEntry(x:Double(i), y:Double(self.matchGraphData.planned_score?[i] ?? "0")!)
            lineChartEntry2.append(value)
        }
        lineChartEntry.sort(by: { $0.x < $1.x })
        lineChartEntry2.sort(by: { $0.x < $1.x })
        self.teamAgilityChart.xAxis.axisMinimum =  -0.5
        let line1 = LineChartDataSet(entries: lineChartEntry, label: "Actual Score")
        let line2 = LineChartDataSet(entries: lineChartEntry2, label: "Planned Score")
        line1.colors = [NSUIColor.black]
        line2.colors = [NSUIColor.blue]
        let data = LineChartData()
        data.addDataSet(line1)
        data.addDataSet(line2)
        teamAgilityChart.data = data
        teamAgilityChart.setVisibleXRangeMaximum(5)
    }
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        if(((self.matchGraphData.matches?.count ?? 0) > 0) && (Int(value) < (self.matchGraphData.matches?.count ?? 0))){
            var fullString: String = self.matchGraphData.matches?[Int(value)] ?? ""
            let fullStringArr = fullString.components(separatedBy: " ")
            var firstString: String = fullStringArr[0]
            var lastString:String?
            if(fullStringArr.count > 1){
             lastString  = fullStringArr[1]
            }
            return firstString + "\n" + (lastString ?? "")
        }else {
            return ""
        }
    }
    
    //MARK: IBActions
    @IBAction func selectSprintAction(_ sender: Any) {
        self.pickerType = 1
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.pickerData = []
        myVC.pickerDelegate = self
        myVC.pickerHeading = "Select Sprint"
        for val in self.sprintData{
            myVC.pickerData.append("SPRINT" + (val.sprint_no ?? ""))
        }
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func selectMatchSprint(_ sender: Any) {
        self.pickerType = 2
        let myVC = self.storyboard?.instantiateViewController(withIdentifier: "PickerViewController") as! PickerViewController
        myVC.modalPresentationStyle = .overFullScreen
        myVC.pickerData = []
        myVC.pickerDelegate = self
        myVC.pickerHeading = "Select Match Sprint"
        myVC.pickerData.append("Sprint Overview")
        for val in self.matchSprintData  {
            let opponent = (val.opponent_team ?? "") + "  " +  self.convertTimestampToDate(val.date ?? 0, to: "MMM dd yyyy")
            myVC.pickerData.append(opponent)
        }
        
        if(myVC.pickerData.count > 0){
            self.navigationController?.present(myVC, animated: true, completion: nil)
        }
    }
    
    @IBAction func optionActoin(_ sender: Any) {
        self.menu()
    }
    
}

extension DashboardViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(collectionView == gameStaticCollection){
            return 1
        }else {
            return (self.actualSprintData.players?.count ?? 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == gameStaticCollection){
            return self.matchGraphData.selected_match.agility_card_new?.count ?? 0
        }else {
            return ((self.actualSprintData.players?[section].metrics?.count ?? 0))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == gameStaticCollection){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell", for: indexPath) as! AgilityCardCollectionViewCell
            let val = self.matchGraphData.selected_match.agility_card_new?[indexPath.row]
            cell.titleFCM.text = val?.metrics_name
            cell.scoreValue.text = val?.average?[0].metrics_average
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AgilityCardCollectionViewCell2", for: indexPath) as! AgilityCardCollectionViewCell
            if(indexPath.section == 0 && indexPath.row == 0){
                cell.titleFCM.text = "Player"
                cell.titleFCM.textColor = .white
                cell.backView.backgroundColor = brownColor
            }else if(indexPath.section == 0){
                cell.titleFCM.text =  self.actualSprintData.players?[indexPath.section].metrics?[indexPath.row].metrics_name
                cell.titleFCM.textColor = .white
                cell.backView.backgroundColor = brownColor
            }else if(indexPath.row == 0){
                cell.titleFCM.text = (self.actualSprintData.players?[indexPath.section].first_name ?? "") + " "  + (self.actualSprintData.players?[indexPath.section].last_name ?? "")
                cell.titleFCM.textColor = brownColor
                cell.backView.backgroundColor = .white
            }else {
                cell.titleFCM.text = self.actualSprintData.players?[indexPath.section].metrics?[indexPath.row].score
                cell.titleFCM.textColor = brownColor
                cell.backView.backgroundColor = .white
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == gameStaticCollection){
            return CGSize(width: 80, height: 20)
        }else {
            return CGSize(width: 80, height: 40)
        }
    }
}

extension DashboardViewController: ChartViewDelegate,IAxisValueFormatter{
    func initChartView(){
        
    }
    
    func handleChartView(){
        
    }
    
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print("working")
    }
}

extension DashboardViewController {
    func handleAPI(){
        self.getSprintData { (response) in
            self.sprintData = response.response
            Singleton.shared.sprintData = response.response
            if(self.sprintData.count > 0){
                self.selectedSprint = self.sprintData[0]
                self.sprintName.text = "SPRINT " + "\(self.selectedSprint.id ?? 0)"
                self.getMatchSprintData(id: self.selectedSprint.id ?? 0)
                self.getSprints(id: 0)
                self.getActualSprintValue(sprintId:self.selectedSprint.id ?? 0)
                self.getMatchScoreGraph(matchId: 0, sprintId: self.selectedSprint.id ?? 0)
                self.getSurveyResult(sprintId:self.selectedSprint.id ?? 0, matchId: 0)
                self.getSprintSurveyGraph(matchId: 0, sprintId: self.selectedSprint.id ?? 0)
                self.getMatchStatics(id: 0)
            }
        }
        
    }
    
    func getSprints(id:Int){
        var url = String()
        if(id == 0){
            url = U_BASE + U_GET_SPRINT + "?matchId="
            self.sprintOverView.text = "Sprint Overview"
        }else {
            url = U_BASE + U_GET_SPRINT + "?matchId=\(id)"
        }
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetSprints.self, requestCode: U_GET_SPRINT) { (response) in
            self.lineChartData = response.response
            
        }
    }
    
    func getMatchSprintData(id:Int){
        var url = String()
        if(id == 0){
            url = U_BASE + U_GET_SPRINT_MATCH + "sprint_overview"
            self.sprintOverView.text = "Sprint Overview"
        }else {
            url = U_BASE + U_GET_SPRINT_MATCH + "\(id)"
        }
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetMatchSprint.self, requestCode: U_GET_SPRINT_MATCH) { (response) in
            self.matchSprintData = response.response
            
        }
    }
    
    func getMatchScoreGraph(matchId:Int,sprintId:Int){
        var url = String()
        if(matchId==0){
            url = U_BASE + U_GET_MATCH_SCORE_GRAPH + "sprint_overview" + "&sprintId=\(sprintId)"
        }else {
            url = U_BASE + U_GET_MATCH_SCORE_GRAPH + "\(matchId)" + "&sprintId=\(sprintId)"
        }
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: MatchGraphData.self, requestCode: U_GET_MATCH_SCORE_GRAPH) { (response) in
            self.matchGraphData = response.response
            self.handleLineChart()
            self.gameStaticCollection.reloadData()
        }
        
    }
    
    func getSurveyResult(sprintId:Int,matchId: Int){
        ActivityIndicator.show(view: self.view)
        var url = String()
        if(matchId==0){
            url = U_BASE + U_GET_TEAM_MEMBERS + "\(sprintId)&matchId=1"
        }else {
            url = U_BASE + U_GET_TEAM_MEMBERS + "\(sprintId)&matchId=\(matchId)"
        }
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetSurveyResult.self, requestCode: U_GET_TEAM_MEMBERS) { (response) in
            self.surveyData = response.response
            NotificationCenter.default.post(name: NSNotification.Name(N_SURVEY_DATA), object: self.surveyData)
        }
    }
    
    
    func getActualSprintValue(sprintId:Int){
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_ACTUAL_SPRINT_VALUE + "\(sprintId)", method: .get, parameter: nil, objectClass: ActualSpritData.self, requestCode: U_GET_ACTUAL_SPRINT_VALUE) { (response) in
            self.actualSprintData = ActualSpritDataResponse()
            self.actualSprintData = response.response!
            var metrices = response.response?.players?[0].metrics
            metrices?.insert(MatricesResponse(), at: 0)
            let user = UserInfo(id: 0, username: "", first_name: "", last_name: "", email: "", salt: "", user_type_id: 0, team_id: 0, recommended_value: "", planed_value: "", actual_value: "", status: "", unique_code_id: 0, time_zone: "", image: "", assign_League_id: 0, user_role: "", token: "", last_login: 0, player_country: "", player_dob: 0, player_height: "", player_weight: "", player_jersey_no: "", player_draft_year: "", player_position: 0, player_experience: 0, nba_league_id: 0, nba_team_id: 0, nba_player_id: 0, opponent_team: "", metrics: metrices)
            self.actualSprintData.players?.insert(user, at: 0)
            self.playerAgilityCardCollection.reloadData()
        }
    }
    
    func getSprintSurveyGraph(matchId:Int,sprintId:Int){
        var url = String()
        if(matchId == 0){
            url =  U_BASE + U_SPRINT_SURVEY_GRAPH + "sprint_overview" +  "/\(sprintId)"
        }else {
            url =  U_BASE + U_SPRINT_SURVEY_GRAPH + "\(matchId)" +  "/\(sprintId)"
        }
        SessionManager.shared.methodForApiCalling(url:url, method: .get, parameter: nil, objectClass: SprintSurvey.self, requestCode: U_SPRINT_SURVEY_GRAPH) { (response) in
            print(response.response)
        }
    }
    
    func getMatchStatics(id:Int){
        var url = String()
        if(id==0){
            url = U_BASE + U_MATCH_STATISTICS + "sprint_overview/\(self.selectedSprint.id ?? 0)"
        }else {
            url = U_BASE + U_MATCH_STATISTICS + "\(id)/\(self.selectedSprint.id ?? 0)"
        }
        SessionManager.shared.methodForApiCalling(url: url, method: .get, parameter: nil, objectClass: GetMatchStatics.self, requestCode: U_MATCH_STATISTICS) { (response) in
            self.matchStatistics = response.response
            self.topSprintPerformerName.text = (response.response?.best_player?.first_name ?? "") + (response.response?.best_player?.last_name ?? "")
            self.topSprintPerformerImage.sd_setImage(with: URL(string: (response.response?.best_player?.image ?? "")), placeholderImage: #imageLiteral(resourceName: "player"))
            self.topPerformerPoint.text = response.response?.best_player?.actual_value
            self.performanceOpponentTeam.text = response.response?.best_player?.opponent_team
            if((response.response?.most_metrics?.count ?? 0) > 0){
                self.firstPlayerName.text = (response.response?.most_metrics?[0].first_name ?? "") + " " + (response.response?.most_metrics?[0].last_name ?? "")
                self.firstImagePlayer.sd_setImage(with: URL(string: (response.response?.most_metrics?[0].image ?? "")), placeholderImage: #imageLiteral(resourceName: "player"))
                self.firstAchievedMatric.text = ""
                self.secondAchievedMatric.text = ""
                if let intVal = response.response?.most_metrics?[0].total?.totalInt as? Int {
                    self.firstAchievedMatric.text = "\(intVal) out of 18"
                }
                if(self.firstAchievedMatric.text!.isEmpty){
                    if let stringVal = response.response?.most_metrics?[0].total?.totalString as? String {
                        self.firstAchievedMatric.text = "\(stringVal) out of 18"
                    }
                }
                
                if let intVal = response.response?.most_metrics?[1].total?.totalInt as? Int {
                    self.secondAchievedMatric.text = "\(intVal) out of 18"
                }
                if(self.secondAchievedMatric.text!.isEmpty){
                    if let stringVal = response.response?.most_metrics?[1].total?.totalString as? String {
                        self.secondAchievedMatric.text = "\(stringVal) out of 18"
                    }
                }
                self.secondPlayerName.text = (response.response?.most_metrics?[1].first_name ?? "") + (response.response?.most_metrics?[1].last_name ?? "")
                self.secondImagePlayer?.sd_setImage(with: URL(string: (response.response?.most_metrics?[1].image ?? "")), placeholderImage: #imageLiteral(resourceName: "player"))
            }
            self.teamPerformancePoint.text = response.response?.match_highest?.actual_value
            self.playerPerformerImage.sd_setImage(with: URL(string: (response.response?.match_highest?.opponent_profile_picture ?? "")), placeholderImage: #imageLiteral(resourceName: "player"))
            self.oppositeTeamName.text = response.response?.match_highest?.opponent_team
            self.opponentTeamImage.sd_setImage(with: URL(string: (response.response?.match_highest?.opponent_profile_picture ?? "")), placeholderImage: nil)
            self.myTeamImage.sd_setImage(with: URL(string: (response.response?.match_highest?.team_profile_picture ?? "")), placeholderImage:nil)
            self.myTeamName.text = "Golden State Warriors"
            self.opponentTeamNme.text = response.response?.best_player?.opponent_team
            if(self.sprintOverView.text == "Sprint Overview"){
                self.totalGameStatisticLabel.text = "Team's Sprint Statistics"
                self.viewLineGraph.isHidden = false
            }else {
                self.viewLineGraph.isHidden = true
                self.totalGameStatisticLabel.text = ("Golden State Warriors") + " Vs. " + (response.response?.best_player?.opponent_team ?? "")
            }
            
            self.gameDate.text = self.convertTimestampToDate(response.response?.best_player?.date ?? 0, to: "dd MMM YYYY h:mm")
            self.gameLocation.text = response.response?.best_player?.location
        }
    }
}
