//
//  PostSurveyViewController.swift
//  Agile Sports
//
//  Created by qw on 26/07/20.
//  Copyright © 2020 AM. All rights reserved.
//

import UIKit

protocol ImagesIndexDelegate {
    func getImageIndex(index: Int, totalImages: Int)
}

class PostSurveyViewController: UIPageViewController {

    static var dataSource1: UIPageViewControllerDataSource?
    static var indexDelegate: ImagesIndexDelegate? = nil
    var currentIndex: Int = 0
    var surveyData = [GetSurveyResultResponse]()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate = self
        PostSurveyViewController.dataSource1 = self
        
        getImages()
    
        //Add observer so when images comes from server it will update view accordingly
        DispatchQueue.main.async {
            NotificationCenter.default.addObserver(self, selector: #selector(self.getSurveyData(notification:)), name: NSNotification.Name(N_SURVEY_DATA), object: nil)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if surveyData.count > 0 {
           PostSurveyViewController.indexDelegate?.getImageIndex(index: currentIndex, totalImages: (surveyData.count))
        }
    }
    
    @objc func getSurveyData(notification: NSNotification) {
        if let object = notification.object as? [GetSurveyResultResponse] {
            self.surveyData = object
            self.moveToNextPage()
            currentIndex = 0
        }
    }
    
    @objc func getImages() {
        self.setViewControllers([getViewControllerAtIndex(index: currentIndex)!], direction: .forward, animated: false, completion: nil)
    }
    
    @objc func moveToNextPage() {
        if currentIndex > (surveyData.count)-1 {
            currentIndex = 0
        }
        setViewControllers([getViewControllerAtIndex(index: currentIndex)!], direction: .forward, animated: false, completion: nil)
        currentIndex += 1
    }
    
    func getViewControllerAtIndex(index: Int) -> SurveyViewController? {
        let VC = self.storyboard?.instantiateViewController(withIdentifier: "SurveyViewController") as! SurveyViewController
        if surveyData.count > 0 {
            if index > -1 && index < surveyData.count {
                VC.answerData = surveyData[index]
                VC.itemIndex = index
                PostSurveyViewController.indexDelegate?.getImageIndex(index: currentIndex, totalImages:surveyData.count)
            }
        }
        return VC
    }
}

//MARK: Page view controller datasource methods
extension PostSurveyViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let pageContent: SurveyViewController = viewController as! SurveyViewController
        var index = pageContent.itemIndex
        if ((index == 0) || (index == NSNotFound)) {
            return nil
        }
        index -= 1
        return getViewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let pageContent: SurveyViewController = viewController as! SurveyViewController
        var index = pageContent.itemIndex
        if index == NSNotFound {
            return nil
        }
        index += 1
        if index == surveyData.count {
            return nil
        }
        return getViewControllerAtIndex(index: index)
    }
}

//MARK: Page view controller delegate methods
extension PostSurveyViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let currentVC: SurveyViewController = pageViewController.viewControllers?[0] as! SurveyViewController
        currentIndex = Int(currentVC.itemIndex)
        if surveyData.count > 0 {
            PostSurveyViewController.indexDelegate?.getImageIndex(index: currentIndex, totalImages: surveyData.count)
        }
    }
}

