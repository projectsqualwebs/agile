//
//  ManageBottomTabViewController.swift
//  Agile Sports
//
//  Created by AM on 26/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class ManageBottomTabViewController: UIViewController {
    
    @IBOutlet weak var viewForPlayer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if(K_CURRENT_USER == 1 || K_CURRENT_USER == 2 || K_CURRENT_USER == 3 || K_CURRENT_USER == 4 ){
            viewForPlayer.isHidden = false
        }else {
            viewForPlayer.isHidden = true
        }
        self.playersButtonAction(self)
    }
    
    //MARK:IBACTONS
    @IBAction func dashBoardButtonAction(_ sender: Any) {
        let pageViewController = BottomTabViewController.customDataSource as! BottomTabViewController
    pageViewController.setFirstController()
    }
    
    @IBAction func playersButtonAction(_ sender: Any) {
        let pageViewController = BottomTabViewController.customDataSource as! BottomTabViewController
        pageViewController.setSecondController()
    }
    
    @IBAction func messageButtonAction(_ sender: Any) {
        let pageViewController = BottomTabViewController.customDataSource as! BottomTabViewController
        pageViewController.setFirstController()
    }
    
    @IBAction func settingButtonAction(_ sender: Any) {
        let pageViewController = BottomTabViewController.customDataSource as! BottomTabViewController
        pageViewController.setFirstController()
    }
    
}
