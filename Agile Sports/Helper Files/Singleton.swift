//
//  Singleton.swift
//  Agile Sports
//
//  Created by AM on 21/06/19.
//  Copyright © 2019 AM. All rights reserved.
//

import Foundation
import UIKit
import Toaster

class Singleton {
    static let shared = Singleton()
    var userInfo = getUserInfo()
    var addUser = AddUser()
    var userToAdd = String()
    var leagueData = [LeagueData]()
    var matchData = [MatchResponse]()
    var teamData = [TeamsResponse]()
    var sprintData = [SprintResponse]()

    
    public static func getUserInfo() -> UserInfo{
        guard let userData = UserDefaults.standard.data(forKey:UD_USERINFO) else{
            return UserInfo()
        }
        let userInfo = try! JSONDecoder().decode(UserInfo.self, from: userData)
        K_CURRENT_USER = userInfo.user_type_id!
        return userInfo
    }
    
    func showToast(text: String) {
    if (ToastCenter.default.currentToast?.text == text) {
         return
        }
       
        ToastView.appearance().textColor = .white
        ToastView.appearance().backgroundColor = UIColor(hue: 0, saturation: 0, brightness: 0, alpha: 0.7)
        ToastView.appearance().font = UIFont(name: "Montserrat-Medium", size: 17)
       
        Toast(text: text, delay: 0, duration: 2).show()
        Toast(text: text, delay: 0, duration: 2).cancel()
    }
    
    func inializeValue(){
        self.userInfo = Singleton.getUserInfo()
        self.addUser = AddUser()
        self.userToAdd = String()
        self.leagueData = [LeagueData]()
        self.matchData = [MatchResponse]()
        self.teamData = [TeamsResponse]()
        self.sprintData = [SprintResponse]()
    }
    
}

