//
//  Constants.swift
//
//  Created by Manisha  Sharma on 02/01/2019.
//  Copyright © 2019 Qualwebs. All rights reserved.
//

import UIKit

//MARK: Constants
var primaryColor = UIColor(red: 0/255, green: 88/255, blue: 186/255, alpha: 1)//0058BA
var blueColor = UIColor(red: 0/255, green: 119/255, blue: 250/255, alpha: 1)//0077FA
var greenColor = UIColor(red: 0/255, green: 230/255, blue: 118/255, alpha: 1)
var brownColor = UIColor(red: 88/255, green: 48/255, blue: 46/255, alpha: 1)//58302E

////    UIColor(red: 229/255, green: 178/255, blue: 69/255, alpha: 1)
//var secondaryColor = UIColor(red: 219/255, green: 183/255, blue: 55/255, alpha: 1)
//var barBackgroundColor = UIColor(red: 236/255, green: 236/255, blue: 236/255, alpha: 1)
//var backgroundColor = UIColor(red: 229/255, green: 229/255, blue: 229/255, alpha: 1)
//var offWhiteColor = UIColor(red: 249/255, green: 249/255, blue: 249/255, alpha: 1)
//
//Keys


//MARK: API Constants
//Base Url
var U_BASE = "http://159.65.142.31/agile-sports-analytics-api/api/"
var U_BASE_IMAGE = "http://159.65.142.31/agile-sports-api/public/images/"

var U_LOGIN = "login"
var U_GET_SPRINT = "get-sprint"
var U_GET_SPRINT_MATCH = "get-sprint-match/"
var U_MATCH_STATISTICS = "match-statistics/"
var U_GET_MATCH_SCORE_GRAPH = "get-match-scores-graph?matchId="
var U_GET_ACTUAL_SPRINT_VALUE = "actual-value-of-sprint/"
var U_SPRINT_SURVEY_GRAPH = "survey-graph/"


var U_GET_LEAGUE = "get-leagues"
var U_GET_ALL_USERS = "get-all-users?id="
var U_GET_TEAMPLAYERS = "get-team-players"
var U_GET_MATRICES = "get-metrics"
var U_GET_TEAMS = "get-teams"
var U_GET_ASSIGNEMETRICS = "get-assigned-metrics?userId="
var U_ASSIGN_METRICS = "assign-metrics"

var U_GET_ADMINS = "get-admins/"
var U_GET_MATCHES = "get-matches"
var U_GET_TEAM_MATCHES = "get-team-matches"
var U_GET_SINGLE_TEAMINFO = "get-single-team-info/"
var U_GET_POST_GAME_QUESTIONS = "post-game-survey-questions/"
var U_SUBMIT_SURVEY = "submit-post-game-survey"
var U_GET_ALL_PLAYER = "get-team-players?matchId="
var U_GET_STATISTICS_DATA = "get-team-statistics/"
var U_GET_TEAM_MEMBERS = "all-team-members?type="
var U_GET_MATCH_STATISTICS = "match-statistics/"
var U_GET_METRICS = "get-metrics"
var U_ADD_METRIC = "add-metrics"
var U_EDIT_METRIC = "edit-metrics"
var U_GET_SINGLE_SPRINT = "get-single-sprint/"
var U_DELETE_ENTRIES = "delete-entries"
var U_GET_SCOUTING_REPORT_QUESTION = "scouting-report-question/"//need match_id
var U_GET_SPRINT_REVIEW_QUESTION = "sprint-review-question/"//sprint_id
var U_SPRINT_STRATEGY_QUESTION = "sprint-strategy-question/"

var U_GET_SPRINT_REVIEW = "sprint-review-question/"
var U_SUBMIT_SPRINT_STRATEGY = "submit-sprint-strategy"
var U_SUBMIT_SPRINT_REVIEW = "submit-sprint-review"
var U_SUBMIT_SCOUTING_REPORT = "submit-scouting-report"
var U_SUBMIT_MEETING_NOTES = "submit-meeting-notes"
var U_MEETING_NOTES_QUESTION = "meeting-notes-question/"


var U_EDIT_USER = "edit-user"
var U_EDIT_MATCH = "edit-match"
var U_EDIT_TEAM = "edit-team"
var U_EDIT_SPRINT = "edit-sprint"
var U_EDIT_LEAGUE = "edit-league"
var U_EDIT_MATRICS = "edit-metrics"


var U_ADD_NEWTEAM  = "add-new-team"
var U_ADD_TEAM = "add-team"
var U_ADD_METRICES = "add-metrics"
var U_ADD_USER = "add-user"
var U_ADD_LEAGUE = "add-league"
var U_ADD_MATCH = "add-match"


var N_LOGOUT_USER = "logout_user"


//Controller id's
var K_SPLASH = "splash"
var K_BOTTOMTAB = "bottom_tab"

//MARK: Constants
var K_CURRENT_USER = Int()
var K_EMAIL = "email"
var K_PASSWORD = "password"
var K_USERNAME = "username"
var K_FIRSTNAME = "first_name"
var K_LASTNAME = "last_name"
var K_ROLETYPE = "role"
var K_ROLEID = "role_id"
var K_LOCATION = "location"
var K_LEAGUEID = "league_id"
var K_SPRINTS = "sprints"
var K_TEAMID = "team_id"
var K_PHONE_NUMBER = "phone_number"
var K_NAME = "name"
var K_PROFILE_PICTURE = "profile_picture"

var K_POST_GAME_SURVEY = "post_game_survey"
var K_PLAN_AGILITY_SCORE = "plan_agility_score"
var k_SUBMIT_SPRINT_STRATEGY = "submit_sprint_stategy"
var K_SUBMIT_MEETING_NOTES = "submit_meeting_notes"
var K_SUBMIT_SPRINT_REVIEW = "submit_sprint_review"
var K_SET_RECOMMENDED_VALUE  = "set_recommended_value"
var K_SUBMIT_SCOUTING_REPORT = "submit_scounting_report"
var K_ALL_PLAYER_AGILITY_CARDS = "playser_agility_card"
var K_STATISTICS_SCREEN = "players_statistics"
var K_VIEW_SURVEY_RESULT = "view_survey_result"
var K_SPRINT_STRATEGY = "view_sprint_startegy"
var K_SCOUTING_REPORT = "view_scounting_report"
var K_SPRINT_REVIEW = "view_sprint_review"
var K_POST_GAME_NOTES = "view_post_game_survey_notes"

//MARK: User Defaults
var UD_TOKEN = "access_token"
var UD_USERINFO = "user_info"




//MARK: Notification
var N_UPDATE_COLLECTION_FIELDS = "update_collection_fields"
var N_EDIT_SPRINT_STRATEGY = "edit_sprint_strategy"
var N_SURVEY_DATA = "post_survey_data"
