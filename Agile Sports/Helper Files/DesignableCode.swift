

import UIKit

@IBDesignable
class CustomButton: UIButton {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }

    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var fontSize: Int {
        get {
            return fontSize
        } set {
            self.titleLabel?.font = changeFont(val: newValue)
            
        }
    }

    
    @IBInspectable var circle: Bool {
        get {
            return circle
        } set {
            if newValue {
                self.layer.cornerRadius = self.frame.width/2
                self.clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return cornerRadius
        } set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            self.layer.shadowOffset = newValue
        }
    }
}

@IBDesignable
class View: UIView {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWidth
        } set {
            self.layer.borderWidth = newValue
            self.layer.masksToBounds = false
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
            self.layer.masksToBounds = false
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return cornerRadius
        } set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
            self.layer.masksToBounds = false
        }
    }
    
    @IBInspectable var circle: Bool {
        get {
            return circle
        } set {
            if newValue {
                self.layer.cornerRadius = self.frame.width/2
                self.clipsToBounds = true
            }
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
            self.layer.masksToBounds = false
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
            self.layer.masksToBounds = false
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            self.layer.shadowOffset = newValue
            self.layer.masksToBounds = false
        }
    }
}

@IBDesignable
class ImageView: UIImageView {
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            self.layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return cornerRadius
        } set {
            self.layer.cornerRadius = newValue
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var circle: Bool {
        get {
            return circle
        } set {
            if newValue {
                self.layer.cornerRadius = self.frame.height/2
                self.clipsToBounds = true
                self.layer.masksToBounds = true
                self.contentMode = .scaleAspectFill
            }
        }
    }
    
    @IBInspectable var customTintColor: UIColor {
        get {
            return customTintColor
        } set {
            let templateImage =  self.image?.withRenderingMode(.alwaysTemplate)
            self.image = templateImage
            self.tintColor = newValue
        }
    }
}

@IBDesignable
class DesignableUITextField: UITextField {
    
    // Provides left padding for images
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
    @IBInspectable var placeholderTextColor: UIColor? = .lightGray
    
    override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.leftViewRect(forBounds: bounds)
        textRect.origin.x += leftPadding
        return textRect
    }
    
    @IBInspectable var placeHolderText: String {
        get {
            return placeHolderText
        } set {
            self.attributedPlaceholder = NSAttributedString(string: newValue, attributes: [NSAttributedString.Key.foregroundColor : placeholderTextColor])
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var fontSize: Int {
        get {
            return fontSize
        } set {
            self.font = changeFont(val: newValue)
            
        }
    }

    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        var textRect = super.rightViewRect(forBounds: bounds)
        textRect.origin.x -= rightPadding
        return textRect
    }
    
    @IBInspectable var leftImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var rightImage: UIImage? {
        didSet {
            updateView()
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.shadowColor!)
        } set {
            self.layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return self.layer.shadowOffset
        } set {
            self.layer.shadowOffset = newValue
        }
    }
  
    
    @IBInspectable var color: UIColor = primaryColor {
        didSet {
            updateView()
        }
    }
    
    func updateView() {
        if let image = leftImage {
            leftViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            leftView = imageView
        } else if let image = rightImage {
            rightViewMode = UITextField.ViewMode.always
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .scaleAspectFit
            imageView.image = image
            // Note: In order for your image to use the tint color, you have to select the image in the Assets.xcassets and change the "Render As" property to "Template Image".
            imageView.tintColor = color
            rightView = imageView
        } else {
            leftViewMode = UITextField.ViewMode.never
            leftView = nil
            rightViewMode = UITextField.ViewMode.never
            rightView = nil
        }
        
        // Placeholder text color
        attributedPlaceholder = NSAttributedString(string: placeholder != nil ?  placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: UIColor.black])
    }
}


@IBDesignable
class DesignableUILabel: UILabel {
    
    // Provides left padding for images
    
    @IBInspectable var leftPadding: CGFloat = 0
    @IBInspectable var rightPadding: CGFloat = 0
   
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return borderWidth
        } set {
            self.layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var fontSize: Int {
        get {
            return fontSize
        } set {
            self.font = changeFont(val: newValue)
            
        }
    }

    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        } set {
            self.layer.borderColor = newValue?.cgColor
        }
    }
    
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return self.layer.shadowOpacity
        } set {
            self.layer.shadowOpacity = newValue
        }
    }
    
}

class GradientView: UIView {
    
    @IBInspectable
    public var startColor: UIColor = blueColor  {
        didSet {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    public var endColor: UIColor = primaryColor {
        didSet {
            gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
            setNeedsDisplay()
        }
    }
    
    @IBInspectable
    public var cornerRadius:CGFloat = 0.0
    
    @IBInspectable
    public var circleView:Bool = false
    
    private lazy var gradientLayer: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.bounds
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x:0.5, y:1)
        return gradientLayer
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layer.insertSublayer(gradientLayer, at: 0)
        self.layer.opacity = 0.4
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = self.bounds
        if circleView {
            self.layer.cornerRadius = self.frame.size.width/2
            gradientLayer.cornerRadius = self.frame.size.width/2
        } else {
            gradientLayer.cornerRadius = cornerRadius
        }
    }
}



func changeFont(val: Int) -> UIFont {

    switch val {
    //For Main heading
    case 1:
        return UIFont.boldSystemFont(ofSize: 21)
    //FOR Main Title
    case 2:
        return UIFont.boldSystemFont(ofSize: 21)
    //FOR Main Sub-heading
    case 3:
        return UIFont.systemFont(ofSize: 19, weight: .semibold)
    //FOR Body heading
    case 4:
    return UIFont.systemFont(ofSize: 17, weight: .medium)
    //FOR small heading
    case 5:
     return UIFont.systemFont(ofSize: 15, weight: .regular)
    //FOR extra small heading
    case 6:
        return UIFont.systemFont(ofSize: 13, weight: .regular)
    default:
     return UIFont.systemFont(ofSize: 17, weight: .medium)
    }

}
