
import UIKit

struct SplashContent {
    var backgroundImage: String
    var icon: String?
    var title: String?
    var content: String?
}

struct MenuObject {
   var name: String
    var items: [String]
    var collapsed: Bool
      
    init(name: String, items: [String], collapsed: Bool = true) {
      self.name = name
      self.items = items
      self.collapsed = collapsed
    }
}

struct Response: Codable {
    var message: String?
    var status: Int?
}

struct CreateTeam: Codable {
    var message: String?
    var response: Int?
    var status: Int?
}

struct Loginresponse: Codable {
    var response: UserInfo?
    var message: String?
    var status: Int?
}

struct GetMatch: Codable {
    var response =  [MatchResponse]()
    var message: String?
    var status: Int?
}

struct MatchResponse: Codable {
    var id: Int?
    var team_id: Int?
    var opponent_id: Int?
    var league_id: Int?
    var date: Int?
    var sprint_id: String?
    var city: String?
    var time: String?
    var location: String?
    var team_name: String?
    var league_name: String?
    var opponent_team: String?
    var opponent_team_profile_picture: String?
    var team_details:TeamDetails?
    var opp_team_details:TeamDetails?
}

struct TeamDetails: Codable {
    var id: Int?
    var user_id: Int?
    var match_id: Int?
    var recommended_value: String?
    var planed_value: String?
    var actual_value: String?
    var user_type_id: Int?
    var sprint_id: Int?
}


struct UserInfo: Codable {
    var id: Int?
    var username: String?
    var first_name: String?
    var last_name: String?
    var email: String?
    var salt: String?
    var user_type_id: Int?
    var team_id: Int?
    var recommended_value: String?
    var planed_value:String?
    var actual_value: String?
    var status: String?
    var unique_code_id: Int?
    var time_zone: String?
    var image:String?
    var assign_League_id: Int?
    var user_role: String?
    var token: String?
    var last_login: Int?
    var player_country: String?
    var player_dob: Int?
    var player_height: String?
    var player_weight: String?
    var player_jersey_no: String?
    var player_draft_year: String?
    var player_position: Int?
    var player_experience: Int?
    var nba_league_id: Int?
    var nba_team_id: Int?
    var nba_player_id: Int?
    var opponent_team: String?
    //var team_name:String?
    var date:Int?
    var location:String?
    var opponent_profile_picture:String?
    var team_profile_picture: String?
    var metrics :[MatricesResponse]?
}

struct GetTeamAdmin: Codable {
    var response: [UserInfo]?
    var message: String?
    var status: Int?
}


struct GetLeagueAdmin: Codable {
    var response: [UserInfo]?
    var message: String?
    var status: Int?
}

struct GetUsers: Codable {
    var response: [UserInfo]?
    var message: String?
    var status: Int?
}

struct AddUser: Codable {
    var username: String?
    var first_name: String?
    var last_name: String?
    var email: String?
    var user_type_id:Int?
    var team_id: String?
    var league_id: String?
}

struct GetMatrices: Codable {
    var response: [MatricesResponse]?
    var message: String?
    var status: Int?
}

struct MatricesResponse: Codable {
    var id: Int?
    var item_id:Int?
    var metrics_name: String?
    var item_text: String?
    var type: Int?
    var sub_type: Int?
    var by_player: Int?
    var by_analyst: Int?
    var user_id: Int?
    var score: String?
    var metrics_value: String?
    var numerator_metrics: Int?
    var earned: String?
    var denominator_metrics: Int?
    var ppa_recommended: Int?
    var failed: Int?
    var ppa_planned: Int?
    var succeed: Int?
    var edited:Bool? = false
    var match_id: Int?
    var isSelected:Int?
}

struct LeagueResponse: Codable {
    var response: [LeagueData]?
    var message: String?
    var status: Int?
}

struct LeagueData: Codable {
    var id: Int?
    var game_id: Int?
    var name: String?
    var short_name: String?
    var location: String?
    var admin_name: String?
    var phone_number: String?
    var email: String?
    var no_of_teams: Int?
    var no_of_games: Int?
    var is_deleted: Int?
    var created_at: String?
    var updated_at: String?
}

struct GetTeams: Codable {
    var response: [TeamsResponse]?
    var message: String?
    var status: Int?
}

struct TeamsResponse: Codable {
    var id: Int?
    var name:String?
    var email: String?
    var phone_numbre: String?
    var profile_picture: String?
    var status: String?
    var city: String?
    var admin: String?
    var no_of_sprints: String?
    var league_id: String?
    var year_founded: String?
    var nba_team_id: Int?
    var is_nba_franchise: String?
    var alt_city_name:String?
    var tri_code: String?
    var nick_name: String?
    var url_name :String?
    var league_admin:String?
    var first_name:String?
    var last_name: String?
    var league_name: String?
    var short_name: String?
    var div_name: String?
    var conf_name:String?
    var is_all_star: String?
    var sub_domain : String?
    var created_at: String?
    var updated_at: String?
}

struct GetAgility:Codable {
   var response: AgilityResponse?
   var message: String?
   var status: Int?
}

struct AgilityResponse:Codable {
    var metrics: [MatricesResponse]?
    var score: ScoreResponse?
}

struct ScoreResponse: Codable {
    var actual_value : String?
    var recommended_value: String?
    var planed_value: String?
}

struct GetPlayers:Codable {
    var response: PlayerResponse?
    var message: String?
    var status: Int?
}

struct PlayerResponse: Codable {
    var players : [UserInfo]?
}

struct GetTeamInfo: Codable {
    var response: GetTeamResponse?
    var message:String?
    var status:Int?
}

struct GetTeamResponse: Codable {
    var players: [UserInfo]?
    var coach: UserInfo?
    var analyst: UserInfo?
    var team: TeamsResponse?
}

struct GetSprints: Codable {
    var response = [SprintResponse]()
    var message:String?
    var status:Int?
    
}

struct SprintResponse: Codable {
    var id: Int?
    var sprint_no: String?
    var league_id: Int?
    var team_id: Int?
    var start_date: Int?
    var end_date: Int?
    var status:String?
    var match: [MatchResponse]?
    var team_name: String?
    var league_name: String?
}

struct GetMatchSprint: Codable {
    var response = [GetMatchSprintResponse]()
    var status: Int?
    var message: String?
}

struct GetMatchSprintResponse: Codable {
    var id: Int?
    var date: Int?
    var time: String?
    var location: String?
    var team_name: String?
    var opponent_team: String?
    var opponent_team_profile_picture: String?
}

struct GetMatchStatics: Codable {
    var response : GetMatchStaticsResponse?
    var message: String?
    var status: Int?
}

struct GetMatchStaticsResponse: Codable {
    var most_metrics : [MostMetrics]?
    var best_player : UserInfo?
    var team_agility_score : MostMetrics?
    var match_highest : MatchHighest?
    var match_lowest : MatchHighest?
}

struct MostMetrics: Codable {
    var total: Total?
    var user_id: Int?
    var id: Int?
    var username: String?
    var first_name: String?
    var last_name: String?
    var email: String?
    var image: String?
    var actual_value: String?
}

struct MatchHighest: Codable {
    var match_id: Int?
    var actual_value: String?
    var date:Int?
    var time: String?
    var location: String?
    var team_name: String?
    var team_profile_picture: String?
    var opponent_team: String?
    var opponent_profile_picture: String?
    var opponent_id: Int?
}

struct GetSurveyQuestion: Codable {
    var response = GetSurveyData()
    var message: String?
    var status: Int?
}

struct GetSurveyData: Codable {
    var data = [GetSurveyQuestionResponse]()
    var isSubmitted: Bool?
}

struct GetSurveyQuestionResponse:Codable{
    var question: String?
    var question_id: Int?
    var answer_id: Int?
    var answer = [GetAnswer]()
}

struct GetAnswer:Codable{
    var id: Int?
    var user_id: Int?
    var sprint_id: Int?
    var question_id: Int?
    var comment_date: String?
    var first_name: String?
    var last_name: String?
    var image: String?
    var name:String?
    var y:Int?
    var answer: String?
}

struct GetSubmitSurvey: Codable {
    var response:GetSubmitSurveyResponse?
    var message: String?
    var status: Int?
}

struct GetSubmitSurveyResponse: Codable {
    var data = [SubmitSurvey]()
    var isSubmitted: Bool?
}

struct SubmitSurvey: Codable{
    var question: String?
    var question_id: Int?
    var answer_id: Int?
    var answer: String?
}

struct GetAllPlayer: Codable {
    var response = GetAllPlayerResponse()
    var message:String?
    var status: Int?
}

struct GetAllPlayerResponse: Codable{
    var players:[AllPlayerDetail]?
    var coach:AllPlayerDetail?
    var analyst:AllPlayerDetail?
    var team : TeamsResponse?
}

struct AllPlayerDetail: Codable {
    var id: Int?
    var username: String?
    var first_name: String?
    var last_name: String?
    var email: String?
    var image: String?
    var planed_value: String?
    var actual_value: String?
    var recommended_value: String?
    var metrics:[MatricesResponse]?
}


struct GetSurveyResult: Codable {
    var response = [GetSurveyResultResponse]()
    var message: String?
    var status: Int?
    
}

struct GetSurveyResultResponse: Codable {
    var id: Int?
    var category: String?
    var question: String?
    var status: String?
    var answers = [GetAnswer]()
    var survey_by:[SurveyBy]?
    var  collapsed: Bool?
    
}

struct SurveyBy: Codable{
    var first_name: String?
    var last_name: String?
    var image: String?
    var answer_id: Int?
}


//struct GetSprintStrategy: Codable {
//    var response = [GetSprintStrategyResponse]()
//    var message: String?
//    var status: Int?
//}

struct GetSprintStrategyResponse {
    var id: Int?
    var category: String?
    var question: String?
    var status: String?
    var answers = [GetAnswer]()
    var  collapsed: Bool?
    
    init(id: Int?, category: String, question: String, status: String, answers: [GetAnswer], collapsed: Bool = false) {
        self.id = id
        self.category = category
        self.question = question
        self.status = status
        self.answers = answers
        self.collapsed = collapsed
    }
    
}


struct MatchGraphData: Codable {
    var response = MatchGraphDataResponse()
    var message:String?
    var status: Int?
}

struct MatchGraphDataResponse: Codable {
    var actual_score:[String]?
    var planned_score:[String]?
    var matches:[String]?
    var selected_match = SelectedMatchResponse()
    
}

struct SelectedMatchResponse: Codable {
    var agility_card : [AgilityCardResponse]?
    var agility_card_new: [AgilityCardResponse]?
    var active:Int?
    var all_player_of_team:[TeamPlayers]?
    var players:[UserInfo]?
}

struct TeamPlayers: Codable{
    var name:String?
    var id:Int?
    var y: Double?
    var survey_by: [SurveyBy]?
}

struct AgilityCardResponse: Codable{
    var id: Int?
    var sprint_id: Int?
    var metrics_id:Int?
    var type: Int?
    var sub_type: Int?
    var metrics_value: String?
    var score: String?
    var metrics_name: String?
    var full_name: String?
    var team_id:Int?
    var added_by: String?
    var numerator_metrics: Int?
    var denominator_metrics: Int?
    var average:[AverageValue]?
}
 
struct AverageValue: Codable {
    var metrics_average:String?
}

struct ActualSpritData: Codable {
    var response: ActualSpritDataResponse?
}

struct ActualSpritDataResponse: Codable {
    var players: [UserInfo]?
}

struct SprintSurvey: Codable {
    var response: SprintSurveyData?
    var message:String?
    var status:Int?
}

struct SprintSurveyData: Codable {
    var data:[SprintSurveyDataResponse]?
    var isSubmitted:Bool?
}

struct SprintSurveyDataResponse: Codable {
    var question_id:Int?
    var question: String?
    var data:[TeamPlayers]?
}



struct GetSingleMatch: Codable{
    var response: MatchResponse?
    var message:String?
    var status: Int?
}


struct GetSingleSprints: Codable {
    var response = GetSingleSprintsResponse()
    var message:String?
    var status:Int?
    
}

struct GetSingleSprintsResponse: Codable {
    var id: Int?
    var sprint_no: String?
    var league_id: Int?
    var team_id: Int?
    var start_date: Int?
    var end_date: Int?
    var status:String?
    var matches: [SingleMatchResponse]?
    var metrics:[MatricesResponse]?
    var team:TeamsResponse?
    var league:LeagueData?
    var team_name: String?
    var league_name: String?
}

struct SingleMatchResponse: Codable {
    var id: Int?
    var item_id: Int?
    var team_id: Int?
    var opponent_id: Int?
    var league_id: Int?
    var date: String?
    var sprint_id: String?
    var city: String?
    var time: String?
    var location: String?
    var team_name: String?
    var item_text:String?
    var match_date: Int?
    var league_name: String?
    var opponent_team: String?
    var opponent_team_profile_picture: String?
    var team_details:TeamDetails?
    var opp_team_details:TeamDetails?
    var isSelected: Int?
}

struct Total: Codable {
    let totalString: String?
    let totalInt: Int?

    // Where we determine what type the value is
    init(from decoder: Decoder) throws {
        let container =  try decoder.singleValueContainer()

        // Check for a boolean
        do {
            totalString = try container.decode(String.self)
            totalInt = 0
        } catch {
            // Check for an integer
            totalInt = try container.decode(Int.self)
            totalString = ""
        }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try (totalString != nil) ? container.encode(totalString) : container.encode(totalInt)
    }
}
