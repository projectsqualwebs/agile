//
//  CallAPIViewController.swift
//  Agile Sports
//
//  Created by qw on 23/12/19.
//  Copyright © 2019 AM. All rights reserved.
//

import UIKit

class CallAPIViewController: ParentViewController {
    
    static var shared = CallAPIViewController()
 
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func getMatches(completionHandler: @escaping ([MatchResponse]) -> Void) {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_MATCHES, method: .get, parameter: nil, objectClass: GetMatch.self, requestCode: U_GET_MATCHES) { (response) in
            completionHandler(response.response)
            Singleton.shared.matchData = response.response
            ActivityIndicator.hide()
        }
    }
    
    func getLeagueData(completionHandler: @escaping ([LeagueData]) -> Void) {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_LEAGUE, method:.get, parameter: nil, objectClass:LeagueResponse.self, requestCode: U_GET_LEAGUE) { (response) in
            completionHandler(response.response!)
            Singleton.shared.leagueData = response.response!
            ActivityIndicator.hide()
        }
    }
    
    func getTeams(completionHandler: @escaping ([TeamsResponse]) -> Void) {
        ActivityIndicator.show(view: self.view)
        SessionManager.shared.methodForApiCalling(url: U_BASE + U_GET_TEAMS, method:.get, parameter: nil, objectClass:GetTeams.self, requestCode: U_GET_LEAGUE) { (response) in
            completionHandler(response.response!)
            Singleton.shared.teamData = response.response!
            ActivityIndicator.hide()
        }
    }


}
